﻿using System;

namespace FreshCar.Utils.Logger
{
    /// <summary>
    /// Исключение логгирования
    /// </summary>
    public sealed class LoggerException : Exception
    {
        public LoggerException()
        {
        }

        public LoggerException(string message) : base(message)
        {
        }

        public LoggerException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
