﻿using System;

namespace FreshCar.Utils.Geolocation
{
    /// <summary>
    /// Исключение утилиты геолокации
    /// </summary>
    public class GeolocationUtilityException : Exception
    {
        public GeolocationUtilityException()
        {
        }

        public GeolocationUtilityException(string message) : base(message)
        {
        }

        public GeolocationUtilityException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
