﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreshCar.Utils.Extensions
{
    /// <summary>
    /// Расширения для строк
    /// </summary>
    public static class StringExtensions
    {
        public static bool IsNullOrWhiteSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Пытается получить из строки int
        /// </summary>
        public static int? GetIntValue(this string value)
        {
            int? result = null;

            int parsed;

            if (int.TryParse(value, out parsed))
            {
                result = parsed;
            }

            return result;
        }
    }
}
