﻿using System;
using System.Threading.Tasks;
using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Сервис для работы с фирмами
    /// </summary>
    public interface ICompanyService
    {
        /// <summary>
        /// Ищет айди компании, которая принадлежит пользователю
        /// </summary>
        Task<int> GetCompanyId(Guid userId);

        /// <summary>
        /// Возвращает фирму по ее айди
        /// </summary>
        /// <param name="readOnly">true, если данные нужны только для чтения, иначе false</param>
        Task<Company> GetCompany(int companyId, bool readOnly);
    }
}
