﻿using System.Threading.Tasks;
using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Сервис для работы с сотрудниками
    /// </summary>
    public interface IEmployeeService
    {
        /// <summary>
        /// Возвращает список администраторов автомойки. Только для чтения
        /// </summary>
        /// <param name="carWashId">Id автомойки</param>
        Task<CarWashEmployeeInfo[]> GetCarWashAdministrators(int carWashId);

        /// <summary>
        /// Возвращает сотрудника
        /// </summary>
        Task<Employee> GetEmployee(int employeeId);

        /// <summary>
        /// Возвращает сотрудника автомойки
        /// </summary>
        /// <param name="employeeId">Айди сотрудника</param>
        /// <param name="carWashId">Айди автомойки</param>
        Task<CarWashEmployee> GetCarWashEmployee(int employeeId, int carWashId);
    }
}
