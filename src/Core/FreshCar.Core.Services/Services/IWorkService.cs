﻿using System;
using FreshCar.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Сервис для работы с услугами, предоставляемой фирмой (например, мойка с пеной).
    /// </summary>
    public interface IWorkService
    {
        /// <summary>
        /// Возвращает список основных системных услуг. Только для чтения
        /// </summary>
        Task<IEnumerable<Work>> GetMainSystemWorksAsync();

        /// <summary>
        /// Возврвщает список дополнительных системных услуг (кофе, wi-if...). Только для чтения
        /// </summary>
        Task<IEnumerable<Work>> GetAdditionalSystemWorksAsync();



        /// <summary>
        /// Возвращает все основные услуги фирмы (только активные). Только для чтения
        /// </summary>
        Task<Work[]> GetMainWorksAsync(int companyId);

        /// <summary>
        /// Возвращает список доп услуг, присутствующих у автомойки
        /// </summary>
        Task<IEnumerable<Work>> GetAdditionalWorksAsync(int carWashId, bool readOnly);

        /// <summary>
        /// Возвращает список доп услуг, присутствующих у автомойки
        /// </summary>
        Task<CarWashAdditionalWork[]> GetAdditionalWorksLinksAsync(int carWashId, bool readOnly);



        /// <summary>
        /// Возвращает услугу по айди. 
        /// </summary>
        /// <param name="readOnly">true, если данные нужны только для чтения</param>
        Task<Work> GetMainWorkAsync(int workId, bool readOnly);

        /// <summary>
        /// Return Work by Id
        /// </summary>
        Task<Work> GetMainWorkAsync(int workId, int companyId);

        /// <summary>
        /// Проверяет, есть ли уже у фирмы основная услуга с таким названием
        /// </summary>
        Task<bool> MainWorkIsExistAsync(int companyId, string name);

        /// <summary>
        /// Проверяет, можно ли у услуги изменить имя на новое
        /// </summary>
        Task<bool> MainWorkCanEditNameAsync(int companyId, int workId, string name);



        /// <summary>
        /// Ищет тариф по айдишнику
        /// </summary>
        Task<Tariff> GetTariffByIdAsync(int tariffId);

        /// <summary>
        /// Возвращает тариф
        /// </summary>
        /// <param name="carWashId">Id мойки, для которой надо найти тариф</param>
        /// <param name="workId">Id услуги</param>
        /// <param name="carType">Тип машины</param>
        Task<Tariff> GetTariffAsync(int carWashId, int workId, CarType carType);

        /// <summary>
        /// Возвращает тарифы на услугу автомойки.
        /// </summary>
        /// <param name="carWashId">Id автомойки</param>
        /// <param name="workId">Id услуги</param>
        /// <param name="readOnly">true, если данные нужны только для чтения</param>
        Task<Tariff[]> GetTariffsAsync(int carWashId, int workId, bool readOnly);

        /// <summary>
        /// Возвращает информацию о тарифах. Только для чтения
        /// </summary>
        Task<TariffInfo[]> GetTariffsAsync(int[] tariffsId);
    }
}