﻿using System;
using System.Threading.Tasks;
using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Серис для работы с боксами
    /// </summary>
    public interface ICarWashBoxService
    {
        /// <summary>
        /// Проверяет свободен ли бокс (нет ли заказов в это время)
        /// </summary>
        /// <param name="boxId">Id бокса</param>
        /// <param name="dateFrom">Дата начала заказа</param>
        /// <param name="dateTo">Дата окончания заказа</param>
        Task<bool> BoxIsFree(int boxId, DateTime dateFrom, DateTime dateTo);

        /// <summary>
        /// Возвращает бокс автомойки
        /// </summary>
        /// <param name="readOnly">true, если данные нужны только для чтения</param>
        Task<CarWashBox> GetBox(int boxId, bool readOnly);
    }
}
