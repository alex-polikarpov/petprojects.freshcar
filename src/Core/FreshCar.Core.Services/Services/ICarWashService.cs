﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Сервис для работы с сущностями автомоек
    /// </summary>
    public interface ICarWashService
    {
        /// <summary>
        /// Проверяет используется ли уже адрес в нашей системе
        /// </summary>
        /// <param name="latitude">Широта</param>
        /// <param name="longitude">Долгота</param>
        Task<bool> AddressAlreadyExistsAsync(double latitude, double longitude);

        /// <summary>
        /// Возвращает список автомоек (только в статусе "Работает") с краткой информацией.
        /// </summary>
        ///<param name="filters">Фильтры</param>
        Task<IEnumerable<CarWashShortInfoDto>> GetCarWashesShortInfoAsync(GetCarWashesShortInfoFilters filters);

        /// <summary>
        /// true, если автомойка свободна и на нее можно записаться, иначе false
        /// </summary>
        Task<bool> CarWashIsFreeAsync(int carWashId);

        /// <summary>
        /// Предоставляет информацию об автомойке (только если она в статусе работает)
        /// </summary>
        Task<CarWash> GetCarWashAsync(int carWashId);

        /// <summary>
        /// Возвращает массив боксов автомойки (только в статусе работает). Только для чтения
        /// </summary>
        /// <param name="carWashId">Айди автомойки</param>
        /// <param name="onlyWorked">True, если надо вернуть только боксы в статусе Работает</param>
        Task<CarWashBox[]> GetCarWashBoxesAsync(int carWashId, bool onlyWorked = true);

        /// <summary>
        /// Возвращает кол-во рабочих боксов, которые есть у автомойки 
        /// </summary>
        Task<int> GetBoxesCount(int carWashId);

        /// <summary>
        /// Возвращает фотографии автомойки
        /// </summary>
        Task<CarWashPhoto[]> GetPhotos(int carWashId);
    }
}
