﻿using System;
using System.Threading.Tasks;
using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Сервис для работы с аккаунтами
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Возвращает основную информацию по пользователю
        /// </summary>
        Task<UserInfo> GetUserInfoAsync(Guid userId);

        /// <summary>
        /// Возвращает аккаунт по логину и паролю или null, если он не найден
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="passwordHash">хеш код пароля пользователя</param>
        Task<Account> GetAccountAsync(string login, string passwordHash);

        /// <summary>
        /// Проверяет есть ли пользователь с таким логином
        /// </summary>
        Task<bool> UserIsExistsAsync(string login);

        /// <summary>
        /// Получает аккаунт сотрудника или нулл, если у него нет аккаунта
        /// </summary>
        Task<Account> GetEmployeeAccount(int employeeId);

        /// <summary>
        /// Проверяет, может ли сотрудник сменить логин
        /// </summary>
        /// <param name="employeeId">Id сотрудника</param>
        /// <param name="newLogin">Новый логин</param>
        Task<bool> EmployeeCanChangeLogin(int employeeId, string newLogin);
    }
}
