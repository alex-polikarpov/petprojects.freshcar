﻿using System;
using System.Threading.Tasks;
using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Сервис для работы с заказами
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        /// Возвращает заказы за указанную дату (все кроме отмененных и удаленных). Только для чтения
        /// </summary>
        Task<Order[]> GetOrders(int carWashId, DateTime date);

        /// <summary>
        /// Возвращает активный заказ (не завершенный, не отменный, не удаленный).
        /// </summary>
        /// <param name="readOnly">true, если данные нужны только для чтения, иначе false</param>
        Task<Order> GetActiveOrder(int carWashId, long orderId, bool readOnly);

        /// <summary>
        /// Проверяет можно ли изменить время заказа
        /// </summary>
        /// <param name="boxId">Id бокса</param>
        /// <param name="dateFrom">Дата начала заказа</param>
        /// <param name="dateTo">Дата окончания заказа</param>
        /// <param name="orderId">Id заказа, у которого хотят изменить даты</param>
        Task<bool> CanChangeOrderDates(int boxId, long orderId, DateTime dateFrom, DateTime dateTo);

        /// <summary>
        /// Возвращает заявки для статистики. Только для чтения
        /// </summary>
        Task<Order[]> GetOrdersForStatistic(int carWashId, DateTime dateFrom, DateTime dateTo);
    }
}