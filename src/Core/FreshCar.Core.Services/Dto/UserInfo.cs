﻿using System;
using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Основная информация о пользователе
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        public string Email { get; set; }

        /// <summary>
        /// Id фирмы
        /// </summary>
        public int? CompanyId { get; set; }

        /// <summary>
        /// Id работника
        /// </summary>
        public int? EmployeeId { get; set; }

        /// <summary>
        /// Id автомойки
        /// </summary>
        public int? CarWashId { get; set; }

        /// <summary>
        /// Роль
        /// </summary>
        public EmployeeRole? Role { get; set; }
    }
}