﻿using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Дтошка с фильтрами для получения всех автомоек
    /// </summary>
    public class GetCarWashesShortInfoFilters
    {
        /// <summary>
        /// Список айдишников доп. услуг, которые должны присутствовать у автомойки (только системные)
        /// </summary>
        public int[] AdditionalWorksId { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal? Price { get; set; }        

        /// <summary>
        /// Тип услуги (только системные)
        /// </summary>
        public int? WorkId { get; set; }

        /// <summary>
        /// Тип машины
        /// </summary>
        public CarType? CarType { get; set; }
    }
}