﻿using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Информация о сотруднике автомойки
    /// </summary>
    public class CarWashEmployeeInfo
    {
        /// <summary>
        /// Id сотрудника
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия сотрудника
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество сотрудника
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Телефон сотрудника
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Логин сотрудника
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Роль сотрудника
        /// </summary>
        public EmployeeRole Role { get; set; }
    }
}
