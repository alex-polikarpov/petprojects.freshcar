﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Автомойка
    /// </summary>
    [Table("CarWashes")]
    public class CarWash : IEntity<int>
    {
        public const int NAME_MAX_LENGTH = 100;
        public const int DESCRIPTION_MAX_LENGTH = 500;
        public const int PHONE_MAX_LENGTH = 30;
        public const int CITY_MAX_LENGTH = 100;
        public const int STREET_MAX_LENGTH = 200;
        public const int HOUSE_NUMBER_MAX_LENGTH = 20;

        /// <summary>
        /// Id автомойки
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Id компании (чья это автомойка)
        /// </summary>
        [ForeignKey(nameof(Company))]
        [Column("CompanyId")]
        public int CompanyId { get; set; }

        /// <summary>
        /// Название автомойки
        /// </summary>
        [Column("Name")]
        [MaxLength(NAME_MAX_LENGTH)]
        public string Name { get; set; }

        /// <summary>
        /// Описание автомойки
        /// </summary>
        [Column("Description")]
        [MaxLength(DESCRIPTION_MAX_LENGTH)]
        public string Description { get; set; }

        /// <summary>
        /// Время начала рабочего дня (сюда записывать только UTC время)
        /// </summary>
        [Column("StartTimeOfWork")]
        public TimeSpan StartTimeOfWork { get; set; }

        /// <summary>
        /// Время окончания рабочего дня (сюда записывать только UTC время)
        /// </summary>
        [Column("EndTimeOfWork")]
        public TimeSpan EndTimeOfWork { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        [Column("Phone")]
        [MaxLength(PHONE_MAX_LENGTH)]
        public string Phone { get; set; }

        /// <summary>
        /// Город, в котором находится автомойка
        /// </summary>
        [Column("City")]
        [MaxLength(CITY_MAX_LENGTH)]
        public string City { get; set; }

        /// <summary>
        /// Улица, на которой находится автомойка
        /// </summary>
        [Column("Street")]
        [MaxLength(STREET_MAX_LENGTH)]
        public string Street { get; set; }

        /// <summary>
        /// Номер дома автомойки
        /// </summary>
        [Column("HouseNumber")]
        [MaxLength(HOUSE_NUMBER_MAX_LENGTH)]
        public string HouseNumber { get; set; }

        /// <summary>
        /// Широта (местоположение автомойки)
        /// </summary>
        [Column("Latitude")]
        public double Latitude { get; set; }

        /// <summary>
        /// Долгота (местоположение автомойки)
        /// </summary>
        [Column("Longitude")]
        public double Longitude { get; set; }

        /// <summary>
        /// Состояние автомойки
        /// </summary>
        [Column("State")]
        public CarWashState State { get; set; }



        #region Навигационные свойства 

        /// <summary>
        /// Навигационное свойство. Компания (фирма) этой автомойки. 
        /// </summary>
        public virtual Company Company { get; set; }

        /// <summary>
        /// Навигационное свойство. Сотрудники автомойки
        /// </summary>
        public virtual ICollection<CarWashEmployee> CarWashEmployees { get; set; }

        /// <summary>
        /// Навигационное свойство. Связи с доп. услугами для этой мойки
        /// </summary>
        public virtual ICollection<CarWashAdditionalWork> CarWashAdditionalWorks { get; set; }

        /// <summary>
        /// Навигационное свойство. Боксы автомойки
        /// </summary>
        public virtual ICollection<CarWashBox> CarWashBoxes { get; set; }

        /// <summary>
        /// Навигационное свойство. Тарифы
        /// </summary>
        public virtual ICollection<Tariff> Tariffs { get; set; }

        /// <summary>
        /// Фотографии автомойки
        /// </summary>
        public virtual ICollection<CarWashPhoto> CarWashPhotos { get; set; }    

        #endregion
    }
}