﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    //ПРИМЕЧАНИЕ: один сотрудник теоретически может работать на нескольких автомойках,
    //поэтому эта табличка нужна

    /// <summary>
    /// Сотрудник автомойки (связующая сущность)
    /// </summary>
    [Table("CarWashEmployees")]
    public class CarWashEmployee : IEntity<int>
    {
        /// <summary>
        /// Id записи
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Id сотрудника
        /// </summary>
        [Column("EmployeeId")]
        [ForeignKey(nameof(Employee))]
        public int EmployeeId { get; set; }

        /// <summary>
        /// Id автомойки, где работает сотрудник
        /// </summary>
        [Column("CarWashId")]
        [ForeignKey(nameof(CarWash))]
        public int CarWashId { get; set; }

        /// <summary>
        /// Роль сотрудника на автомойке
        /// </summary>
        [Column("EmployeeRole")]
        public EmployeeRole EmployeeRole { get; set; }



        /// <summary>
        /// Навигационное свойство. Сотрудник
        /// </summary>
        public virtual Employee Employee { get; set; }

        /// <summary>
        /// Навигационное свойство. Мойка, где работает сотрудник
        /// </summary>
        public virtual CarWash CarWash { get; set; }
    }
}
