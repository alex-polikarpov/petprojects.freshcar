﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Бокс автомойки (место, где моют машину). 
    /// Сколько у автомойки боксов, столько машин одновременно автомойка может мыть
    /// </summary>
    [Table("CarWashBoxes")]
    public class CarWashBox : IEntity<int>
    {
        public const int NAME_MAX_LENGTH = 30;

        /// <summary>
        /// Id бокса автомойки
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Id автомойки
        /// </summary>
        [Column("CarWashId")]
        [ForeignKey(nameof(CarWash))]
        public int CarWashId { get; set; }

        /// <summary>
        /// Название бокса
        /// </summary>
        [Column("Name")]
        [MaxLength(NAME_MAX_LENGTH)]
        public string Name { get; set; }

        /// <summary>
        /// Состояние бокса
        /// </summary>
        [Column("State")]
        public CarWashBoxState State { get; set; }

        /// <summary>
        /// Порядок сортировки
        /// </summary>
        [Column("SortOrder")]
        public byte SortOrder { get; set; }



        /// <summary>
        /// Навигационное свойство. Заказы
        /// </summary>
        public virtual ICollection<Order> Orders { get; set; }

        /// <summary>
        /// Навигацинное свойство. Автомойка, к которой прикреплен этот бокс
        /// </summary>
        public virtual CarWash CarWash { get; set; }
    }
}
