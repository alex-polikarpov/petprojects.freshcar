﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Фотография автомойки
    /// </summary>
    [Table("CarWashPhotos")]
    public class CarWashPhoto : IEntity<int>
    {
        /// <summary>
        /// Максимальное кол-во символов в ссылке на фото
        /// </summary>
        public const int PHOTO_URL_MAX_LENGTH = 250;

        [Column("Id")]
        public int Id { get; set; }

        [Column("CarWashId")]
        [ForeignKey(nameof(CarWash))]
        public int CarWashId { get; set; }

        /// <summary>
        /// Ссылка на фотографию
        /// </summary>
        [Column("PhotoUrl")]
        [MaxLength(PHOTO_URL_MAX_LENGTH)]
        public string PhotoUrl { get; set; }

        public virtual CarWash CarWash { get; set; }
    }
}