﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Тариф на услугу
    /// </summary>
    [Table("Tariffs")]
    public class Tariff : IEntity<int>
    {
        /// <summary>
        /// Id тарифа
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Id услуги, для которой этот тариф
        /// </summary>
        [Column("WorkId")]
        [ForeignKey(nameof(Work))]
        public int WorkId { get; set; }

        /// <summary>
        /// Id мойки, для которой этот тариф
        /// </summary>
        [Column("CarWashId")]
        [ForeignKey(nameof(CarWash))]
        public int CarWashId { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        [Column("Cost")]
        public decimal Cost { get; set; }

        /// <summary>
        /// Время в минутах (сколько эту услугу делают по времени)
        /// </summary>
        [Column("Duration")]
        public int Duration { get; set; }

        /// <summary>
        /// Тип машины, для которой эта услуга
        /// </summary>
        [Column("CarType")]
        public CarType CarType { get; set; }



        /// <summary>
        /// Навигационное свойство. Услуга, для которой этот тариф
        /// </summary>
        public virtual Work Work { get; set; }

        /// <summary>
        /// Навигационное свойство. Заказы
        /// </summary>
        public virtual ICollection<Order> Orders { get; set; }

        /// <summary>
        /// Навигационное свойство. Автомойка, для которой эта услуга
        /// </summary>
        public virtual CarWash CarWash { get; set; }
    }
}