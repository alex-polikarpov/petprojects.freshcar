﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Услуга, предоставляемая фирмой (например, мойка с пеной). 
    /// Пришлось назвать Work, а не Service иначе дикая путаница будет в проекте
    /// </summary>
    [Table("Works")]
    public class Work : IEntity<int>
    {
        public const int NAME_MAX_LENGTH = 150;
        public const int DESCRIPTION_MAX_LENGTH = 500;

        /// <summary>
        /// Id услуги
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Id фирмы чья это услуга (опционально, например, для системных услуг)
        /// </summary>
        [Column("CompanyId")]
        [ForeignKey(nameof(Company))]
        public int? CompanyId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        [Column("Name")]
        [MaxLength(NAME_MAX_LENGTH)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Описание услуги
        /// </summary>
        [Column("Description")]
        [MaxLength(DESCRIPTION_MAX_LENGTH)]
        public string Description { get; set; }

        /// <summary>
        /// Порядок сортировки
        /// </summary>
        [Column("SortOrder")]
        public byte SortOrder { get; set; }

        /// <summary>
        /// Состояние услуги
        /// </summary>
        [Column("WorkState")]
        public WorkState WorkState { get; set; }

        /// <summary>
        /// Тип услуги
        /// </summary>
        [Column("WorkType")]
        public WorkType WorkType { get; set; }



        /// <summary>
        /// Навигационное свойство. Компания, чья это услуга (опционально)
        /// </summary>
        public virtual Company Company { get; set; }

        /// <summary>
        /// Навигационное свойство. Ссылки на доп. услуги автомоек
        /// </summary>
        public virtual ICollection<CarWashAdditionalWork> CarWashAdditionalWorks { get; set; }

        /// <summary>
        /// Навигационное свойство. Тарифы
        /// </summary>
        public virtual ICollection<Tariff> Tariffs { get; set; }
    }
}
