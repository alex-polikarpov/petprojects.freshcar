﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Аккаунт для входа в админку
    /// </summary>
    [Table("Accounts")]
    public class Account : IEntity<Guid>
    {
        public const int LOGIN_MAX_LENGTH = 50;
        public const int PASSWORD_HASH_MAX_LENGTH = 500;

        /// <summary>
        /// Id аккаунта
        /// </summary>
        [Key]
        [Column("Id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Почта (логин) аккаунта
        /// </summary>
        [Column("Login")]
        [MaxLength(LOGIN_MAX_LENGTH)]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Хеш код пароля
        /// </summary>
        [Column("PasswordHash")]
        [MaxLength(PASSWORD_HASH_MAX_LENGTH)]
        [Required]
        public string PasswordHash { get; set; }

        /// <summary>
        /// Состояние аккаунта
        /// </summary>
        [Column("AccountState")]
        public AccountState AccountState { get; set; }

        /// <summary>
        /// Id сотрудника
        /// </summary>
        [Column("EmployeeId")]
        [ForeignKey(nameof(Employee))]
        public int EmployeeId { get; set; }



        /// <summary>
        /// Навигационное свойство. Сотрудник, которому принадлежит аккаунт (опционально)
        /// </summary>
        public virtual Employee Employee { get; set; }
    }
}
