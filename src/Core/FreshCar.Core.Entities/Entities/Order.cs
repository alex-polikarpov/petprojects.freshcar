﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    //Примечание: здесь нужно хранить даты, стоимость

    /// <summary>
    /// Заказ
    /// </summary>
    [Table("Orders")]
    public class Order : IEntity<long>
    {
        public const int CAR_NUMBER_MAX_LENGTH = 9;
        public const int CLIENT_PHONE_MAX_LENGTH = 30;
        public const int CLIENT_NAME_MAX_LENGTH = 50;
        public const int CAR_MAKE_MAX_LENGTH = 30;

        /// <summary>
        /// Id заказа
        /// </summary>
        [Key]
        [Column("Id")]
        public long Id { get; set; }

        /// <summary>
        /// Id тарифа
        /// </summary>
        [ForeignKey(nameof(Tariff))]
        [Column("TariffId")]
        public int TariffId { get; set; }

        /// <summary>
        /// Id бокса мойки, где будет происходить выполнение заказа
        /// </summary>
        [ForeignKey(nameof(CarWashBox))]
        [Column("CarWashBoxId")]
        public int? CarWashBoxId { get; set; }

        /// <summary>
        /// Дата и время, когда начнется выполнение заказа (обязательно UTC)
        /// </summary>
        [Column("DateFrom")]
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Дата и время, когда закончится выполнение заказа (обязательно UTC)
        /// </summary>
        [Column("DateTo")]
        public DateTime DateTo { get; set; }

        /// <summary>
        /// Стоимость заказа
        /// </summary>
        [Column("Cost")]
        public decimal Cost { get; set; }

        /// <summary>
        /// Тип заказа
        /// </summary>
        [Column("OrderType")]
        public OrderType OrderType { get; set; }

        /// <summary>
        /// Состояние заказа
        /// </summary>
        [Column("OrderState")]
        public OrderState OrderState { get; set; }

        /// <summary>
        /// Номер машины клиента
        /// </summary>
        [Column("CarNumber")]
        [MaxLength(CAR_NUMBER_MAX_LENGTH)]
        public string CarNumber { get; set; }

        /// <summary>
        /// Марка автомобиля
        /// </summary>
        [Column("CarMake")]
        [MaxLength(CAR_MAKE_MAX_LENGTH)]
        public string CarMake { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        [Column("ClientPhone")]
        [MaxLength(CLIENT_PHONE_MAX_LENGTH)]
        public string ClientPhone { get; set; }

        /// <summary>
        /// Имя клиента
        /// </summary>
        [Column("ClientName")]
        [MaxLength(CLIENT_NAME_MAX_LENGTH)]
        public string ClientName { get; set; }



        /// <summary>
        /// Навигационное свойство. Тариф
        /// </summary>
        public virtual Tariff Tariff { get; set; }

        /// <summary>
        /// Навигационное свойство. Автомойка
        /// </summary>
        public virtual  CarWashBox CarWashBox { get; set; }
    }
}