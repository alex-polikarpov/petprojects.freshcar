﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Сотрудник фирмы
    /// </summary>
    [Table("Employees")]
    public class Employee : IEntity<int>
    {
        public const int FIRST_NAME_MAX_LENGTH = 50;
        public const int LAST_NAME_MAX_LENGTH = 100;
        public const int MIDDLE_NAME_MAX_LENGTH = 100;
        public const int PHONE_MAX_LENGTH = 30;

        /// <summary>
        /// Id сотрудника
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Имя сотрудника
        /// </summary>
        [Column("FirstName")]
        [MaxLength(FIRST_NAME_MAX_LENGTH)]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия сотрудника
        /// </summary>
        [Column("LastName")]
        [MaxLength(LAST_NAME_MAX_LENGTH)]
        public string LastName { get; set; }

        /// <summary>
        /// Отчество сотрудника
        /// </summary>
        [Column("MiddleName")]
        [MaxLength(MIDDLE_NAME_MAX_LENGTH)]
        public string MiddleName { get; set; }

        /// <summary>
        /// Телефон сотрудника
        /// </summary>
        [Column("Phone")]
        [MaxLength(PHONE_MAX_LENGTH)]
        public string Phone { get; set; }

        /// <summary>
        /// Id фирмы, в которой работает сотрудник
        /// </summary>
        [Column("CompanyId")]
        [ForeignKey(nameof(Company))]
        public int CompanyId { get; set; }


        /// <summary>
        /// Навигационное свойство. Фирма, где работает сотрудник
        /// </summary>
        public virtual Company Company { get; set; }

        /// <summary>
        /// Навигационное свойство. Ссылки на связи с автомойками, где работает сотрудник
        /// </summary>
        public virtual ICollection<CarWashEmployee> CarWashEmployeeLinks { get; set; }
    }
}
