﻿namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Роль сотрудника на автомойке
    /// </summary>
    public enum EmployeeRole
    {
        /// <summary>
        /// Рядовой сотрудник
        /// </summary>
        Ordinary = 0,

        /// <summary>
        /// Администратор
        /// </summary>
        Administrator = 1,

        /// <summary>
        /// Директор
        /// </summary>
        Director = 2,
    }
}
