﻿namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Состояние услуги
    /// </summary>
    public enum WorkState
    {
        /// <summary>
        /// Активна
        /// </summary>
        Active = 0,

        /// <summary>
        /// Не активна
        /// </summary>
        NotActive = 1,

        /// <summary>
        /// Заморожена / временно приостановлена
        /// </summary>
        Frozen = 2, 

        /// <summary>
        /// Удалена
        /// </summary>
        Deleted = 3,

        /// <summary>
        /// Закрыта (например была какая-то акционная услуга и она закончилась)
        /// </summary>
        Closed = 4,
    }
}
