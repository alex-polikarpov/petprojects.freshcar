﻿namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Тип машины
    /// </summary>
    public enum CarType
    {
        /// <summary>
        /// Другой тип машины (неизвестный)
        /// </summary>
        Other = 0,

        /// <summary>
        /// Мини
        /// </summary>
        Mini = 1,

        /// <summary>
        /// Отечественный легковой автомобиль
        /// </summary>
        PassengerDomestic = 2,

        /// <summary>
        /// Легковой автомобиль иномарка
        /// </summary>
        PassengerForeign = 3,

        /// <summary>
        /// Кроссовер
        /// </summary>
        Crossover = 4,

        /// <summary>
        /// Внедорожник
        /// </summary>
        Suv = 5,

        /// <summary>
        /// Миниавтобус
        /// </summary>
        Minibus = 6
    }
}