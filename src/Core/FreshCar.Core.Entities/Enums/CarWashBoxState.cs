﻿namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Статус бокса автомойки
    /// </summary>
    public enum CarWashBoxState
    {
        /// <summary>
        /// Работает
        /// </summary>
        Works = 0,

        /// <summary>
        /// Не работает
        /// </summary>
        NotWorks = 1,

        /// <summary>
        /// Временно не работает
        /// </summary>
        TemporarilyNotWorks = 2,

        /// <summary>
        /// Удалена
        /// </summary>
        Deleted = 3,

        /// <summary>
        /// На ремонте
        /// </summary>
        OnRepair = 4,
    }
}
