﻿namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Состояние автомойки
    /// </summary>
    public enum CarWashState
    {
        /// <summary>
        /// Работает
        /// </summary>
        Works = 0,

        /// <summary>
        /// Не работает
        /// </summary>
        NotWorks = 1,

        /// <summary>
        /// Временно не работает
        /// </summary>
        TemporarilyNotWorks = 2,

        /// <summary>
        /// Удалена
        /// </summary>
        Deleted = 3,
    }
}
