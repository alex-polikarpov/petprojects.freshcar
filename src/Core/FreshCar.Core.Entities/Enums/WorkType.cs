﻿namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Тип услуги, предоставляемой фирмой
    /// </summary>
    public enum WorkType
    {
        /// <summary>
        /// Основная услуга, созданная администратором автомойки (например, мойка двигателя и колес)
        /// </summary>
        MainCreatedByUser = 0,

        /// <summary>
        /// Основная системная услуга, которую нельзя удалять и менять у нее название, описание
        /// </summary>
        MainSystem = 1,

        /// <summary>
        /// Дополнительная услуга системная, которую нельзя удалять, 
        /// менять (например, кофе, бесплатный wi-fi)
        /// </summary>
        AdditionalSystem = 2,

        /// <summary>
        /// Дополнительная услуга, созданная администратором автомойки
        /// </summary>
        AdditionalCreatedByUser = 3,
    }
}