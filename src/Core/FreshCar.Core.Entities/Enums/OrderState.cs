﻿namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Состояние заказа
    /// </summary>
    public enum OrderState
    {
        /// <summary>
        /// Новый заказ, только что поступил
        /// </summary>
        New = 0,

        /// <summary>
        /// Заказ принят
        /// </summary>
        Accepted = 1,

        /// <summary>
        /// Заказ в работе
        /// </summary>
        InWork = 2,

        /// <summary>
        /// Выполнен и оплачен
        /// </summary>
        FinishedAndPaid = 3,

        /// <summary>
        /// Выполнен, но не оплачен
        /// </summary>
        FinishedAndNotPaid = 4,

        /// <summary>
        /// Удален
        /// </summary>
        Deleted = 5,

        /// <summary>
        /// Отменен клиентом
        /// </summary>
        CanceledByClient = 6,

        /// <summary>
        /// Отменен администратором (когда в админке отменяют)
        /// </summary>
        CanceledByAdministrator = 7
    }
}
