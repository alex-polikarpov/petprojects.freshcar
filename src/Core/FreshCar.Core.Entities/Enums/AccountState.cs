﻿namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Состояние аккаунта
    /// </summary>
    public enum AccountState
    {
        /// <summary>
        /// Не верифицирован (не проверен). Еще не проверяли действительно ли это сотрудник мойки или нет
        /// </summary>
        NotVerified = 0,

        /// <summary>
        /// Верифицирован (проверен)
        /// </summary>
        Verified = 1,

        /// <summary>
        /// Заблокирован
        /// </summary>
        Blocked = 2,

        /// <summary>
        /// Удален
        /// </summary>
        Deleted = 3
    }
}
