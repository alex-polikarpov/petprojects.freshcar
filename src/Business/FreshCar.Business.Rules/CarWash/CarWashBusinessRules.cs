﻿using System;
using Entities = FreshCar.Core.Entities;

namespace FreshCar.Business.Rules.CarWash
{
    /// <summary>
    /// Бизнес правила для автомойки
    /// </summary>
    public static class CarWashBusinessRules
    {
        /// <summary>
        /// Проверяет работает ли автомойка круглосуточно. Возвращает
        /// true если работает круглосуточно, иначе false
        /// </summary>
        public static bool WorkAroundTheClock(this Entities.CarWash carWash)
        {
            var startTime = new TimeSpan(carWash.StartTimeOfWork.Hours, carWash.StartTimeOfWork.Minutes,0);
            var endTime = new TimeSpan(carWash.EndTimeOfWork.Hours, carWash.EndTimeOfWork.Minutes, 0);

            if (TimesIsEquals(carWash.StartTimeOfWork, carWash.EndTimeOfWork))
                return true;



            //если время начала больше времени


            if (carWash.StartTimeOfWork < carWash.EndTimeOfWork)
            {
                
            }

            return false;
        }

        private static bool TimesIsEquals(TimeSpan a, TimeSpan b)
        {
            return a.Hours == b.Hours && a.Minutes == b.Minutes;
        }
    }
}
