﻿namespace FreshCar.Business.Errors
{
    /// <summary>
    /// Константы с ошибками
    /// </summary>
    public static class ErrorConstants
    {
        /// <summary>
        /// Системные ошибки
        /// </summary>
        public static class System
        {
            public const string INVALID_REQUEST = "Неверный запрос";
            public const string ARGUMENT_IS_NULL = "Argument is null";
            public const string INTERNAL_SERVER_ERROR = "Внутренняя ошибка сервера :( Повторите попытку позже.";
        }

        /// <summary>
        /// Ошибки по аккаунтам
        /// </summary>
        public static class Account
        {
            public const string INVALID_LOGIN_OR_PASS = "Неверный логин или пароль!";
            public const string GEOLOCATION_ERROR = "Введите существующий адрес";
            public const string EMAIL_ALREADY_USED = "Электронный адрес уже занят, необходимо использовать другой!";
            public const string ADDRESS_ALREADY_USED = "Данная автомойка уже есть в системе!";
            public const string ACCOUNT_NOT_VERIFIED = "Аккаунт еще не подтвержден! Повторите попытку позже.";
            public const string INVALID_ACCOUNT_STATUS = "Доступ заблокирован";
        }

        /// <summary>
        /// Ошибки услуг
        /// </summary>
        public static class Work
        {
            public const string WORK_CANT_BE_MODIFIED = "Данную услугу нельзя изменять или удалять";
            public const string WORK_NOT_FOUND =  "Услуга не найдена";
            public const string WORK_ALREADY_EXIST = "У Вас уже существует услуга с таким названием";
        }

        /// <summary>
        /// Ошибки по автомойкам
        /// </summary>
        public static class CarWash
        {
            public const string CAR_WASH_NOT_FOUND = "Автомойка не найдена";
            public const string CAR_WASH_NOT_FREE = "К сожалению автомойка занята в этом промежутке времени";
            public const string CAR_WASH_NOT_WORKING = "Автомойка не работает";
            public const string CAR_WASH_NOT_WORKING_ON_THIS_TIME = "Автомойка не работает в это время";
            public const string BOX_NOT_FOUND = "Бокс не найден";
            public const string BOX_NOT_FREE = "Бокс занят в данном промежутке времени";
            public const string MANY_BOXES = "У Вас уже есть 15 боксов, больше создавать нельзя";
            public const string CANT_DELETE_LAST_BOX = "У вас всего один бокс, его нельзя удалить";
            public const string INCORRECT_WORK_TIME = "Неверное время работы автомойки";
        }

        /// <summary>
        /// Ошибки по тарифам
        /// </summary>
        public static class Tariff
        {
            public const string TARIFF_NOT_FOUND = "Тариф не найден";
        }

        /// <summary>
        /// Ошибки по заказам
        /// </summary>
        public static class Order
        {
            public const string ORDER_NOT_FOUND = "Заказ не найден";
            public const string INCORRECT_ORDER_STATE = "Невалидный статус";
            public const string INCORRECT_DATES = "Невалидные даты заказа";
            public const string INCORRECT_COST = "Невалидная стоимость заказа";
            public const string FINISHED_ORDER_CANT_DELETED = "Нельзя удалить выполненный заказ";
        }

        /// <summary>
        /// Ошибки по сотрудникам
        /// </summary>
        public static class Employee
        {
            public const string EMPLOYEE_NOT_FOUND = "Сотрудник не найден";
            public const string DIRECTOR_CANT_DELETED = "Директора нельзя удалить";
            public const string DIRECTOR_CANT_EDITED = "Информацию о директоре нельзя изменять";
        }
    }
}
