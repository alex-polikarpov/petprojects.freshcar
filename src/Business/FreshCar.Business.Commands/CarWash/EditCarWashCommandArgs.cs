﻿using System;

namespace FreshCar.Business.Commands
{
    public class EditCarWashCommandArgs : BaseCommandArguments
    {
        public string Name { get; set; }

        public TimeSpan StartTimeOfWork { get; set; }

        public TimeSpan EndTimeOfWork { get; set; }

        public string Phone { get; set; }
    }
}
