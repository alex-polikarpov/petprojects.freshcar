﻿using System;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands
{
    internal class EditCarWashCommand : BaseCommand<EditCarWashCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _uow;
        private readonly ICarWashService _carWashService;
        private CarWash _carWash;

        public EditCarWashCommand(ILoggerUtility logger, IUnitOfWork uow,
            ICarWashService carWashService)
            : base(logger)
        {
            _uow = uow;
            _carWashService = carWashService;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(EditCarWashCommandArgs arguments)
        {
            _carWash.Name = arguments.Name;
            _carWash.Phone = arguments.Phone;
            _carWash.StartTimeOfWork = new TimeSpan(arguments.StartTimeOfWork.Hours, arguments.StartTimeOfWork.Minutes, 0);
            _carWash.EndTimeOfWork = new TimeSpan(arguments.EndTimeOfWork.Hours, arguments.EndTimeOfWork.Minutes, 0);
            await _uow.SaveChangesAsync();
            return new EmptyCommandResult { IsSuccess = true };
        }

        protected override async Task<EmptyCommandResult> Validate(EditCarWashCommandArgs arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };

            _carWash = await _carWashService.GetCarWashAsync(arguments.CurrentUserInfo.CarWashId.Value);

            if (_carWash == null)
                return result.AddError(ErrorConstants.CarWash.CAR_WASH_NOT_FOUND, 404);

            return result;
        }
    }
}
