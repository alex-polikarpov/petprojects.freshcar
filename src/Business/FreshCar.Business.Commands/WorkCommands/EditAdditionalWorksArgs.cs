﻿using System.Collections.Generic;

namespace FreshCar.Business.Commands.WorkCommands
{
    public class EditAdditionalWorksArgs : BaseCommandArguments
    {
        public EditAdditionalWorkArgs[] CarWashAdditionalWorks { get; set; }
    }
}
