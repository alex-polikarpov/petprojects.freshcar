﻿using System.Collections.Generic;
using System.Linq;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;
using System.Threading.Tasks;

namespace FreshCar.Business.Commands.WorkCommands
{
    internal class EditWorkTariffsCommand : BaseCommand<EditWorkTariffsArguments, EmptyCommandResult>
    {
        private readonly IUnitOfWork _uow;
        private readonly IWorkService _workService;
        private Work _work;
        private Tariff[] _tariffs;

        public EditWorkTariffsCommand(IUnitOfWork uow, IWorkService workService, ILoggerUtility logger)
           : base(logger)
        {
            _uow = uow;
            _workService = workService;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(EditWorkTariffsArguments arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };

            foreach (var tariffFromArgs in arguments.Tariffs)
            {
                var tariff = _tariffs.FirstOrDefault(x => x.Id == tariffFromArgs.TariffId);
                tariff.Cost = tariffFromArgs.Cost;
                tariff.Duration = tariffFromArgs.Duration;
            }

            await _uow.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(EditWorkTariffsArguments arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };

            DistinctTariffs(arguments);

            _work = await _workService.GetMainWorkAsync(arguments.WorkId, arguments.CurrentUserInfo.CompanyId.Value);

            if (_work == null)
                return result.AddError(ErrorConstants.Work.WORK_NOT_FOUND, 2001);

            _tariffs = await _workService.GetTariffsAsync(arguments.CurrentUserInfo.CarWashId.Value, arguments.WorkId, false);

            foreach (var tariffFromArgs in arguments.Tariffs)
            {
                var contains = _tariffs.Any(x => x.Id == tariffFromArgs.TariffId);
                if (!contains)
                {
                    return result.AddError(ErrorConstants.Tariff.TARIFF_NOT_FOUND, 404);
                }
            }

            return result;
        }

        private void DistinctTariffs(EditWorkTariffsArguments arguments)
        {
            var tariffsId = arguments.Tariffs.Select(x => x.TariffId).Distinct().ToList();
            var tariffs = new List<EditTariffArguments>();
            foreach (var id in tariffsId)
            {
                var tariff = arguments.Tariffs.FirstOrDefault(x => x.TariffId == id);
                tariffs.Add(tariff);
            }
            arguments.Tariffs = tariffs.ToArray();
        }
    }
}
