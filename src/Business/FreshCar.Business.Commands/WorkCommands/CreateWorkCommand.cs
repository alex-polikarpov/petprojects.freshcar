﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.WorkCommands
{
    /// <summary>
    /// Команда создания новой услуги
    /// </summary>
    internal class CreateWorkCommand : BaseCommand<CreateWorkArguments, EmptyCommandResult>
    {
        private readonly IWorkService _workService;
        private readonly IUnitOfWork _uow;

        public CreateWorkCommand(IUnitOfWork uow, ILoggerUtility logger, IWorkService workService)
           : base(logger)
        {
            _uow = uow;
            _workService = workService;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(CreateWorkArguments arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };
            var work = CreateWork(arguments);
            work.Tariffs = CreateTariffs(arguments);
            var repository = _uow.GetRepository<Work>();
            repository.Add(work);
            await _uow.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(CreateWorkArguments arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };

            var workIsExists = await _workService
                .MainWorkIsExistAsync(arguments.CurrentUserInfo.CompanyId.Value, arguments.Name);

            if (workIsExists)
                return result.AddError(ErrorConstants.Work.WORK_ALREADY_EXIST, 2004);

            return result;
        }

        private Work CreateWork(CreateWorkArguments arguments)
        {
            var work = new Work
            {
                Name = arguments.Name,
                WorkState = WorkState.Active,
                WorkType = WorkType.MainCreatedByUser,
                CompanyId = arguments.CurrentUserInfo.CompanyId                
            };

            return work;
        }

        private List<Tariff> CreateTariffs(CreateWorkArguments arguments)
        {
            var tariffs = new List<Tariff>();

            foreach (CarType carType in Enum.GetValues(typeof(CarType)))
            {
                var tariff = new Tariff()
                {
                    CarType = carType,
                    Duration = 30,
                    Cost = 300,
                    CarWashId = arguments.CurrentUserInfo.CarWashId.Value                    
                };
                tariffs.Add(tariff);
            }
            return tariffs;
        }
    }
}
