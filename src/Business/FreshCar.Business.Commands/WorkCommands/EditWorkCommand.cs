﻿using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.WorkCommands
{
    internal class EditWorkCommand : BaseCommand<EditWorkCommandArgs, EmptyCommandResult>
    {
        private readonly IWorkService _workService;
        private readonly IUnitOfWork _unitOfWork;
        private Work _work;

        public EditWorkCommand(ILoggerUtility loggerUtility,
            IWorkService workService,
            IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _workService = workService;
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(EditWorkCommandArgs arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };
            _work.Name = arguments.Name;
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(EditWorkCommandArgs arguments)
        {
            var result = new EmptyCommandResult {IsSuccess = true};

            _work = await _workService.GetMainWorkAsync(arguments.WorkId, arguments.CurrentUserInfo.CompanyId.Value);

            if (_work == null)
                return result.AddError(ErrorConstants.Work.WORK_NOT_FOUND, 404);

            var canEditName = await _workService.MainWorkCanEditNameAsync(
                arguments.CurrentUserInfo.CompanyId.Value, arguments.WorkId, arguments.Name);

            if (!canEditName)
            {
                return result.AddError(ErrorConstants.Work.WORK_ALREADY_EXIST, 400);
            }

            return result;
        }
    }
}
