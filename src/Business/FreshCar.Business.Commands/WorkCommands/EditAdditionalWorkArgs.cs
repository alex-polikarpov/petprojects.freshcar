﻿namespace FreshCar.Business.Commands.WorkCommands
{
    public class EditAdditionalWorkArgs : BaseCommandArguments
    {
        public int WorkId { get; set; }

        public bool IsActive { get; set; }
    }
}
