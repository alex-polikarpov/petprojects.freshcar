﻿namespace FreshCar.Business.Commands.WorkCommands
{
    /// <summary>
    /// Аргументы для создания услуги
    /// </summary>
    public class CreateWorkArguments : BaseCommandArguments
    {
        /// <summary>
        /// Название новой услуги
        /// </summary>
        public string Name { get; set; }
    }
}
