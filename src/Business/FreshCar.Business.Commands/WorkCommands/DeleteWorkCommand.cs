﻿using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.WorkCommands
{
    /// <summary>
    /// Команда удаления основной услуги
    /// </summary>
    internal class DeleteWorkCommand : BaseCommand<DeleteWorkArguments, EmptyCommandResult>
    {
        private readonly IWorkService _workService;
        private readonly IUnitOfWork _uow;
        private Work _work;

        public DeleteWorkCommand(IUnitOfWork uow, ILoggerUtility logger, IWorkService workService)
            : base(logger)
        {
            _workService = workService;
            _uow = uow;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(DeleteWorkArguments arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };

            _work.WorkState = WorkState.Deleted;
            await _uow.SaveChangesAsync();

            return result;
        }

        protected async override Task<EmptyCommandResult> Validate(DeleteWorkArguments arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };
            _work = await _workService.GetMainWorkAsync(arguments.Id, arguments.CurrentUserInfo.CompanyId.Value);

            if (_work == null)
                return result.AddError(ErrorConstants.Work.WORK_NOT_FOUND, 3001);

            if (_work.WorkType != WorkType.MainCreatedByUser)
                return result.AddError(ErrorConstants.Work.WORK_CANT_BE_MODIFIED, 3002);

            return result;
        }
    }
}
