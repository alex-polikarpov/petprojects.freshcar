﻿namespace FreshCar.Business.Commands.WorkCommands
{
    public class EditWorkCommandArgs : BaseCommandArguments
    {
        public int WorkId { get; set; }

        public string Name { get; set; }
    }
}
