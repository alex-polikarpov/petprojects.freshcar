﻿using System;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Cryptography;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Команда для добавления нового сотрудника
    /// </summary>
    internal class CreateNewEmployeeCommand : BaseCommand<CreateNewEmployeeCommandArgs, EmptyCommandResult>
    {
        private readonly IAccountService _accountService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICryptographyUtility _cryptographyUtility;

        public CreateNewEmployeeCommand(IAccountService accountService,
            IUnitOfWork unitOfWork, 
            ICryptographyUtility cryptographyUtility,
            ILoggerUtility logger) : base(logger)
        {
            _accountService = accountService;
            _unitOfWork = unitOfWork;
            _cryptographyUtility = cryptographyUtility;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(CreateNewEmployeeCommandArgs arguments)
        {
            var result = new EmptyCommandResult()
            {
                IsSuccess = true
            };

            var accountRepository = _unitOfWork.GetRepository<Account>();
            var employeeRepository = _unitOfWork.GetRepository<CarWashEmployee>();

            try
            {
                _unitOfWork.BeginTransaction();
                var employee = new Employee()
                {
                    FirstName = arguments.FirstName,
                    LastName = arguments.LastName,
                    MiddleName = arguments.MiddleName,
                    Phone = arguments.Phone,
                    CompanyId = arguments.CurrentUserInfo.CompanyId.Value
                };

                var account = new Account()
                {
                    Employee = employee,
                    Id = Guid.NewGuid(),
                    Email = arguments.Login.ToLower(),
                    AccountState = AccountState.Verified,
                    PasswordHash = _cryptographyUtility.GetHash(arguments.Password)
                };

                accountRepository.Add(account);
                employeeRepository.Add(new CarWashEmployee()
                {
                    CarWashId = arguments.CurrentUserInfo.CarWashId.Value,
                    EmployeeRole = EmployeeRole.Administrator
                });

                await _unitOfWork.SaveChangesAsync();
                _unitOfWork.CommitTransaction();
            }
            catch (Exception)
            {
                _unitOfWork.RollBackTransaction();
                throw;
            }

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(CreateNewEmployeeCommandArgs arguments)
        {
            var result = new EmptyCommandResult()
            {
                IsSuccess = true
            };

            if (await _accountService.UserIsExistsAsync(arguments.Login))
                return result.AddError(ErrorConstants.Account.EMAIL_ALREADY_USED, 1000);

            return result;
        }
    }
}
