﻿using System;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Cryptography;
using FreshCar.Utils.Extensions;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Команда изменения сотрудника
    /// </summary>
    public class EditEmployeeCommand : BaseCommand<EditEmployeeCommandArgs, EmptyCommandResult>
    {
        private readonly ICryptographyUtility _cryptographyUtility;
        private readonly IEmployeeService _employeeService;
        private readonly IAccountService _accountService;
        private readonly IUnitOfWork _unitOfWork;
        private Employee _employee;

        public EditEmployeeCommand(
            ICryptographyUtility cryptographyUtility,
            IEmployeeService employeeService,
            IAccountService accountService,
            IUnitOfWork unitOfWork,
            ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            _cryptographyUtility = cryptographyUtility;
            _employeeService = employeeService;
            _accountService = accountService;
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(EditEmployeeCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            try
            {
                _unitOfWork.BeginTransaction();
                _employee.FirstName = arguments.FirstName;
                _employee.LastName = arguments.LastName;
                _employee.MiddleName = arguments.MiddleName;
                _employee.Phone = arguments.Phone;

                var account = await _accountService.GetEmployeeAccount(arguments.EmployeeId);                
                account.Email = arguments.Login.ToLower();                    

                if (!arguments.Password.IsNullOrWhiteSpace())
                    account.PasswordHash = _cryptographyUtility.GetHash(arguments.Password);

                await _unitOfWork.SaveChangesAsync();
                _unitOfWork.CommitTransaction();                
            }
            catch (Exception)
            {
                _unitOfWork.RollBackTransaction();
                throw;
            }

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(EditEmployeeCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            _employee = await _employeeService.GetEmployee(arguments.EmployeeId);

            if (_employee == null || _employee.CompanyId != arguments.CurrentUserInfo.CompanyId.Value)
                return result.AddError(ErrorConstants.Employee.EMPLOYEE_NOT_FOUND, 1000);

            var link = await _employeeService.GetCarWashEmployee(
               arguments.EmployeeId, arguments.CurrentUserInfo.CarWashId.Value);

            if (link != null && link.EmployeeRole == EmployeeRole.Director)
                return result.AddError(ErrorConstants.Employee.DIRECTOR_CANT_EDITED, 1002);

            if (!await _accountService.EmployeeCanChangeLogin(arguments.EmployeeId, arguments.Login))
                return result.AddError(ErrorConstants.Account.EMAIL_ALREADY_USED, 1000);

            return result;
        }
    }
}
