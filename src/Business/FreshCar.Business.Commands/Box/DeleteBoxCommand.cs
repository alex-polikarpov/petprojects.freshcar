﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.Box
{
    internal class DeleteBoxCommand : BaseCommand<DeleteBoxCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICarWashBoxService _carWashBoxService;
        private readonly ICarWashService _carWashService;
        private CarWashBox _box;

        public DeleteBoxCommand(ILoggerUtility loggerUtility,
            IUnitOfWork unitOfWork,
            ICarWashBoxService carWashBoxService,
            ICarWashService carWashService) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
            _carWashBoxService = carWashBoxService;
            _carWashService = carWashService;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(DeleteBoxCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            _box.State = CarWashBoxState.Deleted;
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(DeleteBoxCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            _box = await _carWashBoxService.GetBox(arguments.Id, false);

            if (_box == null || _box.CarWashId != arguments.CurrentUserInfo.CarWashId.Value)
            {
                return result.AddError(ErrorConstants.CarWash.BOX_NOT_FOUND, 404);
            }

            var boxesCount = await _carWashService.GetBoxesCount(arguments.CurrentUserInfo.CarWashId.Value);

            if (boxesCount == 1)
            {
                return result.AddError(ErrorConstants.CarWash.CANT_DELETE_LAST_BOX, 1000);
            }

            return result;
        }
    }
}
