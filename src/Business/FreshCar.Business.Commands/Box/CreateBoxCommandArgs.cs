﻿namespace FreshCar.Business.Commands.Box
{
    public class CreateBoxCommandArgs : BaseCommandArguments
    {
        public string Name { get; set; }
    }
}
