﻿using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.Box
{
    internal class CreateBoxCommand : BaseCommand<CreateBoxCommandArgs, EmptyCommandResult>
    {
        private readonly ICarWashService _carWashService;
        private readonly IUnitOfWork _unitOfWork;

        public CreateBoxCommand(ILoggerUtility loggerUtility,
            ICarWashService carWashService,
            IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _carWashService = carWashService;
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(CreateBoxCommandArgs arguments)
        {
            var repository = _unitOfWork.GetRepository<CarWashBox>();
            repository.Add(new CarWashBox()
            {
                CarWashId = arguments.CurrentUserInfo.CarWashId.Value,
                Name = arguments.Name,
                State = CarWashBoxState.Works
            });
            await _unitOfWork.SaveChangesAsync();
            return new EmptyCommandResult() { IsSuccess = true };
        }

        protected override async Task<EmptyCommandResult> Validate(CreateBoxCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            var boxesCount = await _carWashService.GetBoxesCount(arguments.CurrentUserInfo.CarWashId.Value);
            if (boxesCount >= 15)
            {
                return result.AddError(ErrorConstants.CarWash.MANY_BOXES, 1000);
            }

            return result;
        }
    }
}
