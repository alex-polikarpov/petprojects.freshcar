﻿namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Аргументы команды регистрации пользователя
    /// </summary>
    public class RegisterUserCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Улица
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Номер дома
        /// </summary>
        public string House { get; set; }

        /// <summary>
        /// Название фирмы (и название мойки)
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Почта (логин)
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }
    }
}