﻿namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Аргументы для команды поиска аккаунта
    /// </summary>
    public class AuthorizeUserCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Логин (почта)
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль, переданный пользователем
        /// </summary>
        public string Password { get; set; }
    }
}
