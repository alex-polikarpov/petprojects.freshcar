﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Cryptography;
using FreshCar.Utils.Geolocation;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Команда регистрации пользователя
    /// </summary>
    internal class RegisterUserCommand : BaseCommand<RegisterUserCommandArgs, EmptyCommandResult>
    {
        private readonly IGeolocationUtility _geolocationUtility;        
        private readonly IAccountService _accountService;
        private readonly ICarWashService _carWashService;
        private readonly IWorkService _workService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICryptographyUtility _cryptographyUtility;
        private GeolocationInfo _geolocationInfo;

        public RegisterUserCommand(
            ILoggerUtility loggerUtility, 
            IGeolocationUtility geolocationUtility,
            IAccountService accountService,
            ICarWashService carWashService,
            IWorkService workService,
            IUnitOfWork unitOfWork, 
            ICryptographyUtility cryptographyUtility) : base(loggerUtility)
        {
            _geolocationUtility = geolocationUtility;
            _accountService = accountService;
            _carWashService = carWashService;
            _workService = workService;
            _unitOfWork = unitOfWork;
            _cryptographyUtility = cryptographyUtility;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(RegisterUserCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            try
            {
                var company = CreateCompany(arguments);
                var employee = CreateEmployee(arguments, company);
                var account = CreateNewAccount(arguments);
                account.Employee = employee;
                var carWash = CreateCarWash(arguments, company);
                
                account.Employee = employee;
                var carWashEmployee = CreateCarWashEmployee(arguments, carWash, employee);
                var carWashBox = CreateBox(arguments, carWash);
                var tariffs = await CreateTariffs(arguments, carWash);

                _unitOfWork.BeginTransaction();
                await _unitOfWork.SaveChangesAsync();
                _unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {                
                _unitOfWork.RollBackTransaction();
                throw;
            }

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(RegisterUserCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            //Проверяем уникальность логина
            var exists = await _accountService.UserIsExistsAsync(arguments.Email);

            if (exists)
            {
                return result.AddError(ErrorConstants.Account.EMAIL_ALREADY_USED, 10003);
            }

            //Пытаемся получить координаты адреса
            try
            {
                _geolocationInfo = await _geolocationUtility.GetLocationByAddress(
                    arguments.City, arguments.Street, arguments.House);

                if (_geolocationInfo == null)
                    return result.AddError(ErrorConstants.Account.GEOLOCATION_ERROR, 10002);
            }
            catch (Exception ex)
            {
                return result.AddError(ErrorConstants.Account.GEOLOCATION_ERROR, 10002);
            }

            //проверяем уникальность адреса по координатам. по координатам надежнее потому что 
            //если будут небольшие изменения типа пробел и т.д., их не заметим
            var addressUsed = await _carWashService
                .AddressAlreadyExistsAsync(_geolocationInfo.Latitude, _geolocationInfo.Longitude);

            if (addressUsed)
            {
                return result.AddError(ErrorConstants.Account.ADDRESS_ALREADY_USED, 10004);
            }

            return result;
        }

        #region Вспомогательные методы

        public Account CreateNewAccount(RegisterUserCommandArgs arguments)
        {
            var accountRepository = _unitOfWork.GetRepository<Account>();

            #warning TODO поменять перед релизом AccountState = AccountState.NotVerified,
            var newAccount = new Account()
            {
                Id = Guid.NewGuid(),
                AccountState = AccountState.Verified,
                Email = arguments.Email.ToLower(),
                PasswordHash = _cryptographyUtility.GetHash(arguments.Password)
            };

            accountRepository.Add(newAccount);
            return newAccount;
        }


        private Company CreateCompany(RegisterUserCommandArgs arguments)
        {
            var companyRepository = _unitOfWork.GetRepository<Company>();

            //по дефолту зачисляем 1000 рублей

            var company = new Company()
            {
                Balance = 1000,
                Name = arguments.CompanyName
            };

            companyRepository.Add(company);
            return company;
        }


        private CarWash CreateCarWash(RegisterUserCommandArgs arguments, Company company)
        {
            var repository = _unitOfWork.GetRepository<CarWash>();

            #warning TODO ПОМЕНЯТЬ ПЕРЕД РЕЛИЗОМ State = CarWashState.NotWorks
            var carWash = new CarWash()
            {
                Phone = arguments.Phone,
                City = arguments.City,
                HouseNumber = arguments.House,
                Street = arguments.Street,
                Latitude = _geolocationInfo.Latitude,
                Longitude = _geolocationInfo.Longitude,
                Name = arguments.CompanyName,
                State = CarWashState.Works, // пока на проверке, значит мойка не должна работать
                StartTimeOfWork = new TimeSpan(21, 0, 0), // UTC ВРЕМЯ
                EndTimeOfWork = new TimeSpan(20, 59, 59),
                Company = company,
            };

            repository.Add(carWash);
            return carWash;
        }


        private Employee CreateEmployee(RegisterUserCommandArgs arguments, Company company)
        {
            var repository = _unitOfWork.GetRepository<Employee>();

            var employee = new Employee()
            {
                Phone = arguments.Phone,
                Company = company
            };

            repository.Add(employee);
            return employee;
        }


        private CarWashEmployee CreateCarWashEmployee(RegisterUserCommandArgs arguments, CarWash carWash, Employee employee)
        {
            var repository = _unitOfWork.GetRepository<CarWashEmployee>();

            var carWashEmployee = new CarWashEmployee()
            {
                CarWash = carWash,
                Employee = employee,
                EmployeeRole = EmployeeRole.Director,
            };

            repository.Add(carWashEmployee);
            return carWashEmployee;
        }


        private CarWashBox CreateBox(RegisterUserCommandArgs arguments, CarWash carWash)
        {
            var repository = _unitOfWork.GetRepository<CarWashBox>();

            var carWashBox = new CarWashBox()
            {
                CarWash = carWash,
                SortOrder = 1,
                Name = "Бокс 1",
                State = CarWashBoxState.Works,
            };

            repository.Add(carWashBox);
            return carWashBox;
        }


        private async Task<List<Tariff>> CreateTariffs(RegisterUserCommandArgs arguments, CarWash carWash)
        {
            var repository = _unitOfWork.GetRepository<Tariff>();
            var result = new List<Tariff>();
            var systemWorks = await _workService.GetMainSystemWorksAsync();

            foreach (var systemWork in systemWorks)
            {
                foreach (CarType carType in Enum.GetValues(typeof(CarType)))
                {
                    var tariff = new Tariff()
                    {
                        WorkId = systemWork.Id,
                        CarWash = carWash,
                        CarType = carType,
                        Duration = 30,
                        Cost = 300
                    };
                    repository.Add(tariff);
                    result.Add(tariff);
                }
            }

            return result;
        }

        #endregion
    }
}
