﻿using FreshCar.Core.Services;

namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Базовый класс аргументов команды
    /// </summary>
    public abstract class BaseCommandArguments : ICommandArguments
    {
        /// <summary>
        /// Основная информация о текущем пользователе
        /// </summary>
        public virtual UserInfo CurrentUserInfo { get; set; }
    }
}