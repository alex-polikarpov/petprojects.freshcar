﻿namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Базовый класс результата команды
    /// </summary>
    public abstract class BaseCommandResult : ICommandResult
    {
        /// <summary>
        /// True, если команда выполнилась успешно, иначе false
        /// </summary>
        public virtual bool IsSuccess { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public virtual string ErrorMessage { get; set; }

        /// <summary>
        /// Код ошибки
        /// </summary>
        public virtual int ErrorCode { get; set; }
    }
}
