﻿using System.Threading.Tasks;

namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Бизнес команда, выполняющая какие-либо действия
    /// </summary>
    /// <typeparam name="TArguments">Тип входных аргументов для команды</typeparam>
    /// <typeparam name="TResult">Результирующий тип команды</typeparam>    
    public interface ICommand<TArguments, TResult>
        where TArguments : class, ICommandArguments
        where TResult : class, ICommandResult, new()
    {
        /// <summary>
        /// Выполняет команду
        /// </summary>
        /// <param name="arguments">Входные параметры</param>
        Task<TResult> Execute(TArguments arguments);
    }
}