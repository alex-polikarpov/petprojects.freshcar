﻿using System;
using System.Security.Principal;
using Entities = FreshCar.Core.Entities;

namespace FreshCar.Business.Commands.Extensions.Rules
{
    /// <summary>
    /// Бизнес правила для автомойки
    /// </summary>
    public static class CarWashBusinessRules
    {
        //сколько минут в сутках может не работать автомойка, чтобы ее считать круглосуточной
        private const int AROUND_THE_CLOCK_MINUTES = 30;
        private const int MINUTES_IN_DAY = 1440;

        /// <summary>
        /// Проверяет работает ли автомойка круглосуточно. Возвращает
        /// true если работает круглосуточно, иначе false
        /// </summary>
        public static bool WorkAroundTheClock(this Entities.CarWash carWash)
        {
            var startTime = new TimeSpan(carWash.StartTimeOfWork.Hours, carWash.StartTimeOfWork.Minutes,0);
            var endTime = new TimeSpan(carWash.EndTimeOfWork.Hours, carWash.EndTimeOfWork.Minutes, 0);

            if (TimesIsEquals(startTime, endTime))
                return true;

            var startMinutes = startTime.Hours*60 + startTime.Minutes;
            var endMinutes = endTime.Hours*60 + endTime.Minutes;

            //если время начала меньше времени конца, значит промежуток непрерывный
            //и можно просто вычислить сколько минут в сутках работает мойка 
            if (startTime < endTime)
            {
                //Проверяем сколько минут в сутках НЕ работает автомойка
                var workMinutes = MINUTES_IN_DAY - endMinutes - startMinutes;
                return (MINUTES_IN_DAY - workMinutes) <= AROUND_THE_CLOCK_MINUTES;
            }

            //если время начала больше времени конца, значит тут сложнее
            //например это может быть, если укажут время работы с 10:00 до 05:00
            //это будет означать с 10 утра до 5 утра след. дня
            //тут т.е. 2 промежутка времени работы получается от времени начала до 0:00 и от 0:00 до
            //времени конца.
            var workingMinutes = MINUTES_IN_DAY - startMinutes + endMinutes;
            return (MINUTES_IN_DAY - workingMinutes) <= AROUND_THE_CLOCK_MINUTES;
        }

        /// <summary>
        /// Проверяет работает ли автомойка в указанный промежуток времени
        /// </summary>
        /// <param name="timeFrom">Время начала для проверки</param>
        /// <param name="timeTo">Время окончания для проверки</param>
        public static bool IsWorking(this Entities.CarWash carWash, TimeSpan timeFrom, TimeSpan timeTo)
        {
            if (carWash.WorkAroundTheClock())
                return true;

            var workingStartTime = new TimeSpan(carWash.StartTimeOfWork.Hours, carWash.StartTimeOfWork.Minutes, 0);
            var workingEndTime = new TimeSpan(carWash.EndTimeOfWork.Hours, carWash.EndTimeOfWork.Minutes, 0);

            //секунды можно не учитывать
            timeFrom = new TimeSpan(timeFrom.Hours, timeFrom.Minutes, 0);
            timeTo = new TimeSpan(timeTo.Hours, timeTo.Minutes, 0);

            //алгоритм для времени работы с переходом на след. день и без разные
            if (workingStartTime < workingEndTime)
            {
                var working = (timeFrom >= workingStartTime && timeFrom < workingEndTime) &&
                              (timeTo >= workingStartTime && timeTo <= workingEndTime);

                if (timeFrom <= timeTo)
                    return working;

                //если время начала и окончания попадают в рабочий промежуток, но дата начала меньше даты конца
                //значит в работу попадают также НЕ рабочие промежутки
                return false;
            }

            //проверяем попало ли время в НЕ рабочий промежуток
            var notWorking = (timeFrom >= workingEndTime && timeFrom < workingStartTime) ||
                                 (timeTo > workingEndTime && timeTo < workingStartTime);

            //если одно из времен попадает в период НЕ рабочего времени, возвращаем ошибку
            if (notWorking)
                return false;

            if (timeFrom <= timeTo)
            {
                notWorking = (timeFrom <= workingEndTime && timeTo > workingEndTime);
                return !notWorking;
            }

            notWorking = (timeFrom <= workingEndTime ||
                          (timeFrom >= workingStartTime && timeTo > workingEndTime));
            
            return !notWorking;
        }

        private static bool TimesIsEquals(TimeSpan a, TimeSpan b)
        {
            return a.Hours == b.Hours && a.Minutes == b.Minutes;
        }
    }
}
