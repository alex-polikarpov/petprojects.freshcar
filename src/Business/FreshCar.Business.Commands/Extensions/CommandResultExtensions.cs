﻿namespace FreshCar.Business.Commands.Extensions
{
    public static class CommandResultExtensions
    {
        public static TResult AddError<TResult>(this TResult result, string error, int errorCode)
            where TResult : ICommandResult
        {
            if (result == null)
                return result;

            result.ErrorMessage = error;
            result.IsSuccess = false;
            result.ErrorCode = errorCode;
            return result;
        }
    }
}
