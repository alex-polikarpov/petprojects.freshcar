﻿using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.Order
{
    /// <summary>
    /// Команда удаления заказа
    /// </summary>
    internal class DeleteOrderCommand : BaseCommand<DeleteOrderCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderService _orderService;
        private Core.Entities.Order _order;

        public DeleteOrderCommand(ILoggerUtility loggerUtility,
            IUnitOfWork unitOfWork, IOrderService orderService)
            : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
            _orderService = orderService;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(DeleteOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            _order.OrderState = OrderState.Deleted;
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(DeleteOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            _order = await _orderService.GetActiveOrder(arguments.CurrentUserInfo.CarWashId.Value, arguments.OrderId, false);

            if (_order == null)
            {
                return result.AddError(ErrorConstants.Order.ORDER_NOT_FOUND, 404);
            }

            if (_order.OrderState == OrderState.FinishedAndNotPaid)
            {
                return result.AddError(ErrorConstants.Order.FINISHED_ORDER_CANT_DELETED, 1000);
            }

            return result;
        }
    }
}