﻿namespace FreshCar.Business.Commands.Order
{
    /// <summary>
    /// Аргументы команды удаления заказа
    /// </summary>
    public class DeleteOrderCommandArgs :BaseCommandArguments
    {
        public long OrderId { get; set; }
    }
}
