﻿using System;
using FreshCar.Core.Entities;

namespace FreshCar.Business.Commands.Order
{
    /// <summary>
    /// Аргументы команды редактирования заказа от имени администратора
    /// </summary>
    public class EditOrderFromAdminArgs : BaseCommandArguments
    {
        /// <summary>
        /// Айди заказа
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Id тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Id бокса мойки, где будет происходить выполнение заказа
        /// </summary>
        public int CarWashBoxId { get; set; }

        /// <summary>
        /// Дата и время, когда начнется выполнение заказа (обязательно в UTC)
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Дата и время, окончания заказа (в UTC)
        /// </summary>
        public DateTime DateTo { get; set; }

        /// <summary>
        /// Номер машины клиента
        /// </summary>
        public string CarNumber { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        public string ClientPhone { get; set; }

        /// <summary>
        /// Марка автомобиля
        /// </summary>
        public string CarMake { get; set; }

        /// <summary>
        /// Стоимость заказа
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Состояние заказа
        /// </summary>
        public OrderState OrderState { get; set; }
    }
}
