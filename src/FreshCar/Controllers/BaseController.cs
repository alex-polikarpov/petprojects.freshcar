﻿using System;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using FreshCar.Business.Commands;
using FreshCar.Constants;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Helpers;
using FreshCar.Utils.Extensions;

namespace FreshCar.Controllers
{
    /// <summary>
    /// Базовый контроллер
    /// </summary>
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// Текущий пользователь
        /// </summary>
        protected virtual UserInfo CurrentUser
        {
            get
            {
                var result = new UserInfo();
                result.AccountId = GetCurrentAccountId();

                if (!result.AccountId.HasValue)
                    return result;

                result.Role = GetCurrentUserRole();
                result.CarWashId = GetClaimsValue(CustomClaimsConstants.CAR_WASH_ID).GetIntValue();
                result.CompanyId = GetClaimsValue(CustomClaimsConstants.COMPANY_ID).GetIntValue();
                result.EmployeeId = GetClaimsValue(CustomClaimsConstants.EMPLOYEE_ID).GetIntValue();
                result.Email = GetClaimsValue(ClaimTypes.Email);

                return result;
            }
        }

        /// <summary>
        /// Получает Email текущего аккаунта из куков
        /// </summary>
        private Guid? GetCurrentAccountId()
        {
            Guid? result = null;
            var value = GetClaimsValue(ClaimTypes.NameIdentifier);

            Guid g;
            if (!value.IsNullOrWhiteSpace() && Guid.TryParse(value, out g))
            {
                result = g;
            }

            return result;
        }

        /// <summary>
        /// Роль текущего юзера из куков
        /// </summary>
        private EmployeeRole? GetCurrentUserRole()
        {
            EmployeeRole? result = null;
            var value = GetClaimsValue(ClaimTypes.Role);
            EmployeeRole role;

            if (!value.IsNullOrWhiteSpace() && Enum.TryParse(value, true, out role))
            {
                result = role;
            }

            return result;
        }

        /// <summary>
        /// Создает класс аргументов команды и заполняет текущего юзера и роль
        /// </summary>
        /// <typeparam name="TCommandArgs">Тип аргументов команды, который нужно создать</typeparam>
        protected virtual TCommandArgs CreateCommandArgs<TCommandArgs>()
            where TCommandArgs : BaseCommandArguments, new()
        {
            var result = new TCommandArgs();
            result.CurrentUserInfo = CurrentUser;
            return result;
        }

        /// <summary>
        /// Получает значение из куков
        /// </summary>
        private string GetClaimsValue(string claimsType)
        {
            string result = null;
            var claimsPrincipal = HttpContext?.GetOwinContext()?.Authentication?.User;

            if (claimsPrincipal != null)
            {
                var idClaim = claimsPrincipal.FindFirst(claimsType);
                result = idClaim?.Value;                
            }

            return result;
        }

        /// <summary>
        /// Переопределяет метод Json, чтобы он сериализовал с помощью NewtonSoft, а не стандартного сериализатора
        /// </summary>
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding)
        {
            return new NewtonsoftJsonResult
            {
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                Data = data
            };
        }

        /// <summary>
        /// Переопределяет метод Json, чтобы он сериализовал с помощью NewtonSoft, а не стандартного сериализатора
        /// </summary>
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new NewtonsoftJsonResult
            {
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                Data = data,
                JsonRequestBehavior = behavior
            };
        }
    }
}