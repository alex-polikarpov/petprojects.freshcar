﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using FreshCar.Attributes;
using FreshCar.Business.Commands;
using FreshCar.Business.Commands.Box;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models.CarWashBoxModels;
using FreshCar.Requests.Box;
using FreshCar.Responses;
using FreshCar.Responses.Box;

namespace FreshCar.Controllers
{
    /// <summary>
    /// Контроллер для работы с боксами автомойки
    /// </summary>
    [Authorize]
    public class BoxController : BaseController
    {
        private readonly ICarWashService _carWashService;

        public BoxController(ICarWashService carWashService)
        {
            _carWashService = carWashService;
        }

        public async Task<ActionResult> Index()
        {
            var boxes = await _carWashService.GetCarWashBoxesAsync(CurrentUser.CarWashId.Value, true);
            return View(boxes);
        }

        /// <summary>
        /// Получает список рабочих боксов автомойки
        /// </summary>
        [HttpGet, AjaxOnly]
        public async Task<JsonResult> GetBoxes()
        {
            var boxes = await _carWashService.GetCarWashBoxesAsync(CurrentUser.CarWashId.Value);
            var response = new GetBoxesResponse();
            response.Boxes = boxes.Select(x => new CarWashBoxShortInfoModel()
            {
                Name = x.Name,
                Id = x.Id
            }).ToArray();

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateRequest, AjaxOnly]
        public async Task<JsonResult> CreateBox(EditBoxRequest request)
        {
            var args = CreateCommandArgs<CreateBoxCommandArgs>();
            args.Name = request.Name?.Trim();
            var command = DependencyResolver.Current
                .GetService<ICommand<CreateBoxCommandArgs, EmptyCommandResult>>();
            var commandResult = await command.Execute(args);

            if (commandResult.IsSuccess)
            {
                TempData["Success"] = "Новый бокс успешно добавлен";
            }

            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        [HttpPost, ValidateRequest, AjaxOnly]
        public async Task<JsonResult> DeleteBox(int id)
        {
            var args = CreateCommandArgs<DeleteBoxCommandArgs>();
            args.Id = id;
            var command = DependencyResolver.Current
                .GetService<ICommand<DeleteBoxCommandArgs, EmptyCommandResult>>();
            var commandResult = await command.Execute(args);

            if (commandResult.IsSuccess)
            {
                TempData["Success"] = "Бокс успешно удален";
            }

            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        [HttpPost, ValidateRequest, AjaxOnly]
        public async Task<JsonResult> EditBox(EditBoxRequest request)
        {
            var args = CreateCommandArgs<EditBoxCommandArgs>();
            args.Name = request.Name?.Trim();
            args.Id = request.Id;
            var command = DependencyResolver.Current
                .GetService<ICommand<EditBoxCommandArgs, EmptyCommandResult>>();
            var commandResult = await command.Execute(args);

            if (commandResult.IsSuccess)
            {
                TempData["Success"] = "Бокс успешно изменен";
            }

            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }
    }
}