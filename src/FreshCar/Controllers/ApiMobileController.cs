﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using FreshCar.Attributes;
using FreshCar.Business.Commands;
using FreshCar.Business.Commands.Order;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models.WorkModels;
using FreshCar.Requests.ApiMobile;
using FreshCar.ResponseBuilders.ApiMobile;
using FreshCar.Responses;
using FreshCar.Responses.ApiMobile;
using FreshCar.Utils.Extensions;
using FreshCar.Utils.Mapping;

namespace FreshCar.Controllers
{
    /// <summary>
    /// Контроллер для мобильного приложения
    /// </summary>
    [DenyAuthorized]
    public class ApiMobileController : BaseController
    {
        private readonly ICarWashService _carWashService;
        private readonly IMapperUtility _mapperUtility;
        private readonly IWorkService _workService;

        public ApiMobileController(
            ICarWashService carWashService,
            IMapperUtility mapperUtility,
            IWorkService workService)
        {
            _carWashService = carWashService;
            _mapperUtility = mapperUtility;
            _workService = workService;
        }

        /// <summary>
        /// Возвращает список автомоек с краткой информацией по ним и статусом свободна / занята
        /// </summary>
        [AllowCrossFilter, HttpGet]
        public async Task<JsonResult> GetCarWashes(GetCarWashesRequest request = null)
        {
            var builder = new GetCarWashesResponseBuilder(_carWashService, _mapperUtility);
            return Json(await builder.Build(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Возвращает список основных системных услуг, общих для всех автомоек
        /// </summary>
        [AllowCrossFilter, HttpGet]
        public async Task<JsonResult> GetMainSystemWorks()
        {
            var works = await _workService.GetMainSystemWorksAsync();

            var result = new GetSystemWorksResponse
            {
                Works = _mapperUtility.MapEnumerable<Work, WorkShortInfoModel>(works).ToArray()
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Возвращает список дополнительных системных услуг (кофе, wi-fi, ...)
        /// </summary>
        [AllowCrossFilter, HttpGet]
        public async Task<JsonResult> GetMainAdditionalWorks()
        {
            var works = await _workService.GetAdditionalSystemWorksAsync();

            var result = new GetSystemWorksResponse
            {
                Works = _mapperUtility.MapEnumerable<Work, WorkShortInfoModel>(works).ToArray()
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Возвращает информацию об автомойке
        /// </summary>
        [AllowCrossFilter, HttpGet, ValidateRequest]
        public async Task<JsonResult> GetCarWash(GetCarWashRequest request)
        {
            var builder = new GetCarWashResponseBuilder(_workService, _carWashService, _mapperUtility);
            var response = await builder.Build(request);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Предоставляет тариф на услугу
        /// </summary>
        [AllowCrossFilter, ValidateRequest,  HttpGet]
        public async Task<JsonResult> GetTariff(GetTariffRequest request)
        {
            var builder = new GetTariffResponseBuilder(_workService);
            return Json(await builder.Build(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Метод для записи на автомойку
        /// </summary>
        [AllowCrossFilter, HttpPost, ValidateRequest]
        public async Task<JsonResult> CreateOrder(CreateOrderRequest request)
        {
            request.StartDate = request.StartDate.ToUtc();
            var args = CreateCommandArgs<CreateOrderFromClientArgs>();
            args.CarNumber = request.CarNumber?.Trim();
            args.ClientPhone = request.ClientPhone?.Trim();
            args.StartDate = request.StartDate.ResetSeconds().ToUtc();
            args.TariffId = request.TariffId;

            var command = DependencyResolver.Current
                .GetService<ICommand<CreateOrderFromClientArgs, EmptyCommandResult>>();
            var result = await command.Execute(args);

            return Json(result.ConverToResponse<EmptyResponse>());
        }
    }
}
