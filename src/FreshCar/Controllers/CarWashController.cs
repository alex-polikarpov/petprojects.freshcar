﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using FreshCar.Attributes;
using FreshCar.Business.Commands;
using FreshCar.Core.Services;
using FreshCar.ResponseBuilders.CarWash;
using FreshCar.Responses;
using FreshCar.Extensions;
using FreshCar.Requests.CarWashRequests;
using FreshCar.Utils.Extensions;
using FreshCar.Utils.Mapping;

namespace FreshCar.Controllers
{
    [Authorize]
    public class CarWashController : BaseController
    {
        private readonly ICarWashService _carWashService;
        private readonly IMapperUtility _mapper;

        public CarWashController(ICarWashService carWashService, IMapperUtility mapper)
        {
            _carWashService = carWashService;
            _mapper = mapper;
        }


        public ActionResult Index()
        {
            return View("Index");
        }

        [HttpGet, AjaxOnly]
        public async Task<JsonResult> GetCarWash()
        {
            var builder = new GetCarWashResponseBuilder(_carWashService, _mapper);
            var result = await builder.Build(CurrentUser.CarWashId.Value);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, AjaxOnly, ValidateRequest]
        public async Task<JsonResult> EditCarWash(EditCarWashRequest request)
        {
            var arguments = CreateCommandArgs<EditCarWashCommandArgs>();
            arguments.Name = request.Name?.Trim();
            arguments.Phone = request.Phone?.Trim();
            arguments.StartTimeOfWork = request.StartTimeOfWork.ToUtc().TimeOfDay;
            arguments.EndTimeOfWork = request.EndTimeOfWork.ToUtc().TimeOfDay;

            var command = DependencyResolver.Current
               .GetService<ICommand<EditCarWashCommandArgs, EmptyCommandResult>>();
            var result = await command.Execute(arguments);

            return Json(result.ConverToResponse<EmptyResponse>());
        }
    }
}