﻿using System.Linq;
using FreshCar.Business.Commands.WorkCommands;
using System.Web.Mvc;
using FreshCar.Core.Services;
using System.Threading.Tasks;
using FreshCar.Business.Commands;
using FreshCar.Attributes;
using FreshCar.Utils.Mapping;
using FreshCar.Responses;
using FreshCar.Extensions;
using FreshCar.Models.CarWashAdditionalWorkModels;
using FreshCar.Requests.WorkRequests;
using FreshCar.ResponseBuilders;
using FreshCar.ResponseBuilders.CarWashAdditionalWork;

namespace FreshCar.Controllers
{
    /// <summary>
    /// Контроллер для работы с услугами и тарифами в админке
    /// </summary>
    [Authorize]
    public class WorkController : BaseController
    {
        private readonly IWorkService _workService;
        private readonly IMapperUtility _mapper;

        public WorkController(IWorkService workService, 
            IMapperUtility mapper)
        {
            _workService = workService;
            _mapper = mapper;
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Возвращает все основные услуги автомойки
        /// </summary>
        [HttpGet, AjaxOnly]
        public async Task<JsonResult> GetMainWorks()
        {
            var builder = new GetMainWorksResponseBuilder(_workService, _mapper);
            return Json(await builder.Build(CurrentUser.CompanyId.Value), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Возвращает все доп. услуги автомойки с флагом работает/нет
        /// </summary>
        [HttpGet, AjaxOnly]
        public async Task<JsonResult> GetAdditionalWorks()
        {
            var builder = new GetCarWashAdditionalWorkResponseBuilder(_workService);
            return Json(await builder.Build(CurrentUser), JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// Метод создания новой услуги
        /// </summary>
        [HttpPost, AjaxOnly, ValidateRequest]
        public async Task<JsonResult> CreateWork(CreateWorkRequest request)
        {
            var arguments = CreateCommandArgs<CreateWorkArguments>();
            arguments.Name = request.Name?.Trim();

            var command = DependencyResolver.Current
                .GetService<ICommand<CreateWorkArguments, EmptyCommandResult>>();
            var result = await command.Execute(arguments);

            return Json(result.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Метод удаления услуги
        /// </summary>
        [HttpPost, AjaxOnly]
        public async Task<JsonResult> DeleteWork(int id)
        {
            var arguments = CreateCommandArgs<DeleteWorkArguments>();
            arguments.Id = id;

            var command = DependencyResolver.Current
                .GetService<ICommand<DeleteWorkArguments, EmptyCommandResult>>();
            var result = await command.Execute(arguments);

            return Json(result.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Меняет название услуги
        /// </summary>
        [HttpPost, AjaxOnly, ValidateRequest]
        public async Task<JsonResult> EditWork(EditWorkRequest request)
        {
            var arguments = CreateCommandArgs<EditWorkCommandArgs>();
            arguments.Name = request.Name?.Trim();
            arguments.WorkId = request.WorkId;

            var command = DependencyResolver.Current
                .GetService<ICommand<EditWorkCommandArgs, EmptyCommandResult>>();

            var commandResult = await command.Execute(arguments);
            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Меняет доп. услуги автомойки
        /// </summary>
        [HttpPost, ValidateRequest]
        public async Task<JsonResult> EditAdditionalWorks(EditAdditionalWorksRequest request)
        {
            var arguments = CreateCommandArgs<EditAdditionalWorksArgs>();
            arguments.CarWashAdditionalWorks = request.CarWashAdditionalWorkModel
                .Select(x => new EditAdditionalWorkArgs()
                {
                    WorkId = x.WorkId,
                    IsActive = x.IsActive
                }).ToArray();

            var command = DependencyResolver.Current
                .GetService<ICommand<EditAdditionalWorksArgs, EmptyCommandResult>>();
            var result = await command.Execute(arguments);
            return Json(result.ConverToResponse<EmptyResponse>(), JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// Возвращает тарифы услуги
        /// </summary>
        [HttpGet, AjaxOnly]
        public async Task<JsonResult> GetWorkTariffs(int id)
        {
            var builder = new GetWorkTariffsResponseBuilder(_workService);
            var response = await builder.Build(id, CurrentUser);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Метод редактирования тарифов услуги
        /// </summary>
        [HttpPost, AjaxOnly, ValidateRequest]
        public async Task<JsonResult> EditWorkTariffs(EditWorkTariffsRequest request)
        {
            var arguments = CreateCommandArgs<EditWorkTariffsArguments>();
            arguments.WorkId = request.WorkId;
            arguments.Tariffs = request.Tariffs.Select(x => new EditTariffArguments
            {
                TariffId = x.Id,
                Cost = x.Cost,
                Duration = x.Duration                
            }).ToArray();

            if (arguments.Tariffs.Length < 1)
                return Json(new EmptyResponse());

            var command = DependencyResolver.Current
                .GetService<ICommand<EditWorkTariffsArguments, EmptyCommandResult>>();
            var result = await command.Execute(arguments);

            return Json(result.ConverToResponse<EmptyResponse>());
        }
    }
}