﻿using System.Threading.Tasks;
using System.Web.Mvc;
using FreshCar.Attributes;
using FreshCar.Business.Commands;
using FreshCar.Business.Commands.Order;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Requests.OrderRequests;
using FreshCar.ResponseBuilders;
using FreshCar.Responses;
using FreshCar.Responses.OrderResponses;
using FreshCar.Utils.Extensions;
using FreshCar.Utils.Mapping;

namespace FreshCar.Controllers
{
    /// <summary>
    /// Контроллер для работы с заказами
    /// </summary>
    [Authorize]
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;
        private readonly IMapperUtility _mapperUtility;
        private readonly IWorkService _workService;

        public OrderController(IOrderService orderService, 
            IMapperUtility mapperUtility,
            IWorkService workService)
        {
            _orderService = orderService;
            _mapperUtility = mapperUtility;
            _workService = workService;
        }

        /// <summary>
        /// Возвращает список заказов для главной страницы
        /// </summary>
        [HttpGet, ValidateRequest, AjaxOnly]
        public async Task<JsonResult> GetOrders(GetOrdersRequest request)
        {
            request.Date = request.Date.ToUtc();
            var builder = new GetOrdersResponseBuilder(_orderService, _workService, _mapperUtility);
            var result = await builder.Build(request, CurrentUser);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Создает новую заявку 
        /// </summary>
        [HttpPost, ValidateRequest, AjaxOnly]
        public async Task<JsonResult> CreateOrder(CreateOrderRequest request)
        {
            request.CarMake = request.CarMake?.Trim();
            request.CarNumber = request.CarNumber?.Trim();
            request.ClientPhone = request.ClientPhone?.Trim();
            request.DateFrom = request.DateFrom.ToUtc();
            var arguments = _mapperUtility.Map<CreateOrderRequest, CreateOrderFromAdminCommandArgs>(request);
            arguments.CurrentUserInfo = CurrentUser;
            var command = DependencyResolver.Current
                .GetService<ICommand<CreateOrderFromAdminCommandArgs, CreateOrderFromAdminCommandResult>>();
            var result = await command.Execute(arguments);
            var response = result.ConverToResponse<CreateOrderResponse>();
            response.Id = result.Id;
            return Json(response);
        }

        [HttpPost, AjaxOnly]
        public async Task<JsonResult> DeleteOrder(long orderId)
        {
            var args = CreateCommandArgs<DeleteOrderCommandArgs>();
            args.OrderId = orderId;
            var command = DependencyResolver.Current
                .GetService<ICommand<DeleteOrderCommandArgs, EmptyCommandResult>>();
            var result = await command.Execute(args);
            return Json(result.ConverToResponse<EmptyResponse>());
        }

        [HttpPost, AjaxOnly, ValidateRequest]
        public async Task<JsonResult> EditOrder(EditOrderRequest request)
        {
            request.CarMake = request.CarMake?.Trim();
            request.CarNumber = request.CarNumber?.Trim();
            request.ClientPhone = request.ClientPhone?.Trim();
            request.DateFrom = request.DateFrom.ToUtc();
            request.DateTo = request.DateTo.ToUtc();
            var args = _mapperUtility.Map<EditOrderRequest, EditOrderFromAdminArgs>(request);
            args.CurrentUserInfo = CurrentUser;
            var command = DependencyResolver.Current
                .GetService<ICommand<EditOrderFromAdminArgs, EmptyCommandResult>>();
            var result = await command.Execute(args);
            return Json(result.ConverToResponse<EmptyResponse>());
        }
    }
}