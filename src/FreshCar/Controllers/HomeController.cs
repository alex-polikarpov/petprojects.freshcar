﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using FreshCar.Core.Services;
using FreshCar.Models;
using FreshCar.Models.Home;
using FreshCar.ResponseBuilders.Home;

namespace FreshCar.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private readonly ICarWashService _carWashService;
        private readonly ICompanyService _companyService;

        public HomeController(ICarWashService carWashService,
            ICompanyService companyService)
        {
            _carWashService = carWashService;
            _companyService = companyService;
        }

        /// <summary>
        /// Возвращает хедер с моделью
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Header()
        {
            var builder = new HeaderModelBuilder(_carWashService, _companyService);
            var task = Task.Run<HeaderModel>(async () => await builder.Build(CurrentUser));
            return PartialView("~/Views/Partial/Header.cshtml", task.Result);
        }

        /// <summary>
        /// Возвращает левую менюшку
        /// </summary>
        public PartialViewResult LeftMenu()
        {
            var builder=new LeftMenuModelBuilder();
            var model = builder.Build(CurrentUser);
            return PartialView("~/Views/Partial/LeftMenu.cshtml", model);
        }

        /// <summary>
        /// Главная страница с таблицей заказов
        /// </summary>
        public async Task<ActionResult> Index()
        {
            var carWash = await _carWashService.GetCarWashAsync(CurrentUser.CarWashId.Value);
            var model = new HomeIndexModel()
            {
                StartTimeOfWork = new TimeModel()
                {
                    Hours = carWash.StartTimeOfWork.Hours,
                    Minutes = carWash.StartTimeOfWork.Minutes
                },
                EndTimeOfWork = new TimeModel()
                {
                    Minutes = carWash.EndTimeOfWork.Minutes,
                    Hours = carWash.EndTimeOfWork.Hours
                }
            };
            return View(model);
        }
    }
}
