﻿using System.Threading.Tasks;
using System.Web.Mvc;
using FreshCar.Attributes;
using FreshCar.Constants;
using FreshCar.Core.Services;
using FreshCar.Requests.Statistic;
using FreshCar.ResponseBuilders.Statistic;

namespace FreshCar.Controllers
{
    [Authorize(Roles = AttributeConstants.RolesNames.DIRECTOR)]
    public class StatisticController : BaseController
    {
        private readonly IOrderService _orderService;
        private readonly IWorkService _workService;
        private readonly ICarWashService _carWashService;

        public StatisticController(IOrderService orderService,
            IWorkService workService,
            ICarWashService carWashService)
        {
            _orderService = orderService;
            _workService = workService;
            _carWashService = carWashService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet, AjaxOnly]
        public async Task<ActionResult> GetStatistic(GetStatisticRequest request)
        {
            var builder = new GetStatisticResponseBuilder(_orderService, _workService, _carWashService);
            var response = await builder.Build(request, CurrentUser);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}