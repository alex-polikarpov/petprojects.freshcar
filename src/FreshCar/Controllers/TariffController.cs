﻿using System.Threading.Tasks;
using System.Web.Mvc;
using FreshCar.Attributes;
using FreshCar.Business.Errors;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models.TariffModels;
using FreshCar.Requests.TariffRequests;
using FreshCar.Responses.TariffResponses;

namespace FreshCar.Controllers
{
    /// <summary>
    /// Контроллер для работы с тарифами
    /// </summary>
    [Authorize]
    public class TariffController : BaseController
    {
        private readonly IWorkService _workService;

        public TariffController(IWorkService workService)
        {
            _workService = workService;
        }

        /// <summary>
        /// Получает информацию о тарифе
        /// </summary>
        [HttpGet, AjaxOnly, ValidateRequest]
        public async Task<JsonResult> GetTariff(GetTariffRequest request)
        {
            var tariff = await _workService.GetTariffAsync(CurrentUser.CarWashId.Value, request.WorkId, request.CarType);
            var response = new GetTariffResponses();

            if (tariff == null)
            {
                response.AddError(ErrorConstants.Tariff.TARIFF_NOT_FOUND, 404);
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response.Tariff = new TariffModel()
            {
                Id = tariff.Id,
                Cost = tariff.Cost,
                Duration = tariff.Duration,
                CarType = tariff.CarType
            };

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}