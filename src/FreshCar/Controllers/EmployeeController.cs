﻿using System.Threading.Tasks;
using System.Web.Mvc;
using FreshCar.Attributes;
using FreshCar.Business.Commands;
using FreshCar.Constants;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Requests.Employee;
using FreshCar.Responses;

namespace FreshCar.Controllers
{
    /// <summary>
    /// Контроллер для работы с сотрудниками
    /// </summary>
    [Authorize(Roles = AttributeConstants.RolesNames.DIRECTOR)]
    public class EmployeeController : BaseController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet, AjaxOnly]
        public async Task<JsonResult> GetEmployees()
        {
            return Json(await _employeeService.GetCarWashAdministrators(CurrentUser.CarWashId.Value),JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Метод добавления нового пользователя
        /// </summary>
        [HttpPost, AjaxOnly, ValidateRequest]
        public async Task<JsonResult> AddEmployee(AddEmployeeRequest request)
        {
            var commandArgs = CreateCommandArgs<CreateNewEmployeeCommandArgs>();
            commandArgs.Phone = request?.Phone?.Trim();
            commandArgs.Login = request.Login.Trim();
            commandArgs.MiddleName = request.MiddleName.Trim();
            commandArgs.LastName = request.LastName.Trim();
            commandArgs.FirstName = request.FirstName.Trim();
            commandArgs.Password = request.Password.Trim();

            var command = DependencyResolver.Current
                .GetService<ICommand<CreateNewEmployeeCommandArgs, EmptyCommandResult>>();

            var result = await command.Execute(commandArgs);

            return Json(result.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Метод удаления сотрудника
        /// </summary>
        [HttpPost, AjaxOnly, ValidateRequest]
        public async Task<JsonResult> DeleteEmployee(DeleteEmployeeRequest request)
        {
            var args = CreateCommandArgs<DeleteEmployeeCommandArgs>();
            args.EmployeeId = request.EmployeeId;

            var command = DependencyResolver.Current
                .GetService<ICommand<DeleteEmployeeCommandArgs, EmptyCommandResult>>();
            var result = await command.Execute(args);

            return Json(result.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Метод изменения сотрудника
        /// </summary>
        [HttpPost, AjaxOnly, ValidateRequest]
        public async Task<JsonResult> EditEmployee(EditEmployeeRequest request)
        {
            var args = CreateCommandArgs<EditEmployeeCommandArgs>();
            args.EmployeeId = request.EmployeeId;
            args.Phone = request.Phone?.Trim();
            args.Login = request.Login?.Trim();
            args.MiddleName = request.MiddleName.Trim();
            args.LastName = request.LastName.Trim();
            args.FirstName = request.FirstName.Trim();
            args.Password = request.Password?.Trim();

            var command = DependencyResolver.Current
                .GetService<ICommand<EditEmployeeCommandArgs, EmptyCommandResult>>();

            var result = await command.Execute(args);

            return Json(result.ConverToResponse<EmptyResponse>());
        }
    }
}