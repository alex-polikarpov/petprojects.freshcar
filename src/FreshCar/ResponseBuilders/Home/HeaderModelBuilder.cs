﻿using System.Threading.Tasks;
using FreshCar.Core.Services;
using FreshCar.Models.Home;

namespace FreshCar.ResponseBuilders.Home
{
    public class HeaderModelBuilder
    {
        private readonly ICarWashService _carWashService;
        private readonly ICompanyService _companyService;

        public HeaderModelBuilder(ICarWashService carWashService,
            ICompanyService companyService)
        {
            _carWashService = carWashService;
            _companyService = companyService;
        }

        public async Task<HeaderModel> Build(UserInfo currentUser)
        {
            var result = new HeaderModel();
            result.UserLogin = currentUser.Email;

            var company = await _companyService.GetCompany(currentUser.CompanyId.Value, true);
            result.Balance = company.Balance;

            var carWash = await _carWashService.GetCarWashAsync(currentUser.CarWashId.Value);
            result.CarWashName = carWash.Name;
            return result;
        }
    }
}