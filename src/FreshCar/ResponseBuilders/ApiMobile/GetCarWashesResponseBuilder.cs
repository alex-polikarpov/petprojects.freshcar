﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Business.Errors;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models.CarWashModels;
using FreshCar.Requests.ApiMobile;
using FreshCar.Responses.ApiMobile;
using FreshCar.Utils.Mapping;

namespace FreshCar.ResponseBuilders.ApiMobile
{
    /// <summary>
    /// Билдер для GetCarWashesResponse
    /// </summary>
    public class GetCarWashesResponseBuilder
    {
        private readonly ICarWashService _carWashService;
        private readonly IMapperUtility _mapperUtility;

        public GetCarWashesResponseBuilder(ICarWashService carWashService, IMapperUtility mapperUtility)
        {
            _carWashService = carWashService;
            _mapperUtility = mapperUtility;
        }

        public async Task<GetCarWashesResponse> Build(GetCarWashesRequest filters)
        {
            var response = new GetCarWashesResponse();

            try
            {
                //проверяем, что заполнены все 3 параметра
                var filtersCount = 0;

                if (filters.CarType.HasValue)
                    filtersCount++;

                if (filters.Price.HasValue)
                    filtersCount++;

                if (filters.WorkId.HasValue)
                    filtersCount++;

                if (filtersCount > 0 && filtersCount < 3)
                {
                    return response.AddError("Должны быть заполнены все 3 параметра (стоимость, услуга, тип машины)", 500);
                }

                var carWashes = await _carWashService.GetCarWashesShortInfoAsync(new GetCarWashesShortInfoFilters()
                {
                    CarType = filters.CarType,
                    WorkId = filters.WorkId,
                    Price = filters.Price,
                    AdditionalWorksId = filters.AdditionalWorkIds
                });

                if (carWashes != null)
                {
                    response.CarWashes = _mapperUtility
                        .MapEnumerable<CarWashShortInfoDto, CarWashShortInfoModel>(carWashes)
                        .ToArray();

                    foreach (var carWash in response.CarWashes)
                    {
                        carWash.IsFree = await _carWashService.CarWashIsFreeAsync(carWash.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                response.AddError(ErrorConstants.System.INTERNAL_SERVER_ERROR, 500);
            }            

            return response;
        }
    }
}