﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models;
using FreshCar.Models.Statistic;
using FreshCar.Requests.Statistic;
using FreshCar.Responses.Statistic;
using FreshCar.Utils.Extensions;

namespace FreshCar.ResponseBuilders.Statistic
{
    public class GetStatisticResponseBuilder
    {
        private readonly IOrderService _orderService;
        private readonly IWorkService _workService;
        private readonly ICarWashService _carWashService;

        public GetStatisticResponseBuilder(IOrderService orderService,
            IWorkService workService,
            ICarWashService carWashService)
        {
            _orderService = orderService;
            _workService = workService;
            _carWashService = carWashService;
        }

        public async Task<GetStatisticResponse> Build(GetStatisticRequest request, UserInfo currentUser)
        {
            var response = new GetStatisticResponse()
            {
                FinishedOrdersCount = 0
            };

            request.DateFrom = request.DateFrom.ToUtc();
            request.DateTo = request.DateTo.ToUtc();
            if (request.DateFrom > request.DateTo)
            {
                return response.AddError(ErrorConstants.Order.INCORRECT_DATES, 400);
            }

            var orders = await _orderService
                .GetOrdersForStatistic(currentUser.CarWashId.Value, request.DateFrom, request.DateTo);

            decimal income = 0;

            if (orders != null && orders.Length > 0)
            {
                var tariffsId = orders.Select(x => x.TariffId).ToArray();
                var tariffsInfo = await _workService.GetTariffsAsync(tariffsId);
                var boxes = await _carWashService.GetCarWashBoxesAsync(currentUser.CarWashId.Value, false);
                var ordersModel = new List<OrderStatisticModel>(orders.Length);
                

                foreach (Order order in orders)
                {
                    var orderModel = CreateStatisticModel(tariffsInfo, boxes, order);

                    if (orderModel.OrderState == OrderState.FinishedAndPaid)
                    {
                        response.FinishedOrdersCount++;
                        income += orderModel.Cost;
                    }

                    orderModel.Duration = Convert.ToInt32((orderModel.DateTo - orderModel.DateFrom).TotalMinutes);
                    ordersModel.Add(orderModel);
                }

                response.Orders = ordersModel.ToArray();
                response.AllOrdersCount = ordersModel.Count;
            }
            
            response.Income = income.ToString("F2");
            return response;
        }

        private OrderStatisticModel CreateStatisticModel(TariffInfo[] tariffs, CarWashBox[] boxes, Order order)
        {
            var tariffInfo = tariffs.FirstOrDefault(x => x.TariffId == order.TariffId);
            var box = boxes.FirstOrDefault(x => x.Id == order.CarWashBoxId.Value);

            var result = new OrderStatisticModel()
            {
                OrderState = order.OrderState,
                OrderType = order.OrderType,
                CarMake = order.CarMake,
                CarNumber = order.CarNumber,
                DateFrom = order.DateFrom.ToUtc(),
                DateTo = order.DateTo.ToUtc(),
                Cost = order.Cost,
                OrderId = order.Id,
                ClientPhone = order.ClientPhone,
                TariffId = order.TariffId
            };

            if (tariffInfo != null)
            {
                result.WorkId = tariffInfo.WorkId;
                result.CarType = tariffInfo.CarType;
                result.WorkName = tariffInfo.WorkName;
            }

            if (box != null)
            {
                result.BoxId = box.Id;
                result.BoxName = box.Name;
            }

            return result;
        }
    }
}