﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Business.Errors;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models.OrderModels;
using FreshCar.Requests.OrderRequests;
using FreshCar.Responses.OrderResponses;
using FreshCar.Utils.Extensions;
using FreshCar.Utils.Mapping;

namespace FreshCar.ResponseBuilders
{
    /// <summary>
    /// Билдер для GetOrdersResponse
    /// </summary>
    public class GetOrdersResponseBuilder
    {
        #region конструктор, поля

        private readonly IOrderService _orderService;
        private readonly IWorkService _workService;
        private readonly IMapperUtility _mapperUtility;

        public GetOrdersResponseBuilder(
            IOrderService orderService,
            IWorkService workService,
            IMapperUtility mapperUtility)
        {
            _orderService = orderService;
            _workService = workService;
            _mapperUtility = mapperUtility;
        }

        #endregion

        public async Task<GetOrdersResponse> Build(GetOrdersRequest request, UserInfo currentUser)
        {
            var response = new GetOrdersResponse();

            try
            {
                var orders = await _orderService.GetOrders(currentUser.CarWashId.Value, request.Date);
                response.Orders = _mapperUtility.MapEnumerable<Core.Entities.Order, OrderModel>(orders).ToArray();

                foreach (var order in response.Orders)
                {
                    var tariff = await _workService.GetTariffByIdAsync(order.TariffId);
                    order.WorkId = tariff.WorkId;
                    order.CarType = tariff.CarType;
                    order.DateFrom = order.DateFrom.ToUtc();
                    order.DateTo = order.DateTo.ToUtc();
                }
            }
            catch (Exception ex)
            {
                return response.AddError(ErrorConstants.System.INTERNAL_SERVER_ERROR, 500);
            }

            return response;
        }
    }
}