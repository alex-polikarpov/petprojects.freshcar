﻿using System.ComponentModel.DataAnnotations;
using FreshCar.Core.Entities;

namespace FreshCar.Requests.TariffRequests
{
    /// <summary>
    /// Реквест для получения информации о тарифе
    /// </summary>
    public class GetTariffRequest
    {
        public int WorkId { get; set; }

        [EnumDataType(typeof(CarType), ErrorMessage = "Недопустимый тип машины")]
        public CarType CarType { get; set; }
    }
}