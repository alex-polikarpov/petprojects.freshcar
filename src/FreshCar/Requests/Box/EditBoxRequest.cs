﻿using System.ComponentModel.DataAnnotations;
using FreshCar.Core.Entities;

namespace FreshCar.Requests.Box
{
    public class EditBoxRequest
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Заполните имя", AllowEmptyStrings = false)]
        [MaxLength(CarWashBox.NAME_MAX_LENGTH, ErrorMessage = "Максимальная длина имени: {1}")]
        public string Name { get; set; }
    }
}