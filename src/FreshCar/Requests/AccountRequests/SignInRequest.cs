﻿using System.ComponentModel.DataAnnotations;
using FreshCar.Constants;

namespace FreshCar.Requests.AccountRequests
{
    /// <summary>
    /// Параметры запроса на вход
    /// </summary>
    public class SignInRequest
    {
        /// <summary>
        /// Логин (почта)
        /// </summary>
        [Required(ErrorMessage = "Заполните логин")]
        [EmailAddress(ErrorMessage = "Логин должен быть почтовым адресом")]
        [MinLength(5, ErrorMessage = "Длина должна быть не менее {1}")]
        [MaxLength(50, ErrorMessage = "Длина должна быть не более {1}")]
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required(ErrorMessage = "Заполните пароль")]
        [MinLength(6, ErrorMessage = "Длина пароля должна быть не менее {1}")]
        [MaxLength(20, ErrorMessage = "Длина пароля должна быть не более {1}")]
        [RegularExpression(RegexConstants.PASSWORD, ErrorMessage = "Пароль должен состоять из цифр или латинских букв")]
        public string Password { get; set; }
    }
}