﻿using System.ComponentModel.DataAnnotations;
using FreshCar.Constants;

namespace FreshCar.Requests.AccountRequests
{
    /// <summary>
    /// Запрос на регистрацию пользователя
    /// </summary>
    public class RegisterRequest
    {
        /// <summary>
        /// Улица
        /// </summary>
        [Required(ErrorMessage = "Улица не может быть пустым полем", AllowEmptyStrings = false)]
        [MaxLength(40, ErrorMessage = "Длина должна быть не более {1}")]
        public string Street { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        [Required(ErrorMessage = "Город не может быть пустым полем", AllowEmptyStrings = false)]
        [MaxLength(30, ErrorMessage = "Длина должна быть не более {1}")]
        public string City { get; set; }

        /// <summary>
        /// Номер дома
        /// </summary>
        [Required(ErrorMessage = "Номер дома не может быть пустым полем", AllowEmptyStrings = false)]
        [MaxLength(10, ErrorMessage = "Длина должна быть не более {1}")]
        public string House { get; set; }

        /// <summary>
        /// Название фирмы (и название мойки)
        /// </summary>
        [Required(ErrorMessage = "Название не может быть пустым полем", AllowEmptyStrings = false)]
        [MaxLength(50, ErrorMessage = "Длина должна быть не более {1}")]
        public string CompanyName { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        [Required(ErrorMessage = "Телефон не может быть пустым полем", AllowEmptyStrings = false)]
        [Phone(ErrorMessage = "Номер должен состоять из цифр")]
        [MaxLength(20, ErrorMessage = "Длина должна быть не более {1}")]
        [MinLength(5, ErrorMessage = "Длина должна быть не менее {1}")]
        public string Phone { get; set; }

        /// <summary>
        /// Почта (логин)
        /// </summary>
        [Required(ErrorMessage = "Почта не может быть пустым полем", AllowEmptyStrings = false)]
        [EmailAddress(ErrorMessage = "Почтовый адрес должен быть в правильном формате")]
        [MinLength(5, ErrorMessage = "Длина должна быть не менее {1}")]
        [MaxLength(50, ErrorMessage = "Длина должна быть не более {1}")]
        public string Email { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required(ErrorMessage = "Пароль не может быть пустым полем", AllowEmptyStrings = false)]
        [MinLength(6, ErrorMessage = "Длина пароля должна быть не менее {1}")]
        [MaxLength(20, ErrorMessage = "Длина пароля должна быть не более {1}")]
        [RegularExpression(RegexConstants.PASSWORD,ErrorMessage = "Пароль должен состоять из цифр или латинских букв")]
        public string Password { get; set; }
    }
}