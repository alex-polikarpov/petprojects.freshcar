﻿using System.ComponentModel.DataAnnotations;
using FreshCar.Models.CarWashAdditionalWorkModels;

namespace FreshCar.Requests.WorkRequests
{
    public class EditAdditionalWorksRequest
    {
        [Required(ErrorMessage = "Укажите доп. услуги для редактирования")]
        public CarWashAdditionalWorkModel[] CarWashAdditionalWorkModel { get; set; }
    }
}