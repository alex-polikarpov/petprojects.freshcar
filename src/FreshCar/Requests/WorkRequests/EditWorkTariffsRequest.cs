﻿using System.ComponentModel.DataAnnotations;

namespace FreshCar.Requests.WorkRequests
{
    public class EditWorkTariffsRequest
    {
        public int WorkId { get; set; }

        [Required(ErrorMessage = "Укажите тарифы для редактирования")]
        public EditTariffRequest[] Tariffs { get; set; }
    }
}