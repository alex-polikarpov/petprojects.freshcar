﻿using System.ComponentModel.DataAnnotations;
using FreshCar.Core.Entities;

namespace FreshCar.Requests.WorkRequests
{
    /// <summary>
    /// Запрос на создание услуги
    /// </summary>
    public class CreateWorkRequest
    {
        [Required(ErrorMessage = "Заполните имя услуги", AllowEmptyStrings = false)]
        [MaxLength(Work.NAME_MAX_LENGTH, ErrorMessage = "Максимальная длина имени услуги: {1}")]
        public string Name { get; set; }
    }
}