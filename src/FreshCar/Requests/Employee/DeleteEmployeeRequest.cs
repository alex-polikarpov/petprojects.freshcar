﻿namespace FreshCar.Requests.Employee
{
    /// <summary>
    /// Запрос на удаление сотрудника
    /// </summary>
    public class DeleteEmployeeRequest
    {
        public int EmployeeId { get; set; }
    }
}