﻿using System.ComponentModel.DataAnnotations;
using FreshCar.Core.Entities;

namespace FreshCar.Requests.ApiMobile
{
    /// <summary>
    /// Реквест для получения информации о тарифе
    /// </summary>
    public class GetTariffRequest
    {
        /// <summary>
        /// Айди мойки
        /// </summary>
        public int CarWashId { get; set; }

        /// <summary>
        /// Id услуги
        /// </summary>
        public int WorkId { get; set; }

        /// <summary>
        /// Тип машины
        /// </summary>
        [EnumDataType(typeof(CarType), ErrorMessage = "Недопустимый тип машины")]
        public CarType CarType { get; set; }
    }
}