﻿using System.ComponentModel.DataAnnotations;

namespace FreshCar.Requests.ApiMobile
{
    /// <summary>
    /// Запрос для получения информации об автомойке
    /// </summary>
    public class GetCarWashRequest
    {
        public int CarWashId { get; set; }
    }
}