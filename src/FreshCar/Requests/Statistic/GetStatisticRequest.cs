﻿using System;
using FreshCar.Attributes;

namespace FreshCar.Requests.Statistic
{
    public class GetStatisticRequest
    {
        [DateLaterThan(14, 03, 2016)]
        public DateTime DateFrom { get; set; }

        [DateLaterThan(14, 03, 2016)]
        public DateTime DateTo { get; set; }
    }
}