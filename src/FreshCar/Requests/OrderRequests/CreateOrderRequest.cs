﻿using System;
using System.ComponentModel.DataAnnotations;
using FreshCar.Attributes;
using FreshCar.Core.Entities;

namespace FreshCar.Requests.OrderRequests
{
    /// <summary>
    /// Реквест на создание нового заказа
    /// </summary>
    public class CreateOrderRequest
    {
        /// <summary>
        /// Id тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Id бокса мойки, где будет происходить выполнение заказа
        /// </summary>
        public int CarWashBoxId { get; set; }

        /// <summary>
        /// Дата и время, когда начнется выполнение заказа (обязательно в UTC)
        /// </summary>
        [DateLaterThan(10,03,2016, ErrorMessage = "Невалидная дата")]
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Номер машины клиента
        /// </summary>
        [MaxLength(Order.CAR_NUMBER_MAX_LENGTH, ErrorMessage = "Максимальная длина номера машины: {1}")]
        public string CarNumber { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        [Phone(ErrorMessage = "Неверный формат телефона")]
        [MaxLength(Order.CLIENT_PHONE_MAX_LENGTH, ErrorMessage = "Максимальная длина телефона: {1}")]
        public string ClientPhone { get; set; }

        /// <summary>
        /// Марка автомобиля
        /// </summary>
        [MaxLength(Order.CAR_MAKE_MAX_LENGTH,ErrorMessage = "Максимальная длина названия марки машины: {1}")]
        public string CarMake { get; set; }
    }
}