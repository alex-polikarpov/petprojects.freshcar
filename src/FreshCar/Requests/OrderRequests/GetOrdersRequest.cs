﻿using System;
using FreshCar.Attributes;

namespace FreshCar.Requests.OrderRequests
{
    public class GetOrdersRequest
    {
        /// <summary>
        /// Дата, для которой нужно вернуть заказы
        /// </summary>
        [DateLaterThan(14, 3, 2015, ErrorMessage = "Нельзя посмотреть заявки за эту дату")]
        public DateTime Date { get; set; }
    }
}