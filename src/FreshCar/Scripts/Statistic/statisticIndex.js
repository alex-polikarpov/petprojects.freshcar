﻿var rootUrl = window.location.protocol + "//" + window.location.host + "/",a,b,
    dataSource = new kendo.data.DataSource({
     /*data: [
        {
            Id: 12123, DateTime: "2016-04-17 16:00:00", CarNumber: "н203рн116", CarMake: "bmw", WorkName: "Экспресс-мойка",
            Cost: 350, Duration: 30, PhoneNumber: "9876543210", OrderState: "3", OrderType: "1", CarType: 3, BoxNumber: 1
        },
        {
            Id: 12124, DateTime: "2016-04-17 18:00:00", CarNumber: "к362со116", CarMake: "Ваз 2114", WorkName: "Комплекс с багажником",
            Cost: 400, Duration: 45, PhoneNumber: "9876543211", OrderState: "7", OrderType: "0", CarType: 2, BoxNumber: 1
        },
        {
            Id: 12125, DateTime: "2016-04-17 18:00:00", CarNumber: "х999хх16", CarMake: "Porshe", WorkName: "Комплекс с багажником",
            Cost: 500, Duration: 50, PhoneNumber: "9876543212", OrderState: "3", OrderType: "1", CarType: 5, BoxNumber: 2
        }
    ],*/
    transport: {
        read: {
            url: rootUrl + "Statistic/GetStatistic",
            dataType: "json"
        }
    },
    schema: {
        model: {
            id: "OrderId",
            fields: {
                OrderId: { type: "number" },
                DateFrom: { type: "string" },
                CarNumber: { type: "string" },
                CarMake: { type: "string"},
                WorkName: { type: "string" },
                Cost: { type: "number" },
                Duration: { type: "number" },
                ClientPhone: { type: "string" },
                OrderState: { type: "number" },
                OrderType: { type: "number" },
                CarType: { type: "number" },
                BoxName: { type: "string" }
            }
        },
        parse: function (data) {
            if (data.ErrorCode !== 0) { 
                // alert(data.ErrorMessage);
                $("#grid").hide();
                showError(data.ErrorMessage);
                return [];
            } else {  //Если нет ошибок, то выводим всё
                if (data.Orders) {
                    $("#grid").show();
                    $("#income").text(data.Income);
                    $("#count").text(data.FinishedOrdersCount);

                    data.Orders.map(function (e) {
                        var start = new Date(e.DateFrom);
                        var end = new Date(e.DateTo);
                        //Получили минуты
                        e.Duration = (end - start) / 1000 / 60;

                        e.DateFrom = formatDate2(start);
                        e.DateTo = formatDate2(end);
                    });

                    return data.Orders;
                } else {
                    $("#grid").hide();
                   // alert("За выбранный интервал данные отсутствуют");
                    showSuccess("За выбранный интервал данные отсутствуют");
                    return [];
                }
            } 
        }
    },
    requestStart: function (e) {
         $(".cssload-jumping").show();
    },
    requestEnd: function (e) {
        $(".cssload-jumping").hide();
    }
});

//--------------------------------
//  Уведомления
//--------------------------------
//Ошибка
function showError(msg) {
    hideMessages();
    $("#error-msg").text(msg);
    $("#error-block").show();
}
//Успех
function showSuccess(msg) {
    hideMessages();
    $("#success-msg").text(msg);
    $("#success-block").show();
}
//Скрытие обоих блоков
function hideMessages() {
    $("#success-block").hide();
    $("#error-block").hide();
}

function formatDate(date) { //Функция форматирования даты и времени
    var str = "",
        val;
    str += date.getFullYear() + "-";
    str += (val = date.getMonth() + 1) < 10 ? "0" + val + "-" : val + "-";
    str += (val = date.getDate()) < 10 ? "0" + val : val;

    return str;
}

function formatDate2(date) { //Функция форматирования даты и времени
    var str = '',
        val;
    str += date.getFullYear() + "-";
    str += (val = date.getMonth() + 1) < 10 ? "0" + val + "-" : val + "-";
    str += (val = date.getDate()) < 10 ? "0" + val + " " : val + " ";

    str += (val = date.getHours()) < 10 ? "0" + val + ":" : val + ":";
    str += (val = date.getMinutes()) < 10 ? "0" + val + ":" : val + ":";
    str += (val = date.getSeconds()) < 10 ? "0" + val : val;
    return str;
}

function detailClose() {
    $("#details").data("kendoWindow").close();
}

function detailShow(e) {

   /* var selectedRows = this.select();
    var selectedDataItems = [];
    for (var i = 0; i < selectedRows.length; i++) {
        var dataItem = this.dataItem(selectedRows[i]);
        selectedDataItems.push(dataItem);
    }*/

    //  selectedDataItems

    var info = dataSource.get(+$(e).data("id"));

    info.OrderTypeString = info.OrderType === 0 ? "С приложения" : "Ручная запись";

    switch (info.OrderState) {
        case 0: info.OrderStateString = "Новый заказ";
            break;
        case 1: info.OrderStateString = "Заказ принят";
            break;
        case 2: info.OrderStateString = "В работе";
            break;
        case 3: info.OrderStateString = "Выполнен и оплачен";
            break;
        case 4: info.OrderStateString = "Выполнен, но не оплачен";
            break;
        case 5: info.OrderStateString = "Удалён";
            break;
        case 6: info.OrderStateString = "Отменён клиентом";
            break;
        case 7: info.OrderStateString = "Отменён";
            break;
    }

    switch (info.CarType) {
        case 0: info.CarTypeString = "Другой";
            break;
        case 1: info.CarTypeString = "Мини";
            break;
        case 2: info.CarTypeString = "Легковая (отечественная)";
            break;
        case 3: info.CarTypeString = "Легковая (Иномарка)";
            break;
        case 4: info.CarTypeString = "Кроссовер";
            break;
        case 5: info.CarTypeString = "Внедорожник";
            break;
        case 6: info.CarTypeString = "Микроавтобус";
            break;
    }

    $("#details").kendoWindow({
        title: "Детальная информация",
        draggable: true,
        modal: true,
        pinned: true,
        resizable: false
    }).data("kendoWindow").center().open();

    kendo.bind($("#details"), info);
   /* var selectedCells = this.select();
    var selectedDataItems = [];
    for (var i = 0; i < selectedCells.length; i++) {
        var dataItem = this.dataItem(selectedCells[i].parentNode);
        if ($.inArray(dataItem, selectedDataItems) < 0) {
            selectedDataItems.push(dataItem);
        }
    }*/
}

function submitStatistic() {

    hideMessages();
    //Проверка времени
    if (!$("#time1").val()) {
        $("#time1").val("00:00");
    }
    if (!$("#time2").val()) {
        $("#time2").val("23:59");
    }
    //Парсим дату и время
    var st = new Date($("#start").val() + " " + $("#time1").val());
    var en = new Date($("#end").val() + " " + $("#time2").val());
    //Проверка дат
  /*  if (st.valueOf() + "" === "NaN" || en.valueOf() + "" === "NaN") {
        showError("Поля 'Дата и время' должны быть корректно заполнены (к примеру в этом месяце меньше 31 дня)");
        alert("Поля 'Дата и время' должны быть корректно заполнены");
        return false;
    } else*/ if (st > en) {
        showError("Дата конца не может быть меньше даты начала");
        alert("Дата конца не может быть меньше даты начала");
        return false;
    } else { //Делаем запрос на сервер, отсылаем даты в utc
        dataSource.options.transport.read.data = {
            DateFrom: st.toISOString(),
            DateTo: en.toISOString()
        };
        dataSource.read();
    }
}

$(document).ready(function () {

    $("li.active").removeClass("active");
    $("a[data-id=3]").parent("li").addClass("active");
   
    kendo.culture("ru-RU");
   /* function setTime(date, hours, minutes, seconds, milliseconds) {
        date.hour(hours);
        date.minute(minutes);
        date.second(seconds);
        date.set("millisecond", milliseconds);
    }

    var dateFrom = moment(new Date());
    var dateTo = moment(new Date());
    setTime(dateFrom, 0, 0, 0, 0);
    setTime(dateTo, 23, 59, 59, 999);

    $.ajax({
        type: "GET",
        url: "../Statistic/GetStatistic",
        data: {
            DateFrom: dateFrom.toISOString(),
            DateTo: dateTo.toISOString()
        },
        beforeSend: function() {
            $(".cssload-jumping").show();
        },
        success: function(response) {
            $(".cssload-jumping").hide();
            if (response.ErrorCode !== 0) {
                showError(response.ErrorMessage);
            } else {
                $("#income").text(response.Statistic.Income);
                $("#count").text(response.Statistic.OrdersCount);
            }
        },
        error: function() {
            $(".cssload-jumping").hide();
            showError("Не удалось получить статистику");
        }
    });*/

    
    //Инициализация отрезков времени
    $("#start").val(formatDate(new Date()));
    $("#end").val(formatDate(new Date()));
  
    //Инициализация грида
    $("#grid").kendoGrid({
        dataSource: dataSource,
        // mobile: true,
       // selectable: "row",
        // change: onChange,
        autoBind: false,
        columns: [
           // { field: "Id", title: "№", width: "50px"}, //footerTemplate: "Помытых машин: #: count #"
            { field: "DateFrom", title: "Дата начала", width: "100px" },
            { field: "CarNumber", title: "Гос. номер", width: "85px" },
            { field: "OrderState", title: "Статус", width: "85px", values: [
                  { text: "Новый заказ", value: 0 },
                  { text: "Заказ принят", value: 1 },
                  { text: "В работе", value: 2 },
                  { text: "Выполнен и оплачен", value: 3 },
                  { text: "Выполнен, но не оплачен", value: 4 },
                  { text: "Удалён", value: 5 },
                  { text: "Отменён клиентом", value: 6 },
                  { text: "Отменён", value: 7 }]
            },
            { field: "Cost", title: "Стоимость", width: "65px", attributes: { style: "padding-left:10px;" } }, // footerTemplate: "Выручка: #: sum #"
            { field: "Duration", title: "Продолжительность (мин)", width: "95px" }, // attributes: { style: "text-align:center;" }
            { field: "CarType", title: "Тип авто", width: "120px", values: [
                            { value: 0, text: "Другой" },
                            { value: 1, text: "Мини" },
                            { value: 2, text: "Легковая (отечественная)" },
                            { value: 3, text: "Легковая (Иномарка)" },
                            { value: 4, text: "Кроссовер" },
                            { value: 5, text: "Внедорожник" },
                            { value: 6, text: "Микроавтобус" }]
            },
            { template: kendo.template($("#details-temp").html()), width: "150px", attributes: { style: "text-align:center;" } }
           /* { field: "CarMake", title: "Марка", width: "80px" },
            { field: "WorkName", title: "Тип мойки", width: "140px" },
            { field: "PhoneNumber", title: "Номер телефона", width: "110px" },
            { field: "OrderType", title: "Тип заявки", width: "80px", values: [{ text: "C приложения", value: 0 }, { text: "Ручная запись", value: 1 }] },
            { field: "BoxNumber", title: "Бокс", width: "70px", attributes: { style: "text-align:center;" } }*/
        ]
    });

});