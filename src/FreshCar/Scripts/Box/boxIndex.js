﻿$(document).ready(function () {

    //делаем в менюшке текущую страницу выделенной
    $("li.active").removeClass("active");
    $("a[data-id=4]").parent("li").addClass("active");

    function showError(msg) {
        $("#error-msg").text(msg);
        $("#error-block").show();
    }

    function hideMessages() {
        $("#success-msg").hide();
        $("#error-block").hide();
    }

    $("#edit-box-modal").kendoWindow({
        title: "Редактирование бокса",
        draggable: false,
        modal: true,
        pinned: true,
        resizable: false
    }).data("kendoWindow");

    $("#create-box-modal").kendoWindow({
        title: "Создание бокса",
        draggable: false,
        modal: true,
        pinned: true,
        resizable: false
    }).data("kendoWindow");


    //==================================
    //==================================
    //Редактирование бокса
    //==================================
    //==================================
    function editBox_Click() {
        hideMessages();
        var $this = $(this);
        var id = $this.data("boxId");
        var name = $this.data("boxName");
        $("#edit-box-id").val(id);
        $("#edit-box-name").val(name);
        var win = $("#edit-box-modal").data("kendoWindow");
        win.center().open();
    }

    function editBoxCancel_Click() {
        var win = $("#edit-box-modal").data("kendoWindow");
        win.close();
    }

    function editBoxSave_Click() {
        var id = $("#edit-box-id").val();
        var name = $("#edit-box-name").val();

        $.ajax({
            type: "POST",
            url: "/Box/EditBox",
            data: { id: id, name: name },
            beforeSend: function() {
                $(".cssload-jumping").show();
            },
            success: function (response) {
                $(".cssload-jumping").hide();
                if (response.ErrorCode !== 0) {
                    editBoxCancel_Click();
                    showError(response.ErrorMessage);
                } else {
                    location.reload();
                }
            },
            error: function () {
                $(".cssload-jumping").hide();
                editBoxCancel_Click();
                showError("Внутренняя ошибка сервера");
            }
        });
    }

    $(".edit-box-button").click(editBox_Click);  
    $("#edit-box-save").click(editBoxSave_Click);
    $("#edit-box-cancel").click(editBoxCancel_Click);


    //==================================
    //==================================
    //Удаление бокса
    //==================================
    //==================================
    function deleteBox_Click() {
        if (confirm("Вы действително хотите удалить бокс?")) {
            hideMessages();

            var $this = $(this);
            var data = $this.data("boxId");

            $.ajax({
                type: "POST",
                url: "/Box/DeleteBox",
                data: { id: data },
                beforeSend: function() {
                    $(".cssload-jumping").hide();
                },
                success: function (response) {
                    $(".cssload-jumping").show();
                    if (response.ErrorCode !== 0) {
                        showError(response.ErrorMessage);
                    } else {
                        location.reload();
                    }
                },
                error: function () {
                    $(".cssload-jumping").hide();
                    showError("Внутренняя ошибка сервера");
                }
            });
        }
    }

    $('.delete-box-button').click(deleteBox_Click);
    
    
    //==================================
    //==================================
    //Создание бокса
    //==================================
    //==================================
    function addBox_Click() {
        hideMessages();
        $("#create-box-name").val("");
        var win = $("#create-box-modal").data("kendoWindow");
        win.center().open();
    }

    function addBoxCancel_Click() {
        var win = $("#create-box-modal").data("kendoWindow");
        win.close();
    }

    function saveNewBox_Click() {
        var name = $("#create-box-name").val();

        $.ajax({
            type: "POST",
            url: "/Box/CreateBox",
            data: { name: name },
            beforeSend: function() {
                $(".cssload-jumping").show();
            },
            success: function (response) {
                $(".cssload-jumping").hide();
                if (response.ErrorCode !== 0) {
                    addBoxCancel_Click();
                    showError(response.ErrorMessage);
                } else {
                    location.reload();
                }
            },
            error: function () {
                $(".cssload-jumping").hide();
                addBoxCancel_Click();
                showError("Внутренняя ошибка сервера");
            }
        });
    }

    $("#addBox").click(addBox_Click);
    $("#create-box-save").click(saveNewBox_Click);
    $("#create-box-cancel").click(addBoxCancel_Click);
});