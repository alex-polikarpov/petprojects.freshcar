﻿$(document).ready(function () {

    $(".form").find("input, textarea").on("keyup blur focus", function (e) {

        var $this = $(this),
            label = $this.prev("label");

        if (e.type === "keyup") {
            if ($this.val() === "") {
                label.removeClass("active highlight");
            } else {
                label.addClass("active highlight");
            }
        } else if (e.type === "blur") {
            if ($this.val() === "") {
                label.removeClass("active highlight");
            } else {
                label.removeClass("highlight");
            }
        } else if (e.type === "focus") {

            if ($this.val() === "") {
                label.removeClass("highlight");
            } else if ($this.val() !== "") {
                label.addClass("highlight");
            }
        }

    });

    $(".tab a").on("click", function (e) {

        e.preventDefault();

        $(this).parent().addClass("active");
        $(this).parent().siblings().removeClass("active");

        target = $(this).attr("href");

        $(".tab-content > div").not(target).hide();

        $(target).fadeIn(600);

    });

    $("#loginForm").on("submit", function (e) {
        e.preventDefault();
        $("#infoMesLogin").text("");
        var form = $(e.target);

        $(".cssload-jumping").show();
        $.post(form.attr("action"), form.serialize(), function (result) {
            $(".cssload-jumping").hide();
            if (result.ErrorCode === 0) {
                $("#infoMesLogin").text("");
               // $(".cssload-jumping").hide();
                location.href = "../Home";
            } else {
                if (result.ErrorCode === 500) {
                    $("#infoMesLogin").text("Что-то пошло не так :( Попрбуйте позже.");
                } else {
                    $("#infoMesLogin").text(result.ErrorMessage);
                }
            }
        });
    });

    $("#registrationFrom").on("submit", function (e) {
        e.preventDefault();
        $("#infoMesReg").text("");
        var form = $(e.target);
        $(".cssload-jumping").show();
        $.post(form.attr("action"), form.serialize(), function (result) {
            $(".cssload-jumping").hide();
            if (result.ErrorCode === 0) {
                $("#infoMesReg").text("Регистрация прошла успешно. В ближайшее время ваша автомойка появится в системе!");
            } else {
                if (result.ErrorCode === 500) {
                    $("#infoMesReg").text("Что-то пошло не так :( Попрбуйте позже.");
                } else {
                    $("#infoMesReg").text(result.ErrorMessage);
                }
            }
        });
    });

});

