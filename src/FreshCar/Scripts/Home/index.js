﻿var carMakes1 = [
        "Alfa Romeo", "Aston Martin", "Audi", "Aro", "Alfa Romeo", "Austin", "Alpina", "AC", "Acura", "Alpine", "Asia", "BMW", "Buick", "Bentley", "Beijing",
        "Bugatti", "Bristol", "BYD", "Bentley", "Brilliance", "BAW", "Volkswagen", "Wartburg", "Venturi", "Volvo", "Vector", "Westfield", "Wiesmann", "Wuling",
        "Vortex", "Geo", "GMC", "Great Wall", "Geely", "De Tomaso", "Daimler", "Dodge", "Dacia", "Daihatsu", "Daewoo", "Jeep", "Jiangnan", "Dallas", "Doninvest",
        "Dadi", "Derways", "Jiangling", "Dong Feng", "Eagle", "Zastava", "ZX", "Isuzu", "Isdera", "Innocenti", "Infiniti", "Invicta", "Iran Khodro", "Xin Kai",
        "IVECO", "Citroen", "Kia", "Carbodies", "Cadillac", "Cizeta", "Caterham", "Callaway", "Qvale", "Coggiola", "Koenigsegg", "Lotus", "Lancia", "Land Rover",
        "Lincoln", "Lexus", "Lamborghini", "LTI", "Landwind", "Lifan", "Luxgen", "Mitsubishi", "Mazda", "Mercedes-Benz", "Mercury", "Morgan", "Maruti", "Maserati",
        "Mahindra", "Monte Carlo", "Metrocab", "McLaren", "Mega", "MG", "Mitsuoka", "Minelli", "Marcos", "Maybach", "Marlin", "Mini", "Marussia", "Nissan", "Noble",
        "Opel", "Oldsmobile", "Premier", "Pontiac", "Peugeot", "Paykan", "Plymouth", "Proton", "Porsche", "Puma", "Panoz", "Perodua", "Pagani", "Renault", "Reliant",
        "Rover", "Rolls-Royce", "Ronart", "Saab", "Suzuki", "Skoda", "SEAT", "Subaru", "SsangYong", "Saturn", "Spectre", "Samsung", "Smart", "Saleen", "Scion", "Soueast",
        "SMA", "Toyota", "Trabant", "Tatra", "Talbot", "Tofas", "TVR", "Tata", "Tianma", "Fiat", "Ford", "FSO", "Ferrari", "FAW", "Fuqi", "Hyundai", "Hindustan", "Honda",
        "Holden", "Hummer", "Hafei", "HuangHai", "Haima", "Chevrolet", "Chrysler", "Chery", "Changhe", "ChangFeng", "Chana", "ShuangHuan", "Shifeng", "Jaguar", "JAC",
        "Богдан", "ВАЗ (Lada)", "ГАЗ", "ЗАЗ", "ИЖ", "Москвич", "СеАЗ", "ТагАЗ", "Газель", "УАЗ", "КАМАЗ", "ЗИЛ"
];

var start = "00:00",
    end = "23:59",
    rootUrl = window.location.protocol + "//" + window.location.host + "/",
        st;

var dtRooms = new kendo.data.DataSource({
    transport: {
        read: rootUrl + "Box/GetBoxes",
        dataType: "json"
    },
    schema: {
        parse: function (data) {
            return data.Boxes;
        },
        model: {
            id: "Id",
            fields: {
                Id: { type: "number" },
                Name: { type: "string" }
            }
        }
    }
});

var dtPrices = new kendo.data.DataSource({
    transport: {
        read: rootUrl + "Work/GetMainWorks",
        dataType: "json"
    },
    schema: {
        parse: function (data) {
            return data.Works;
        },
        model: {
            id: "Id",
            fields: {
                Id: { type: "number" },
                Name: { type: "string" }
            }
        }
    }
});

var worksAdvance = new kendo.data.DataSource({
    data: [
                { Id: 10, Name: "Химичистка" },
                { Id: 11, Name: "Полировка" },
                { Id: 12, Name: "Чернение резины" },
                { Id: 13, Name: "Протирка" }
    ],schema: {
        model: {
            id: "Id",
            fields: {
                Id: { type: "number" },
                Name: { type: "string" }
            }
        }
    }
});

var dtCarTypes = new kendo.data.DataSource({
    data: [
                { Id: 0, Name: "Другой" },
                { Id: 1, Name: "Мини" },
                { Id: 2, Name: "Легковая(отечественная)" },
                { Id: 3, Name: "Легковая(Иномарка)" },
                { Id: 4, Name: "Кроссовер" },
                { Id: 5, Name: "Внедорожник" },
                { Id: 6, Name: "Микроавтобус" }
    ],
    schema: {
        model: {
            id: "Id",
            fields: {
                Id: { type: "number" },
                Name: { type: "string" }
            }
        }
    }
});

var dtStates = new kendo.data.DataSource({
    data: [
                { Id: 0, Name: "Новый заказ" },
                { Id: 1, Name: "Заказ принят" },
                { Id: 2, Name: "В работе" },
                { Id: 3, Name: "Выполнен и оплачен" },
                { Id: 4, Name: "Выполнен, но не оплачен" },
                { Id: 7, Name: "Отменён" }
    ],
    schema: {
        model: {
            id: "Id",
            fields: {
                Id: { type: "number" },
                Name: { type: "string" }
            }
        }
    }
});

var dtStatesInWork = new kendo.data.DataSource({
    data: [
                { Id: 2, Name: "В работе" },
                { Id: 3, Name: "Выполнен и оплачен" },
                { Id: 4, Name: "Выполнен, но не оплачен" },
                { Id: 7, Name: "Отменён" }
    ],
    schema: {
        model: {
            id: "Id",
            fields: {
                Id: { type: "number" },
                Name: { type: "string" }
            }
        }
    }
});

var dtStatesReady = new kendo.data.DataSource({
    data: [
                { Id: 3, Name: "Выполнен и оплачен" },
                { Id: 4, Name: "Выполнен, но не оплачен" }
              //  { Id: 7, Name: "Отменён" }
    ],
    schema: {
        model: {
            id: "Id",
            fields: {
                Id: { type: "number" },
                Name: { type: "string" }
            }
        }
    }
});

function formatTime(time) {
    var str = '',
        val;

    str += (val = time.getHours()) < 10 ? '0' + val + ":" : val + ":";
    str += (val = time.getMinutes()) < 10 ? '0' + val : val;
    str += ":00";
    return str;
}

function formatDate(date) { //Функция форматирования даты и времени
    var str = '',
        val;
    str += date.getFullYear() + "-";
    str += (val = date.getMonth() + 1) < 10 ? '0' + val + '-' : val + '-';
    str += (val = date.getDate()) < 10 ? '0' + val + ' ' : val + ' ';

    str += (val = date.getHours()) < 10 ? '0' + val + ":" : val + ":";
    str += (val = date.getMinutes()) < 10 ? '0' + val + ":" : val + ":";
    str += (val = date.getSeconds()) < 10 ? '0' + val : val;
    return str;
}

function playSound() {
    if (window.HTMLAudioElement) {
        try {
            var oAudio = $("#player")[0];

            if (oAudio.paused) {
                oAudio.play();
            }
            // else {
            //      oAudio.pause();
            //  }
        }
        catch (e) {
            if (window.console && console.error("Error:" + e));
        }
    }
}

function stopSound() {

    if (window.HTMLAudioElement) {
        try {
            var oAudio = $("#player")[0];

            if (oAudio.played) {
                oAudio.pause();
            }
            //  else {
            //      oAudio.pause();
            //  }
        }
        catch (e) {
            if (window.console && console.error("Error:" + e));
        }
    }
}

function enableAll() {
    $("div.editTemp").find("input[disabled='disabled']").prop('disabled', false);
    //Тип машины
    $("input[name='CarType']").data("kendoDropDownList").enable(true);
    //Тип Услуги
    $("input[name='WorkId']").data("kendoDropDownList").enable(true);
    //Бокс
    $("input[name='CarWashBoxId']").data("kendoDropDownList").enable(true);
    //Состояние
    $("input[name='OrderState']").data("kendoDropDownList").enable(true);
}

$(function () {

    $("li.active").removeClass("active");
    $("a[data-id=1]").parent("li").addClass("active");

    var ld = new Date(),
    sd = new Date(),
    flag = false,
    mobile = false,
   globalDate = new Date(),
        event = "";

    var popupNotification = $("#popupNotification").kendoNotification(
                    {
                        //appendTo: "#container",
                        //stacking: "down",
                        autoHideAfter: 0,
                        hide: function () {
                            // alert(123);stopSound()
                            st = null;
                            stopSound();
                        }
                    }).data("kendoNotification");

    kendo.culture("ru-RU");

    var evTemp = "#event-template",
        majorSize = "medium",
        constHeight = 130;

    if ($(document).width() < 680) {
        mobile = true;
        evTemp = "#event-template-mobile";
        majorSize = "small";
        constHeight = 90;
    }

    start = start.split(":");
    end = end.split(":");

    sd.setHours(+start[0], +start[1], 0);
    ld.setHours(+end[0], +end[1], 0);

    $("#scheduler").kendoScheduler({
        date: new Date(),
        timezone: "Europe/Moscow",
        currentTimeMarker: {
            useLocalTimezone: false
        },
       // dateHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'd')#</strong>"),
        workDayStart: sd, //начало рабочего дня
        workDayEnd: ld, //конец рабочего дня
        startTime: sd,
        endTime: ld,
        snap: false, //плавное перемещение события
        mobile: mobile,
        min: new Date("2016/1/1"),
        //toolbar: ["pdf"], //кнопка экспорта в pdf
        majorTick: 10, //показывает итервал в минутах за весь день
        minorTickCount: 1,
        //minorTimeHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'HH')#</strong>"), //показывает стиль разделённой по времени ячейки
        majorTimeHeaderTemplate: kendo.template("<strong style='font-size:" + majorSize + ";'>#=kendo.toString(date, 'HH:mm')#</strong>"),

        edit: function (e) {

            // $("#phone").mask("+7 (999) 999-99-99");
            $("#phone").kendoMaskedTextBox({
                mask: "+7 (000) 000-00-00"
                //promptChar: " "
            });

            $("#CarNumber").kendoMaskedTextBox({
                mask: "~000~~000",
                //value: "x000xx000",
                rules: {
                    "~": /[а-яА-Я]/
                }
            });

            if (mobile) {
                $("#statusMain").hide(); //Скрываем легенду
            }


            //Проверяем состояние заявки
            switch (e.event.OrderState) {
                case 2: { //В работе

                    $("input[name='title']").prop("disabled", false);
                   // $("input[name='CarNumber']").prop('disabled', false);

                    $("input[name='end1']").prop("disabled", false);

                    //Состояние
                    var ds = $("input[name='OrderState']").data("kendoDropDownList");
                    ds.setDataSource(dtStatesInWork);
                    ds.enable(true);
                    break;
                } case 3: { //Выполнен и оплачен
                    //дизейблим все поля и кнопку удалить
                   // $("a.k-scheduler-update").prop("disabled", true);
                    //  $("a.k-scheduler-delete").prop("disabled", true);
                    $("a.k-scheduler-update").hide();
                    $("a.k-scheduler-delete").hide();

                    $("input[name='ClientPhone']").prop("disabled", true);
                    $("input[name='CarNumber']").prop("disabled", true);
                    break;
                } case 4: { //Выполнен, но не оплачен

                    $("input[name='ClientPhone']").prop("disabled", true);
                    $("input[name='CarNumber']").prop("disabled", true);

                    //Состояние
                    var ds1 = $("input[name='OrderState']").data("kendoDropDownList");
                    ds1.setDataSource(dtStatesReady);
                    ds1.enable(true);
                    break;
                } default: {
                    //Енейблим все поля
                    enableAll();
                    break;
                }
            }

            if (event === "add") { //если заявка новая, то дизейблим
                //Состояние
                $("input[name='OrderState']").data("kendoDropDownList").enable(false);
                //Стоимость
                $("input[name='Cost']").prop("disabled", true);
                //Время конца
                $("input[name='end1']").prop("disabled", true);
            }

            $("#carMakes").kendoAutoComplete({
                dataSource: carMakes1,
                filter: "startswith",
                placeholder: "Название..."
            });

            //Прослушиваем событие выбора типа машины или выбора типа услуги
            $("ul[data-role='staticlist']").bind("click", function (event) {
                //Проверяем выбран ли тип авто
                var carType = $("input[name='CarType']").data("kendoDropDownList");
                //Проверяем выбран ли тип услуги
                var ds = $("input[name='WorkId']").data("kendoDropDownList");
                //Если выбрали услугу или тип машины
                if (event.target.id === ds.listView._optionID || event.target.id === carType.listView._optionID) {

                    //если не выбран, то ничего не выполняем
                    if (carType.selectedIndex !== 0 && ds.selectedIndex !== 0) {
                        //Выбираем id авто и id услуги
                        var workId = ds.dataSource.view()[ds.select() - 1].Id;
                        var carTypeId = carType.dataSource.view()[carType.select() - 1].Id;
                        //Делаем запрос к тарифу
                        $.get(rootUrl + "Tariff/GetTariff", { WorkId: workId, CarType: carTypeId }, function (data) {
                            //Вставляем полученную цену и время окончания
                            if (data.Tariff) {

                                $("input[name='Cost']").val(data.Tariff.Cost);
                                e.event.Cost = data.Tariff.Cost;
                                e.event.TariffId = data.Tariff.Id;
                                e.event.Duration = data.Tariff.Duration;

                                var tempDate = new Date();

                                var sT = $("input[name='start1']").val().split(":");

                                tempDate.setHours(+sT[0], +sT[1] + data.Tariff.Duration, 0);

                                var endT = formatTime(tempDate);

                                $("input[name='end1']").val(endT);

                            }

                        });
                    }
                }
            });

            $("input[name='start1']").bind("change", function () {
                //Проверяем выбран ли тип авто
                var carType = $("input[name='CarType']").data("kendoDropDownList");
                //Проверяем выбран ли тип услуги
                var ds = $("input[name='WorkId']").data("kendoDropDownList");
                if (carType.selectedIndex !== 0 && ds.selectedIndex !== 0) {
                    //Выбираем id авто и id услуги
                    var workId = ds.dataSource.view()[ds.select() - 1].Id;
                    var carTypeId = carType.dataSource.view()[carType.select() - 1].Id;
                    //Делаем запрос к тарифу
                    $.get(rootUrl + "Tariff/GetTariff", { WorkId: workId, CarType: carTypeId }, function (data) {
                        //Вставляем полученную цену и время окончания
                        if (data.Tariff) {

                            $("input[name='Cost']").val(data.Tariff.Cost);
                            e.event.Cost = data.Tariff.Cost;
                            e.event.TariffId = data.Tariff.Id;
                            e.event.Duration = data.Tariff.Duration;

                            var tempDate = new Date();

                            var sT = $("input[name='start1']").val().split(":");

                            tempDate.setHours(+sT[0], +sT[1] + data.Tariff.Duration, 0);

                            var endT = formatTime(tempDate);

                            $("input[name='end1']").val(endT);

                        }

                    });
                }

            });


            var inputCount = 2;
            $("#addWork").bind("click", function () {
                //Берём параграф
                var p = $(this).closest("p");
                //Создаём блок
                var div = $("<div id='advWork" + (inputCount - 1) + "' style='margin-top:10px;'>" +
                    "<label style='margin-right:4px;'>Доп. услуга:</label></div>");
                //Создаём новый input и возвращаем его
               /* var input = $("<input name='WorkId" + inputCount + "' data-option-label='-Доп. услуга " +
                    (inputCount - 1) + "-'  data-role='dropdownlist' data-source='worksAdvance'" +
                    "  data-value-field='Id' data-text-field='Name' />").appendTo(p);*/
                /*var input = $("<div><label>услуга " + (inputCount - 1) + "</label><input name='WorkId"
                    + inputCount + "' /></div>").appendTo(p);*/
                var input = $("<input name='WorkId" + inputCount + "' class='toLeft textToCenter' />").appendTo(div);

                input.kendoDropDownList({
                    dataTextField: "Name",
                    dataValueField: "Id",
                    optionLabel: "-Доп. услуга "+ (inputCount - 1) +"-",
                    dataSource: worksAdvance
                });

                p.append(div);

                inputCount++;
            });

            event = "edit";

            var start = formatTime(e.event.start);
            var end = formatTime(e.event.end);

            $("input[name='start1']").val(start);
            $("input[name='end1']").val(end);

            if (mobile) {
               /* $("input[name='start']").next().children("span.k-i-calendar").css("background-color", "black");
                $("input[name='start']").next().children("span.k-i-clock").css("background-color", "black").css("border-radius", "50%");
                $("input[name='end']").next().children("span.k-i-calendar").css("background-color", "black");
                $("input[name='end']").next().children("span.k-i-clock").css("background-color", "black").css("border-radius", "50%");*/
            }
        },
        cancel: function () {
            if (mobile) {
                $("#statusMain").show();
            }
            event = "";
        },
        editable: {
            template: $("#editor").html(),
            confirmation: "Вы уверены что хотите удалить событие?"
        },
        eventTemplate: $(evTemp).html(),
        footer: false,
        /*dataBinding: function(e) {

        },*/
        dataBound: function (e) {
           // if (mobile) {
           //     $("table.k-scheduler-table th.k-today").hide();
           //     $("table.k-scheduler-table tr:eq(1)").remove();
           // }
            //Разделяем события по цветам
            var view = this.view();
            var events = this.dataSource.view();
            var eventElement;
            var ev;

            for (var idx = 0, length = events.length; idx < length; idx++) {
                ev = events[idx];

                //get event element
                eventElement = view.element.find("[data-uid=" + ev.uid + "]");

                switch (ev.OrderState) {
                    case 1: { //Принят
                        eventElement.css("background-color", "#8B4513");
                        eventElement.css("border-color", "#8B4513");
                        break;
                    }
                    case 2: { //В работе
                        eventElement.css("background-color", "#BA55D3");
                        eventElement.css("border-color", "#BA55D3");
                        break;
                    }
                    case 3: { //Выполнен и оплачен 
                        //дизейблим кнопку удаления 
                        //  $(eventElement).find("a.k-event-delete").prop("disabled", true);
                        $(eventElement).find("a.k-event-delete").hide();
                        eventElement.css("background-color", "gray");
                        eventElement.css("border-color", "gray");
                        break;
                    }
                    case 4: { //Выполнен, но не оплачен 
                        eventElement.css("background-color", "#DC143C");
                        eventElement.css("border-color", "#DC143C");
                        break;
                    }
                }
            }

        },
        //showWorkHours: true, //показывает рабочие часы (стандарт от 8 до 4)
        height: $(document).height() - constHeight,// высота планировщика
        allDaySlot: false,
        resize: function (e) {
            //if (roomIsOccupied(e.start, e.end, e.event, e.resources)) {
            //    this.wrapper.find(".k-marquee-color").addClass("invalid-slot");
            //    e.preventDefault();
            // }
            //e.preventDefault();
        },
        resizeEnd: function (e) {
            //if (!checkAvailability(e.start, e.end, e.events)) {
            e.preventDefault();
            // }

        },
        move: function (e) {
           // if (roomIsOccupied(e.start, e.end, e.event, e.resources) || attendeeIsOccupied(e.start, e.end, e.event, e.resources)) {
           //     this.wrapper.find(".k-event-drag-hint").addClass("invalid-slot");
            //  }
           // e.preventDefault();
        },
        moveEnd: function (e) {
            //if (!checkAvailability(e.start, e.end, e.event, e.resources)) {
          //  e.preventDefault();
            //}
            e.preventDefault();
        },
        add: function (e) {
            event = "add";
            //if (!checkAvailability(e.event.start, e.event.end, e.event)) {
            // e.preventDefault();
            //  }
        },
        remove: function (e) {
            event = "";
            if (mobile) {
                $("#statusMain").show();
            }
        },
        save: function (e) {
            event = "";
            // if (!checkAvailability(e.event.start, e.event.end, e.event)) {
            //    e.preventDefault();
            // }
            /*  if (!e.event.CarNumber) {
                  alert("Неободимо ввести номер машины!");
                  e.preventDefault();
                  return false;
              }
              if (!e.event.ClientPhone) {
                  alert("Неободимо ввести номер телефона клиента!");
                  e.preventDefault();
                  return false;
              } */

            if (!$("input[name='start1']").val()) {
                alert("Неободимо выбрать время начала.");
                e.preventDefault();
                return false;
            }

            if (!$("input[name='end1']").val()) {
                alert("Неободимо выбрать время окончания.");
                e.preventDefault();
                return false;
            }

            if (e.event.CarType === -1) {
                alert("Неободимо выбрать тип машины.");
                e.preventDefault();
                return false;
            }
            if (!e.event.WorkId) {
                alert("Неободимо выбрать вид услуги.");
                e.preventDefault();
                return false;
            }

            var ts = $("input[name='start1']").val().split(":");
            var te = ($("input[name='end1']").val() + ":00").split(":");;

            e.event.start.setHours(+ts[0], +ts[1], 0);
            e.event.end.setHours(+te[0], +te[1], 0);

            if (mobile) {
                $("#statusMain").show();
            }
            return true;
        },
        navigate: function (e) {
            event = "";
            globalDate = e.date;
            var dt = $("#scheduler").data("kendoScheduler").dataSource;
            dt.read();

            return false;
        },
        views: [
            {
                type: "day",
                dateHeaderTemplate: kendo.template("<div style='text-align:center;'><strong class='head-date'>#=kendo.toString(date, 'yyyy-MM-dd')#</strong></div>") //заголовок дат по боксам
            }
        ],
        dataSource: {
            batch: true,
            transport: {
                read: {
                    url: rootUrl + "Order/GetOrders",
                    dataType: "json"
                },
                update: {
                    url: rootUrl + "Order/EditOrder",
                    dataType: "json",
                    type: "POST"
                },
                destroy: {
                    url: rootUrl + "Order/DeleteOrder",
                    dataType: "json",
                    type: "POST"
                },
                create: {
                    url: rootUrl + "Order/CreateOrder",
                    dataType: "json",
                    type: "POST"
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        if (operation === "destroy") {
                            return { orderId: JSON.stringify(options.models[options.models.length - 1].Id) };
                        }

                        var last = options.models[options.models.length - 1];

                        last.DateFrom = last.DateFrom.toISOString();
                        last.DateTo = last.DateTo.toISOString();

                        // last.DateFrom = formatDate(last.DateFrom);
                        //last.DateTo = formatDate(last.DateTo);

                        return last;

                    } else {
                        // var reqDate = 
                        globalDate.setHours(0, 0, 0);

                        // var temp = formatDate(globalDate);

                        return { Date: globalDate.toISOString() }; //globalDate.toISOString()
                    }
                }
            },
            requestStart: function (e) {
                if (e.type !== "read") {
                    $(".cssload-jumping").show();
                }

            },
            requestEnd: function (e) {
                if (e.type !== "read") {
                    $(".cssload-jumping").hide();
                }
            },
            schema: {
                data: function (data) {
                    //Если ошибок нет, то парсим даты, которые приходят
                    if (data.ErrorCode === 0) {
                        if (data.Orders) {
                            /*for (var i = 0; i < data.Orders.length; i++) {
                                //var d = new Date();

                                data.Orders[i].DateFrom = new Date(data.Orders[i].DateFrom);
                                data.Orders[i].DateTo = new Date(data.Orders[i].DateTo);
                            }*/
                            return data.Orders;
                        } else {
                            // return false;
                            $("#scheduler").data("kendoScheduler").dataSource.read();
                            return false;
                        }
                    } else {
                        alert(data.ErrorMessage);
                        $("#scheduler").data("kendoScheduler").dataSource.read();
                        return false;
                    }
                },
                model: {
                    id: "Id",
                    fields: {
                        Id: { type: "number" },
                        TariffId: { type: "number" },
                        OrderState: { type: "number" },
                        OrderType: { type: "number", editable: false, defaultValue: 1 },
                        start: { from: "DateFrom", type: "date" },
                        end: { from: "DateTo", type: "date" },
                        title: { from: "CarMake" }, // validation: { required: true }, defaultValue: "Без названия"
                        ClientPhone: { type: "string" },
                        CarNumber: { type: "string" },
                        CarWashBoxId: { type: "number", nullable: false },
                        CarType: { type: "number", defaultValue: -1 },
                        WorkId: { type: "number" },
                        Cost: { type: "number" }
                    }
                }
            }
        },
        group: {
            resources: ["room"]
        },
        resources: [
            {
                field: "OrderType",
                dataSource: [
                    { text: "Ручная запись", value: 1, color: "#007cc0" },
                    { text: "С прилолжения", value: 0, color: "#00a65a" }
                ],
                title: "Вид записи"
            },
            {
                field: "CarWashBoxId",
                name: "room",
                dataSource: {
                    transport: {
                        read: rootUrl + "Box/GetBoxes",
                        dataType: "json"
                    },
                    schema: {
                        parse: function (response) {
                            return response.Boxes;
                        },
                        model: {
                            id: "Id",
                            fields: {
                                value: { from: "Id" },
                                text: { from: "Name" }
                                // Color: { type: "string" }
                            }
                        }
                    }
                },
                title: "Бокс"
            }
        ]
    });

    if (mobile) {
        $("li.k-nav-prev").children().html("<-");
        $("li.k-nav-next").children().html("->");
        $("a.k-scheduler-refresh").html("<i class='fa fa-refresh'></i>");
    }

    /* function refresh() {
        $("#scheduler").data("kendoScheduler").dataSource.read();
    }

   //Текущие заявки с приложения
    var curOrder = [];
    //Конструктор для заявок
    var Order = function(id, carNumber) {
        this.id = id;
        this.carNumber = carNumber;
    } */

    setInterval(function () {

        popupNotification.hide();
        //Проверяем открыто ли окно с редактированием события
        if (event !== "edit") {
            $("#scheduler").data("kendoScheduler").dataSource.read().then(function() {
                var data = $("#scheduler").data("kendoScheduler").dataSource._pristineData;
                //Проходим по всем заявкам
                for (var i = 0; i < data.length; i++) {
                    //Если есть новые заявки и с приложения
                    if (data[i].OrderState === 0 && data[i].OrderType === 0) {

                        //  var order = new Order(data[i].Id, data[i].CarNumber);
                        //Заносим все текущие заявки
                        //  curOrder.push(order);
                        //Проходим по всем текущим заявкам
                        // for (var j = 0; j < curOrder.length; j++) {

                        

                        playSound();
                        popupNotification.show("Новая заявка! Гос. номер " + data[i].CarNumber);

                        start = setTimeout(function() {
                            stopSound();
                        }, 7000);
                        //   }
                        //Очищаем массив
                        // curOrder.length = 0;

                    }
                }


            });
        }


        //Проходим по все текущим 
        //for (var i=0;i<)
        /*var popupNotification = $("#popupNotification").kendoNotification(
                    {
                        //appendTo: "#container",
                        //stacking: "down",
                        autoHideAfter: 0
                    }).data("kendoNotification");

                    //списываем сумму с баланса
        //var score = +$("#score").val() - 25;

        //$("#score").val(score);

        popupNotification.show(data.message);*/
    }, 60000);

    function filterByResource(occurrences, resourceFieldName, value) {
        var result = [];
        var occurrence;

        for (var idx = 0, length = occurrences.length; idx < length; idx++) {
            occurrence = occurrences[idx];
            if (occurrence[resourceFieldName] === value) {
                result.push(occurrence);
            }
        }
        return result;
    }

    function occurrencesInRangeByResource(start, end, resourceFieldName, event, resources) {
        var scheduler = $("#scheduler").getKendoScheduler();

        // var temp = new Date();

        //  temp.setHours(start.getHours(), start.getMinutes() + 30, start.getSeconds());

        var occurrences = scheduler.occurrencesInRange(start, end);

        var idx = occurrences.indexOf(event);
        if (idx > -1) {
            occurrences.splice(idx, 1);
        }

        event = $.extend({}, event, resources);

        return filterByResource(occurrences, resourceFieldName, event[resourceFieldName]);
    }

    function attendeeIsOccupied(start, end, event, resources) {
        var occurrences = occurrencesInRangeByResource(start, end, "OrderType", event, resources);
        if (occurrences.length > 0) {
            return true;
        }
        return false;
    }

    function roomIsOccupied(start, end, event, resources) {
        var occurrences = occurrencesInRangeByResource(start, end, "CarWashBoxId", event, resources);
        if (occurrences.length > 0) {
            return true;
        }
        return false;
    }

    /*  function checkAvailability(start, end, event, resources) {

          //  if (attendeeIsOccupied(start, end, event, resources)) {
          //    setTimeout(function () {
         //         alert("This person is not available in this time period.");
         //     }, 0);

         //     return false;
        //  }

          if (roomIsOccupied(start, end, event, resources)) {
              setTimeout(function() {
                  alert("Этот бокс занят в заданный промежуток времени.");
              }, 0);

              return false;
          }

          return true;
      }*/
});