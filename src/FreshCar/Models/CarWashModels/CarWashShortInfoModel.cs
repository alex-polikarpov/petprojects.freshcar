﻿namespace FreshCar.Models.CarWashModels
{
    /// <summary>
    /// Краткая информация об автомойке
    /// </summary>
    public class CarWashShortInfoModel
    {
        /// <summary>
        /// Id автомойки
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название автомойки
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Широта (местоположение автомойки)
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Долгота (местоположение автомойки)
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// True, если мойка свободна иначе false
        /// </summary>
        public bool IsFree { get; set; }

        /// <summary>
        /// Город, в котором находится автомойка
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Улица, на которой находится автомойка
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Номер дома автомойки
        /// </summary>
        public string HouseNumber { get; set; }

        /// <summary>
        /// Минимальная цена услуг
        /// </summary>
        public decimal MinPrice { get; set; }
    }
}