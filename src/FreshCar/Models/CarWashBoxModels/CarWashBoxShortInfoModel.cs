﻿namespace FreshCar.Models.CarWashBoxModels
{
    /// <summary>
    /// Модель для отображения краткой информации о боксе
    /// </summary>
    public class CarWashBoxShortInfoModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}