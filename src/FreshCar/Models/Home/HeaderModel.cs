﻿namespace FreshCar.Models.Home
{
    /// <summary>
    /// Модель для отображения хедера
    /// </summary>
    public class HeaderModel
    {
        /// <summary>
        /// Название автомойки
        /// </summary>
        public string CarWashName { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string UserLogin { get; set; }

        /// <summary>
        /// Баланс 
        /// </summary>
        public decimal Balance { get; set; }
    }
}