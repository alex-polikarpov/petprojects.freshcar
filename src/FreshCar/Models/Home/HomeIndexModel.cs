﻿namespace FreshCar.Models.Home
{
    /// <summary>
    /// Модель для Home/Index
    /// </summary>
    public class HomeIndexModel
    {
        /// <summary>
        /// Время начала работы 
        /// </summary>
        public TimeModel StartTimeOfWork { get; set; }

        /// <summary>
        /// Время окончания работы
        /// </summary>
        public TimeModel EndTimeOfWork { get; set; }
    }
}