﻿namespace FreshCar.Models.Home
{
    /// <summary>
    /// Элемент левого меню
    /// </summary>
    public class LeftMenuItemModel
    {
        public string Controller { get; set; }

        public string Action { get; set; }

        /// <summary>
        /// Css класс для иконки
        /// </summary>
        public string IconClass { get; set; }

        /// <summary>
        /// Css класс для элемента меню
        /// </summary>
        public string CssClass { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Айди пункта меню, чтобы сделать выделение
        /// </summary>
        public int DataId { get; set; }
    }
}