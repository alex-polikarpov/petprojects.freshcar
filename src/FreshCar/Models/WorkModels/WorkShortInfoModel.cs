﻿namespace FreshCar.Models.WorkModels
{
    /// <summary>
    /// Модель для отображения краткой информации об услугах
    /// </summary>
    public class WorkShortInfoModel
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Является ли услуга системной
        /// </summary>
        public bool IsSystem { get; set; }
    }
}