﻿using System;
using FreshCar.Core.Entities;

namespace FreshCar.Models.OrderModels
{
    public class OrderModel
    {
        /// <summary>
        /// Id заказа
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Id тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Id бокса мойки, где будет происходить выполнение заказа
        /// </summary>
        public int CarWashBoxId { get; set; }

        /// <summary>
        /// Дата и время, когда начнется выполнение заказа (обязательно в UTC)
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Дата и время, когда закончится выполнение заказа (обязательно в UTC)
        /// </summary>
        public DateTime DateTo { get; set; }

        /// <summary>
        /// Стоимость заказа
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Тип заказа
        /// </summary>
        public OrderType OrderType { get; set; }

        /// <summary>
        /// Состояние заказа
        /// </summary>
        public OrderState OrderState { get; set; }

        /// <summary>
        /// Номер машины клиента
        /// </summary>
        public string CarNumber { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        public string ClientPhone { get; set; }

        /// <summary>
        /// Марка автомобиля
        /// </summary>
        public string CarMake { get; set; }

        /// <summary>
        /// Айди услуги
        /// </summary>
        public int WorkId { get; set; }

        /// <summary>
        /// Тип машины
        /// </summary>
        public CarType CarType { get; set; }
    }
}