﻿using System;
using FreshCar.Core.Entities;

namespace FreshCar.Models.Statistic
{
    /// <summary>
    /// Модель для отображения заявки на странице со статистикой
    /// </summary>
    public class OrderStatisticModel
    {
        /// <summary>
        /// Id заказа
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// Дата и время, когда начнется выполнение заказа (обязательно в UTC)
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Дата и время, когда закончилось выполнение заказа (в UTC)
        /// </summary>
        public DateTime DateTo { get; set; }

        /// <summary>
        /// Номер машины клиента
        /// </summary>
        public string CarNumber { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        public string ClientPhone { get; set; }

        /// <summary>
        /// Марка автомобиля
        /// </summary>
        public string CarMake { get; set; }

        /// <summary>
        /// Стоимость заказа
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Тип заказа
        /// </summary>
        public OrderType OrderType { get; set; }

        /// <summary>
        /// Состояние заказа
        /// </summary>
        public OrderState OrderState { get; set; }

        /// <summary>
        /// Тип машины
        /// </summary>
        public CarType CarType { get; set; }



        /// <summary>
        /// Id тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Айди услуги
        /// </summary>
        public int WorkId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string WorkName { get; set; }

        /// <summary>
        /// Id бокса мойки, где заказ был выполнен
        /// </summary>
        public int BoxId { get; set; }

        /// <summary>
        /// Название бокса
        /// </summary>
        public string BoxName { get; set; }

        /// <summary>
        /// Продолжительность мойки, высчитывается как дата окончания - дата начала.
        /// Это НЕ продолжительность из тарифа
        /// </summary>
        public int Duration { get; set; }
    }
}