﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using FreshCar.Infrastructure.Utils.DependencyResolution.Modules;
using FreshCar.Infrastructure.Utils.Mapping;
using FreshCar.Utils.Mapping;

namespace FreshCar
{
    public static class DependencyConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            // Register your MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            builder.RegisterModule<BusinessModule>();
            builder.RegisterModule<ServicesModule>();
            builder.RegisterModule<UtilsModule>();
            
            var mapper = new AutoMapperUtilityImpl();
            mapper.Initialize(MappingConfig.Configure());
            builder.RegisterInstance(mapper).As<IMapperUtility>();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}