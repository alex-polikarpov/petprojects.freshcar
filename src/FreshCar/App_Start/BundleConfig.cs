﻿using System.Web.Optimization;

namespace FreshCar
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/Scripts/jquery-2.1.4.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap")
                .Include("~/Scripts/bootstrap.min.js"));
        
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                 "~/Scripts/kendo.all.min.js",
                 "~/Scripts/kendo.culture.ru-RU.min.js",
                 "~/Scripts/kendo.timezones.min.js",
                 "~/Scripts/kendo.messages.ru-RU.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include("~/Scripts/app.js"));

            bundles.Add(new StyleBundle("~/Styles/css").Include(
                "~/Styles/bootstrap.min.css",
                "~/Styles/font-awesome.min.css",
                "~/Styles/ionicons.min.css",
                "~/Styles/AdminLTE.min.css",
                "~/Styles/skin-blue.min.css",
                "~/Styles/kendo.common-fiori.min.css",
                "~/Styles/kendo.fiori.min.css",
                "~/Styles/main.css"
                ));
        }
    }
}
