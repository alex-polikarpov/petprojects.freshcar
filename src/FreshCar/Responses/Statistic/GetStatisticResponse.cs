﻿using FreshCar.Models.Statistic;

namespace FreshCar.Responses.Statistic
{
    public class GetStatisticResponse : BaseResponse
    {
        /// <summary>
        /// Количество выполненных и оплаченных заказов за выбранный период
        /// </summary>
        public int FinishedOrdersCount { get; set; }

        /// <summary>
        /// Общее количество заявок
        /// </summary>
        public int AllOrdersCount { get; set; }

        /// <summary>
        /// Выручка за выбранный период
        /// </summary>
        public string Income { get; set; }

        /// <summary>
        /// Информация о заказах
        /// </summary>
        public OrderStatisticModel[] Orders { get; set; }        
    }
}