﻿using System.Collections.Generic;
using FreshCar.Models.CarWashAdditionalWorkModels;

namespace FreshCar.Responses.CarWashAdditionalWork
{
    public class GetAdditionalWorksResponseBuilder : BaseResponse
    {
        public CarWashAdditionalWorkModel[] CarWashAdditionalWorks { get; set; }
    }
}