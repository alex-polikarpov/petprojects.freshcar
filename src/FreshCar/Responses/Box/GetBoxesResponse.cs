﻿using FreshCar.Models.CarWashBoxModels;

namespace FreshCar.Responses.Box
{
    /// <summary>
    /// Ответ для метода получения боксов автомойки
    /// </summary>
    public class GetBoxesResponse :BaseResponse
    {
        public CarWashBoxShortInfoModel[] Boxes { get; set; }
    }
}