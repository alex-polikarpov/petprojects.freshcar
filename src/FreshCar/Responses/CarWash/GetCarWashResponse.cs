﻿using FreshCar.Models.CarWashModels;

namespace FreshCar.Responses.CarWash
{
    public class GetCarWashResponse : BaseResponse
    {
        public CarWashModel CarWash { get; set; }
    }
}