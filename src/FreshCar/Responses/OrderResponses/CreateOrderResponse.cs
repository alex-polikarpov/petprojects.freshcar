﻿namespace FreshCar.Responses.OrderResponses
{
    public class CreateOrderResponse : BaseResponse
    {
        public long Id { get; set; }
    }
}