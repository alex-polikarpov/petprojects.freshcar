﻿using FreshCar.Models.OrderModels;

namespace FreshCar.Responses.OrderResponses
{
    /// <summary>
    /// Ответ метода получения списка заказов
    /// </summary>
    public class GetOrdersResponse : BaseResponse
    {
        public OrderModel[] Orders { get; set; }
    }
}