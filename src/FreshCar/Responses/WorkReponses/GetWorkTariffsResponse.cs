﻿using FreshCar.Models.TariffModels;

namespace FreshCar.Responses.WorkReponses
{
    public class GetWorkTariffsResponse : BaseResponse
    {
        public TariffModel[] Tariffs { get; set; }
    }
}