﻿using FreshCar.Models.WorkModels;

namespace FreshCar.Responses.WorkReponses
{
    public class GetAllWorksResponse : BaseResponse
    {
        public WorkShortInfoModel[] Works { get; set; }
    }
}