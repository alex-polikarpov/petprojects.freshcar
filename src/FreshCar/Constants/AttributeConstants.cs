﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreshCar.Constants
{
    /// <summary>
    /// Константы для атрибутов
    /// </summary>
    public static class AttributeConstants
    {
        /// <summary>
        /// Константы с урлами
        /// </summary>
        public static class Urls
        {
            public const string MAIN_PAGE = "/Home";
        }

        /// <summary>
        /// Названия ролей
        /// </summary>
        public static class RolesNames
        {
            /// <summary>
            /// Рядовой сотрудник
            /// </summary>
            public const string ORDINARY = "ordinary";

            /// <summary>
            /// Администратор
            /// </summary>
            public const string ADMINISTRATOR = "administrator";

            /// <summary>
            /// Директор
            /// </summary>
            public const string DIRECTOR = "director";
        }
    }
}