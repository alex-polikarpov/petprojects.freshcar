﻿namespace FreshCar.Constants
{
    /// <summary>
    /// Кастомные клеймы для Asp.net Identity
    /// </summary>
    public static class CustomClaimsConstants
    {
        public const string COMPANY_ID = "CompanyId";

        public const string CAR_WASH_ID = "CarWashId";

        public const string EMPLOYEE_ID = "EmployeeId";
    }
}