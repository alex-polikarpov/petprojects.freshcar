﻿namespace FreshCar.Constants
{
    /// <summary>
    /// Константы для регулярных выражений
    /// </summary>
    public static class RegexConstants
    {
        public const string PASSWORD = "^[0-9a-zA-Z]+$";
    }
}