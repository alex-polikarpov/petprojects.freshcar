﻿using Microsoft.AspNet.Identity;

namespace FreshCar.Autentication
{
    /// <summary>
    /// Этот класс необходим для работы с Asp.net Identity
    /// </summary>
    public class ApplicationUser : IUser
    {
        public string Id { get; set; }

        public string UserName { get; set; }
    }
}