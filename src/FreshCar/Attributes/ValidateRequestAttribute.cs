﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using FreshCar.Responses;

namespace FreshCar.Attributes
{
    /// <summary>
    ///     Фильтр, который делает стандартные проверки модели
    ///     В случае ошибки возвращает BaseApiResponse
    ///     Справедлив для методов апи с методо post,put - где функция принимает один параметр запрос
    /// </summary>
    public class ValidateRequestAttribute : ActionFilterAttribute
    {
        /// <summary>
        ///  Выполняется перед выполенением экшена
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Validate(filterContext);
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// Функция валидации запроса
        /// </summary>
        private void Validate(ActionExecutingContext actionContext)
        {
            var actionArguments = actionContext.ActionParameters;
            var viewData = actionContext.Controller.ViewData;

            if (actionArguments.Count == 1)
            {
                var request = actionArguments.First();
                if (request.Value == null)
                {
                    viewData.ModelState.AddModelError("", "Empty request");
                }
            }

            if (viewData.ModelState.IsValid) return;

            var response = new EmptyResponse()
            {
                ErrorCode = (int) HttpStatusCode.BadRequest,
                ErrorMessage = viewData.ModelState.Values.SelectMany(e => e.Errors).FirstOrDefault()?.ErrorMessage
            };

            actionContext.Result = new JsonResult()
            {
                Data = response,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}