﻿using System.Web.Mvc;

namespace FreshCar.Attributes
{
    public class AllowCrossFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "http://app.icenium.com");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Headers", "ORDsystem");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Method", "GET");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Credentials", "true");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Max-Age", "86400");
            base.OnActionExecuting(filterContext);
        }
    }
}