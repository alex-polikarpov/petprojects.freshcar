﻿using System;
using System.ComponentModel.DataAnnotations;
using FreshCar.Utils.Extensions;

namespace FreshCar.Attributes
{
    /// <summary>
    /// Атрибут для проверки, что значение даты позже, чем установленное
    /// </summary>
    public class DateLaterThanAttribute : ValidationAttribute
    {
        public int Day { get; }
        public int Month { get; }
        public int Year { get; }

        public DateLaterThanAttribute(int day, int month, int year)
        {
            Day = day;
            Month = month;
            Year = year;
            ErrorMessage = "Невалидная дата";
        }

        public override bool IsValid(object value)
        {
            var date = value as DateTime?;

            if (date == null)
                return true;

            var checkDate = new DateTime(Year, Month, Day);

            if (date.Value.ToUtc() < checkDate)
                return false;

            return true;
        }
    }
}