﻿using FreshCar.Business.Commands;
using FreshCar.Responses;

namespace FreshCar.Extensions
{
    /// <summary>
    /// Расширения для результатов команд
    /// </summary>
    public static class CommandResultExtensions
    {
        public static TResponse ConverToResponse<TResponse>(this ICommandResult commandResult)
            where TResponse : BaseResponse, new ()
        {
            var result = new TResponse();
            result.ErrorCode = commandResult.ErrorCode;
            result.ErrorMessage = commandResult.IsSuccess ? null : commandResult.ErrorMessage;
            return result;
        }
    }
}