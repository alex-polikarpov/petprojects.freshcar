﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Utils.Geolocation;
using Geocoding.Google;

namespace FreshCar.Infrastructure.Utils.Geolocation
{
    /// <summary>
    /// Утилита геолокации
    /// </summary>
    internal class GeolocationUtility : IGeolocationUtility
    {
        private const string _GOOGLE_API_KEY = "AIzaSyAORRAYBBwbw-M7yKnjbEXLPvsa6txgW-w";

        /// <summary>
        /// Получает местоположение по адресу
        /// </summary>
        /// <param name="city">Город</param>
        /// <param name="street">Улица</param>
        /// <param name="house">Дом</param>
        public async Task<GeolocationInfo> GetLocationByAddress(string city, string street, string house)
        {
            try
            {
                var geocoder = new GoogleGeocoder(_GOOGLE_API_KEY);
                var addresses = await geocoder.GeocodeAsync(city + ", " + street + ", " + house);
                var address = addresses.FirstOrDefault();

                if (address == null)
                    return null;

                var result = new GeolocationInfo()
                {
                    Latitude = address.Coordinates.Latitude,
                    Longitude = address.Coordinates.Longitude
                };

                return result;
            }
            catch (Exception ex)
            {
                throw new GeolocationUtilityException("Во время попытки получения координат произошла ошибка", ex);
            }                        
        }
    }
}
