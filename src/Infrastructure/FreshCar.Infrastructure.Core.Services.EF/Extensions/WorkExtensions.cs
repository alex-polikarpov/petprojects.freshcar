﻿using System.Linq;
using System.Security.AccessControl;
using FreshCar.Core.Entities;
using FreshCar.Utils.Extensions;

namespace FreshCar.Infrastructure.Core.Services.EF.Extensions
{
    internal static class WorkExtensions
    {
        /// <summary>
        /// Фильтрует услуги по типу и возвращает только MainCreatedByUser и MainSystem
        /// </summary>
        public static IQueryable<Work> OnlyMainWorks(this IQueryable<Work> query)
        {
            return query.Where(x => x.WorkType == WorkType.MainCreatedByUser || x.WorkType == WorkType.MainSystem);
        }

        /// <summary>
        /// Фильтрует услуги по типу и возвращает только AdditionalCreatedByUser и AdditionalSystem
        /// </summary>
        public static IQueryable<Work> OnlyAdditionalWorks(this IQueryable<Work> query)
        {
            return query.Where(x => x.WorkType == WorkType.AdditionalCreatedByUser ||
                                    x.WorkType == WorkType.AdditionalSystem);
        }

        /// <summary>
        /// Фильтрует услуги по состоянию
        /// </summary>
        public static IQueryable<Work> FilterByState(this IQueryable<Work> query, WorkState state)
        {
            return query.Where(x => x.WorkState == state);
        }

        /// <summary>
        /// фильтрует по Id
        /// </summary>
        public static IQueryable<Work> FilterById(this IQueryable<Work> query, int id)
        {
            return query.Where(x => x.Id == id);
        }

        /// <summary>
        /// фильтрует по типу услуги
        /// </summary>
        public static IQueryable<Work> FilterByType(this IQueryable<Work> query, WorkType type)
        {
            return query.Where(x => x.WorkType == type);
        }

        /// <summary>
        /// Фильтрует услуги и возвращает только общие, не привязанные к фирмам
        /// </summary>
        public static IQueryable<Work> WithoutCompany(this IQueryable<Work> query)
        {
            return query.Where(x => x.CompanyId == null);
        }

        /// <summary>
        /// Исключает удаленные услуги
        /// </summary>
        public static IQueryable<Work> NotDeleted(this IQueryable<Work> query)
        {
            return query.Where(x => x.WorkState != WorkState.Deleted);
        }

        /// <summary>
        /// Фильтрует по айди фирмы. Также возвращает сист. услуги без привязки к фирмам
        /// </summary>
        public static IQueryable<Work> FilterByCompanyId(this IQueryable<Work> query, int companyId)
        {
            return query.Where(x => x.CompanyId == null || x.CompanyId == companyId);
        }

        /// <summary>
        /// Фильтрует услуги по имени без учета регистра
        /// </summary>
        public static IQueryable<Work> FilterByName(this IQueryable<Work> query, string name)
        {
            if (name.IsNullOrWhiteSpace())
                return query;

            return query.Where(x => x.Name.ToLower() == name.ToLower());
        }
    }
}
