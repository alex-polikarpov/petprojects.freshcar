﻿using System;
using System.Linq;
using FreshCar.Core.Entities;

namespace FreshCar.Infrastructure.Core.Services.EF.Extensions
{
    internal static class OrderExtensions
    {
        /// <summary>
        /// Возвращает только активные заказы в статусе (Accepted, FinishedAndNotPaid, InWork, New)
        /// </summary>
        public static IQueryable<Order> OnlyActive(this IQueryable<Order> query)
        {
            return query.Where(x =>
                x.OrderState == OrderState.Accepted ||
                x.OrderState == OrderState.FinishedAndNotPaid ||
                x.OrderState == OrderState.InWork ||
                x.OrderState == OrderState.New);
        }

        /// <summary>
        /// Возвращает все заказы кроме отмененных
        /// </summary>
        public static IQueryable<Order> NotCanceled(this IQueryable<Order> query)
        {
            return query.Where(x =>
                x.OrderState != OrderState.CanceledByAdministrator &&
                x.OrderState != OrderState.CanceledByClient);
        }

        /// <summary>
        /// Возвращает все заказы кроме удаленных
        /// </summary>
        public static IQueryable<Order> NotDeleted(this IQueryable<Order> query)
        {
            return query.Where(x => x.OrderState != OrderState.Deleted);
        }

        /// <summary>
        /// Фильтрует заказы по датам для выявления свободен ли бокс
        /// </summary>
        /// <param name="dateFrom">Дата начала нового заказа</param>
        /// <param name="dateTo">Дата окончания нового заказа</param>
        public static IQueryable<Order> FilterByDates(this IQueryable<Order> query,
            DateTime dateFrom, DateTime dateTo)
        {
            return query.Where(x =>
                x.DateFrom == dateFrom ||
                (dateFrom < x.DateFrom && dateTo > x.DateFrom) ||
                (dateFrom > x.DateFrom && dateFrom <= x.DateTo));
        }
    }
}
