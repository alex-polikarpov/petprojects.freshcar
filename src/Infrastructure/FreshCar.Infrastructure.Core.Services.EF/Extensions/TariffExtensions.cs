﻿using System.Linq;
using FreshCar.Core.Entities;

namespace FreshCar.Infrastructure.Core.Services.EF.Extensions
{
    internal static class TariffExtensions
    {
        public static IQueryable<Tariff> FilterByCarType(this IQueryable<Tariff> query, CarType? carType)
        {
            if (!carType.HasValue)
                return query;

            return query.Where(x => x.CarType == carType.Value);
        }
    }
}
