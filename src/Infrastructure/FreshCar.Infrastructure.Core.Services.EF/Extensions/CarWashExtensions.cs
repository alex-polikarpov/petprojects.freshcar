﻿using System.Linq;
using FreshCar.Core.Entities;

namespace FreshCar.Infrastructure.Core.Services.EF.Extensions
{
    internal static class CarWashExtensions
    {
        /// <summary>
        /// Фильтрует автомойки по списку системных доп услуг
        /// </summary>
        /// <param name="additionalWorksId">Айдишники услуг, которые должны быть у автомойки</param>
        public static IQueryable<CarWash> FilterBySystemAdditionalWorks(this IQueryable<CarWash> query,
            int[] additionalWorksId)
        {
            if (additionalWorksId == null)
                return query;

            additionalWorksId = additionalWorksId.Distinct().ToArray();

            foreach(var workId in additionalWorksId)
            {
                query = query.Where(x => x.CarWashAdditionalWorks.Any(y =>
                    y.WorkId == workId && y.Work.WorkType == WorkType.AdditionalSystem));
            }

            return query;
        }

        /// <summary>
        /// Фильтрует автомойке по стоимости системной услуги
        /// </summary>
        public static IQueryable<CarWash> FilterBySystemWorkPrice(this IQueryable<CarWash> query,
            decimal? price, int? workId, CarType? carType)
        {
            if (!price.HasValue || !workId.HasValue || !carType.HasValue)
                return query;

            return query.Where(x => x.Tariffs.Any(y =>
                y.WorkId == workId.Value && 
                y.CarType == carType.Value && 
                y.Cost <= price.Value &&
                y.Work.WorkType == WorkType.MainSystem));
        }
    }
}
