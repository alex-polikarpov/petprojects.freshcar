﻿using System.Data.Entity.Migrations;
using System.Linq;
using FreshCar.Core.Entities;

namespace FreshCar.Infrastructure.Core.Services.EF
{
    /// <summary>
    /// Конфигурация для контекста БД
    /// </summary>
    internal class FreshCarContextConfiguration : DbMigrationsConfiguration<FreshCarContext>
    {
        public FreshCarContextConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FreshCarContext context)
        {
            //ЗДЕСЬ ДОЛЖНЫ ДОБАВЛЯТЬ ДАННЫЕ ИНИЦИАЛИЗАЦИИ
            if (!context.Works.Any())
            {
                context.Works.Add(new Work
                {
                    Name = "Экспресс мойка",
                    WorkType = WorkType.MainSystem,
                    SortOrder = 1,
                    WorkState = WorkState.Active,
                    Description = "Быстрая бесконтактная сбивка грязи без использования химии"
                });

                context.Works.Add(new Work
                {
                    Name = "Наружная мойка с пеной",
                    WorkType = WorkType.MainSystem,
                    SortOrder = 2,
                    WorkState = WorkState.Active,
                    Description = "Быстрая бесконтактная сбивка грязи с использованием химии"
                });

                context.Works.Add(new Work
                {
                    Name = "Комплекс без багажника",
                    WorkType = WorkType.MainSystem,
                    SortOrder = 3,
                    WorkState = WorkState.Active,
                    Description = "Комплексная мойка автомобиля без багажника"
                });

                context.Works.Add(new Work
                {
                    Name = "Комплекс с багажником",
                    WorkType = WorkType.MainSystem,
                    SortOrder = 4,
                    WorkState = WorkState.Active,
                    Description = "Комплексная мойка автомобиля с багажником"
                });

                context.Works.Add(new Work
                {
                    Name = "Безналичная оплата",
                    WorkType = WorkType.AdditionalSystem,
                    SortOrder = 1,
                    WorkState = WorkState.Active,
                    Description = "Оплата кредитной картой"
                });

                context.Works.Add(new Work
                {
                    Name = "Зона отдыха",
                    WorkType = WorkType.AdditionalSystem,
                    SortOrder = 2,
                    WorkState = WorkState.Active,
                    Description = "Комната отдыха для клиентов"
                });

                context.Works.Add(new Work
                {
                    Name = "Кафе",
                    WorkType = WorkType.AdditionalSystem,
                    SortOrder = 3,
                    WorkState = WorkState.Active,
                    Description = "Кафе для клиентов автомойки"
                });

                context.Works.Add(new Work
                {
                    Name = "Wi-Fi",
                    WorkType = WorkType.AdditionalSystem,
                    SortOrder = 4,
                    WorkState = WorkState.Active,
                    Description = "Бесплатный доступ к Wi-Fi"
                });

                context.Works.Add(new Work
                {
                    Name = "Терминал оплаты",
                    WorkType = WorkType.AdditionalSystem,
                    SortOrder = 5,
                    WorkState = WorkState.Active,
                    Description = "Возможность пополнить счёт телефона и т.д."
                });

                context.Works.Add(new Work
                {
                    Name = "Авто-магазин",
                    WorkType = WorkType.AdditionalSystem,
                    SortOrder = 6,
                    WorkState = WorkState.Active,
                    Description = "Магазин, в котором можно приобрести запчасти или акссесуары для автомобиля"
                });

                context.Works.Add(new Work
                {
                    Name = "Шиномонтаж",
                    WorkType = WorkType.AdditionalSystem,
                    SortOrder = 7,
                    WorkState = WorkState.Active,
                    Description = "Возможность починить либо подкачать колёса"
                });

                context.SaveChanges();
            }
        }
    }
}
