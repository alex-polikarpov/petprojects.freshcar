﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Infrastructure.Core.Services.EF.Extensions;

namespace FreshCar.Infrastructure.Core.Services.EF.Services
{
    /// <summary>
    /// Серис для работы с боксами
    /// </summary>
    internal class CarWashBoxService : BaseService, ICarWashBoxService
    {
        public CarWashBoxService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        /// <summary>
        /// Проверяет свободен ли бокс (нет ли заказов в это время)
        /// </summary>
        /// <param name="boxId">Id бокса</param>
        /// <param name="dateFrom">Дата начала заказа</param>
        /// <param name="dateTo">Дата окончания заказа</param>
        public async Task<bool> BoxIsFree(int boxId, DateTime dateFrom, DateTime dateTo)
        {
            var repository = UnitOfWork.GetRepository<Order>();

            var hasAnyOrder = await repository.Query().AsNoTracking()
                .OnlyActive()
                .Where(x => x.CarWashBoxId == boxId)
                .FilterByDates(dateFrom, dateTo)
                .ToListAsync();

            return !hasAnyOrder.Any();
        }

        /// <summary>
        /// Возвращает бокс автомойки
        /// </summary>
        /// <param name="readOnly">true, если данные нужны только для чтения</param>
        public async Task<CarWashBox> GetBox(int boxId, bool readOnly)
        {
            var repository = UnitOfWork.GetRepository<CarWashBox>();
            var query = repository.Query();

            if (readOnly)
                query = query.AsNoTracking();

            return await query.Where(x=>x.State != CarWashBoxState.Deleted)
                .FirstOrDefaultAsync(x => x.Id == boxId);
        }
    }
}
