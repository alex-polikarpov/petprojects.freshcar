﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Infrastructure.Core.Services.EF.Extensions;

namespace FreshCar.Infrastructure.Core.Services.EF.Services
{
    /// <summary>
    /// Сервис для работы с сущностями автомоек
    /// </summary>
    internal class CarWashService : BaseService,ICarWashService
    {
        public CarWashService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        /// <summary>
        /// Проверяет используется ли уже адрес в нашей системе. 
        /// </summary>
        /// <param name="latitude">Широта</param>
        /// <param name="longitude">Долгота</param>
        public async Task<bool> AddressAlreadyExistsAsync(double latitude, double longitude)
        {
            var repository = UnitOfWork.GetRepository<CarWash>();

            return await repository.Query().AsNoTracking()                                
                .Where(x => x.Latitude == latitude && x.Longitude == longitude)
                .AnyAsync();
        }

        /// <summary>
        /// Возвращает список автомоек (только в статусе "Работает") с краткой информацией.
        /// </summary>
        ///<param name="filters">Фильтры</param>
        public async Task<IEnumerable<CarWashShortInfoDto>> GetCarWashesShortInfoAsync(
            GetCarWashesShortInfoFilters filters)
        {
            var repository = UnitOfWork.GetRepository<CarWash>();

            var query = repository.Query().AsNoTracking()
                .Where(x => x.State == CarWashState.Works);

            if (filters != null)
            {
                query = query
                    .FilterBySystemAdditionalWorks(filters.AdditionalWorksId)
                    .FilterBySystemWorkPrice(filters.Price, filters.WorkId, filters.CarType);
            }

            return await query
                .Select(x => new CarWashShortInfoDto()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Longitude = x.Longitude,
                    Latitude = x.Latitude,
                    City = x.City,
                    Street = x.Street,
                    HouseNumber = x.HouseNumber,
                    MinPrice = x.Tariffs.Where(y => y.CarType == CarType.PassengerForeign)
                        .Select(y => y.Cost).Min()
                })
                .ToListAsync();
        }

        /// <summary>
        /// true, если автомойка свободна и на нее можно записаться, иначе false
        /// </summary>
        public async Task<bool> CarWashIsFreeAsync(int carWashId)
        {
            var boxesRepository = UnitOfWork.GetRepository<CarWashBox>();
            var boxesId = await boxesRepository.Query().AsNoTracking()
                .Where(x => x.CarWashId == carWashId)
                .Where(x => x.State == CarWashBoxState.Works)
                .Select(x => x.Id)
                .ToListAsync();

            if (boxesId.Count == 0)
                return false;

            var orderRepository = UnitOfWork.GetRepository<Order>();
            var date = DateTime.UtcNow;
            //получаем список текущих заказов, дальше нужно проверить есть ли боксы без заказов
            var orders = await orderRepository.Query().AsNoTracking()
                .Where(x => x.CarWashBoxId != null)
                .Where(x => x.OrderState == OrderState.New ||
                            x.OrderState == OrderState.Accepted ||
                            x.OrderState == OrderState.InWork)
                .Where(x => boxesId.Contains(x.CarWashBoxId.Value))
                .Where(x => date < x.DateTo)
                .Select(x => new {BoxId = x.CarWashBoxId})
                .ToListAsync();

            foreach (var boxId in boxesId)
            {
                var boxHasOrder = orders.Any(x => x.BoxId == boxId);
                if (!boxHasOrder)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Предоставляет информацию об автомойке (только если она в статусе работает)
        /// </summary>
        public async Task<CarWash> GetCarWashAsync(int carWashId)
        {
            var repository = UnitOfWork.GetRepository<CarWash>();
            return await repository.Query()
                .Where(x => x.Id == carWashId && x.State == CarWashState.Works)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Возвращает массив боксов автомойки (только в статусе работает). Только для чтения
        /// </summary>
        /// <param name="carWashId">Айди автомойки</param>
        /// <param name="onlyWorked">True, если надо вернуть только боксы в статусе Работает</param>
        public async Task<CarWashBox[]> GetCarWashBoxesAsync(int carWashId, bool onlyWorked = true)
        {
            var repository = UnitOfWork.GetRepository<CarWashBox>();
            var query = repository.Query().AsNoTracking()                
                .Where(x => x.CarWashId == carWashId && x.State != CarWashBoxState.Deleted);

            if (onlyWorked)
                query = query.Where(x => x.State == CarWashBoxState.Works);

            return await query.OrderBy(x=>x.Name).ToArrayAsync();
        }

        /// <summary>
        /// Возвращает кол-во рабочих боксов, которые есть у автомойки 
        /// </summary>
        public async Task<int> GetBoxesCount(int carWashId)
        {
            var repository = UnitOfWork.GetRepository<CarWashBox>();
            return await repository.Query().AsNoTracking()
                .Where(x => x.CarWashId == carWashId)
                .Where(x => x.State != CarWashBoxState.Deleted)
                .CountAsync();
        }

        /// <summary>
        /// Возвращает фотографии автомойки
        /// </summary>
        public async Task<CarWashPhoto[]> GetPhotos(int carWashId)
        {
            var repository = UnitOfWork.GetRepository<CarWashPhoto>();

            return await repository.Query().AsNoTracking()
                .Where(x => x.CarWashId == carWashId)
                .ToArrayAsync();
        }
    }
}
