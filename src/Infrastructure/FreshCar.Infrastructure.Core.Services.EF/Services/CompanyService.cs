﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;

namespace FreshCar.Infrastructure.Core.Services.EF.Services
{
    /// <summary>
    /// Сервис для работы с фирмами
    /// </summary>
    internal class CompanyService : BaseService,ICompanyService
    {
        public CompanyService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        /// <summary>
        /// Ищет айди компании, которая принадлежит пользователю
        /// </summary>
        public async Task<int> GetCompanyId(Guid userId)
        {
            var accountRepository = UnitOfWork.GetRepository<Account>();
            var companyRepository = UnitOfWork.GetRepository<Company>();

            var employeeId = await accountRepository.Query().AsNoTracking()
                .Where(x => x.Id == userId)
                .Select(x => x.EmployeeId)
                .FirstOrDefaultAsync();

            if (employeeId == 0)
                return 0;

            return await companyRepository.Query().AsNoTracking()
                .Where(x => x.Employees.Any(y => y.Id == employeeId))
                .Select(x => x.Id)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Возвращает фирму по ее айди
        /// </summary>
        /// <param name="readOnly">true, если данные нужны только для чтения, иначе false</param>
        public async Task<Company> GetCompany(int companyId, bool readOnly)
        {
            var repository = UnitOfWork.GetRepository<Company>();
            var query = repository.Query();

            if (readOnly)
                query = query.AsNoTracking();

            return await query.FirstOrDefaultAsync(x => x.Id == companyId);
        }
    }
}
