﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Extensions;

namespace FreshCar.Infrastructure.Core.Services.EF.Services
{
    /// <summary>
    /// Сервис для работы с аккаунтами
    /// </summary>
    internal class AccountService : BaseService, IAccountService
    {
        public AccountService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        /// <summary>
        /// Возвращает основную информацию по пользователю
        /// </summary>
        public async Task<UserInfo> GetUserInfoAsync(Guid userId)
        {
            var result = new UserInfo();
            var repository = UnitOfWork.GetRepository<Account>();

            var account = await repository.Query().AsNoTracking()
                .Where(x => x.Id == userId)
                .FirstOrDefaultAsync();

            if (account == null)
                return result;

            result.AccountId = account.Id;
            result.EmployeeId = account.EmployeeId;
            result.Email = account.Email;

            var companyId = await UnitOfWork.GetRepository<Employee>().Query().AsNoTracking()
                .Where(x => x.Id == result.EmployeeId.Value)
                .Select(x => x.CompanyId)
                .FirstOrDefaultAsync();

            if (companyId == 0)
                return result;

            result.CompanyId = companyId;

            var carWashId = await UnitOfWork.GetRepository<CarWash>().Query().AsNoTracking()
                .Where(x => x.CompanyId == companyId)
                .Select(x => x.Id)
                .FirstOrDefaultAsync();

            if (carWashId == 0)
                return result;

            result.CarWashId = carWashId;

            result.Role = await UnitOfWork.GetRepository<CarWashEmployee>().Query().AsNoTracking()
                .Where(x => x.CarWashId == carWashId && x.EmployeeId == result.EmployeeId.Value)
                .Select(x => x.EmployeeRole)
                .FirstOrDefaultAsync();

            return result;
        }

        /// <summary>
        /// Возвращает аккаунт по логину и паролю или null, если он не найден
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="passwordHash">хеш код пароля пользователя</param>
        public async Task<Account> GetAccountAsync(string login, string passwordHash)
        {
            if (login.IsNullOrWhiteSpace() || passwordHash.IsNullOrWhiteSpace())
                return null;

            var lowerLogin = login?.ToLower();
            var repository = UnitOfWork.GetRepository<Account>();

            return await repository.Query()
                .Where(x => x.Email.ToLower() == lowerLogin && x.PasswordHash == passwordHash)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Проверяет есть ли пользователь с таким логином
        /// </summary>
        public async Task<bool> UserIsExistsAsync(string login)
        {
            var repository = UnitOfWork.GetRepository<Account>();
            login = login?.ToLower();
            return await repository.Query()
                .Where(x => x.Email.ToLower() == login)
                .AnyAsync();
        }

        /// <summary>
        /// Получает аккаунт сотрудника или нулл, если у него нет аккаунта
        /// </summary>
        public async Task<Account> GetEmployeeAccount(int employeeId)
        {
            var accountRepository = UnitOfWork.GetRepository<Account>();
            return await accountRepository.Query()
                .Where(x => x.EmployeeId == employeeId)
                .SingleOrDefaultAsync();
        }

        /// <summary>
        /// Проверяет, может ли сотрудник сменить логин
        /// </summary>
        /// <param name="employeeId">Id сотрудника</param>
        /// <param name="newLogin">Новый логин</param>
        public async Task<bool> EmployeeCanChangeLogin(int employeeId, string newLogin)
        {
            var lowerLogin = newLogin?.ToLower();
            var repository = UnitOfWork.GetRepository<Account>();

            var accountIsExists = await repository.Query().AsNoTracking()
                .Where(x => x.Email.ToLower() == lowerLogin && x.EmployeeId != employeeId)
                .AnyAsync();

            return !accountIsExists;
        }
    }
}
