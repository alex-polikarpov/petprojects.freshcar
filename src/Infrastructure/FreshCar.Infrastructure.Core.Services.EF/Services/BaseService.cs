﻿using FreshCar.Core.Services;

namespace FreshCar.Infrastructure.Core.Services.EF.Services
{
    /// <summary>
    /// Базовый сервис для работы с БД
    /// </summary>
    internal abstract class BaseService
    {
        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected IUnitOfWork UnitOfWork { get; }
    }
}