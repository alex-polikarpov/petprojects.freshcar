﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;

namespace FreshCar.Infrastructure.Core.Services.EF.Services
{
    /// <summary>
    /// Сервис для работы с сотрудниками
    /// </summary>
    internal class EmployeeService : BaseService, IEmployeeService
    {
        public EmployeeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        /// <summary>
        /// Возвращает список администраторов автомойки. Только для чтения
        /// </summary>
        /// <param name="carWashId">Id автомойки</param>
        public async Task<CarWashEmployeeInfo[]> GetCarWashAdministrators(int carWashId)
        {
            var carWashEmployeeRepository = UnitOfWork.GetRepository<CarWashEmployee>();

            var employees = await carWashEmployeeRepository.Query()
                .AsNoTracking()
                .Where(x => x.CarWashId == carWashId && x.EmployeeRole == EmployeeRole.Administrator)
                .Select(x=>x.Employee)
                .ToArrayAsync();

            if (employees.Length == 0)
                return null;

            var employeesId = employees.Select(x => x.Id).ToArray();

            var accountRepository = UnitOfWork.GetRepository<Account>();
            var accounts = await accountRepository.Query().AsNoTracking()
                .Where(x => employeesId.Contains(x.EmployeeId))
                .ToArrayAsync();

            var result = new List<CarWashEmployeeInfo>();
            foreach (var employee in employees)
            {
                var account = accounts.FirstOrDefault(x => x.EmployeeId == employee.Id);

                if(account!= null && account.AccountState != AccountState.Verified)
                    continue;

                var info = new CarWashEmployeeInfo()
                {
                    Id = employee.Id,
                    Phone = employee.Phone,
                    Role = EmployeeRole.Administrator,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    MiddleName = employee.MiddleName
                };

                if (account != null)
                    info.Login = account.Email;

                result.Add(info);
            }

            return result.ToArray();
        }

        /// <summary>
        /// Возвращает сотрудника
        /// </summary>
        public async Task<Employee> GetEmployee(int employeeId)
        {
            var repository = UnitOfWork.GetRepository<Employee>();
            return await repository.Query()
                .Where(x => x.Id == employeeId)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Возвращает сотрудника автомойки
        /// </summary>
        /// <param name="employeeId">Айди сотрудника</param>
        /// <param name="carWashId">Айди автомойки</param>
        public async Task<CarWashEmployee> GetCarWashEmployee(int employeeId, int carWashId)
        {
            var repository = UnitOfWork.GetRepository<CarWashEmployee>();
            return await repository.Query()
                .FirstOrDefaultAsync(x => x.CarWashId == carWashId && x.EmployeeId == employeeId);
        }
    }
}
