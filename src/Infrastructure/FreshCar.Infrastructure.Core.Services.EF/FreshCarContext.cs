﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using FreshCar.Core.Entities;

namespace FreshCar.Infrastructure.Core.Services.EF
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    internal class FreshCarContext : DbContext
    {
        /// <summary>
        /// Аккаунты
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        /// <summary>
        /// Автомойки
        /// </summary>
        public DbSet<CarWash> CarWashes { get; set; }

        /// <summary>
        /// Фотографии автомоек
        /// </summary>
        public DbSet<CarWashPhoto> CarWashPhotos { get; set; }

        /// <summary>
        /// Дополнительные услуги автомоек
        /// </summary>
        public DbSet<CarWashAdditionalWork> CarWashAdditionalWorks { get; set; }

        /// <summary>
        /// Боксы автомоек
        /// </summary>
        public DbSet<CarWashBox> CarWashBoxes { get; set; }

        /// <summary>
        /// Сотрудники автомойки
        /// </summary>
        public DbSet<CarWashEmployee> CarWashEmployees { get; set; }

        /// <summary>
        /// Фирмы
        /// </summary>
        public DbSet<Company> Companies { get; set; }

        /// <summary>
        /// Сотрудники
        /// </summary>
        public DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Заказы
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Услуги
        /// </summary>
        public DbSet<Work> Works { get; set; }

        /// <summary>
        /// Тарифы
        /// </summary>
        public DbSet<Tariff> Tariffs { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Настройка связей

            modelBuilder.Entity<Account>()
                .HasRequired(x => x.Employee);

            modelBuilder.Entity<CarWash>()
                .HasRequired(x => x.Company)
                .WithMany(x => x.CarWashes)
                .WillCascadeOnDelete(false);            

            modelBuilder.Entity<CarWashAdditionalWork>()
                .HasRequired(x => x.CarWash)
                .WithMany(x => x.CarWashAdditionalWorks)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarWashAdditionalWork>()
                .HasRequired(x => x.Work)
                .WithMany(x => x.CarWashAdditionalWorks)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarWashBox>()
                .HasRequired(x => x.CarWash)
                .WithMany(x => x.CarWashBoxes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarWashEmployee>()
                .HasRequired(x => x.CarWash)
                .WithMany(x => x.CarWashEmployees)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarWashEmployee>()
                .HasRequired(x => x.Employee)
                .WithMany(x => x.CarWashEmployeeLinks)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarWashPhoto>()
                .HasRequired(x=>x.CarWash)
                .WithMany(x=>x.CarWashPhotos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasRequired(x => x.Company)
                .WithMany(x => x.Employees)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasRequired(x => x.Tariff)
                .WithMany(x => x.Orders)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasRequired(x => x.CarWashBox)
                .WithMany(x => x.Orders)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Work>()
                .HasOptional(x => x.Company)
                .WithMany(x => x.Works)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tariff>()
                .HasRequired(x => x.Work)
                .WithMany(x => x.Tariffs)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tariff>()
                .HasRequired(x => x.CarWash)
                .WithMany(x => x.Tariffs)
                .WillCascadeOnDelete(false);

            #endregion


            #region Индексы

            modelBuilder.Entity<Account>()
                .Property(x => x.Email)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("UniqueEmail", 0));

            modelBuilder.Entity<Account>()
                .Property(x => x.EmployeeId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("UniqueEmployee", 1));

            modelBuilder.Entity<CarWashAdditionalWork>()
                .Property(x => x.CarWashId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("IX_CarWashId_WorkId", 0));

            modelBuilder.Entity<CarWashAdditionalWork>()
                .Property(x => x.WorkId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("IX_CarWashId_WorkId", 1));

            modelBuilder.Entity<CarWashEmployee>()
                .Property(x => x.CarWashId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("IX_CarWashId_EmployeeId", 0));

            modelBuilder.Entity<CarWashEmployee>()
                .Property(x => x.EmployeeId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, CreateUniqueIndex("IX_CarWashId_EmployeeId", 1));

            modelBuilder.Entity<Tariff>()
                .Property(x => x.CarType)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    CreateUniqueIndex("IX_CarType_WorkId_CarWashId", 0));

            modelBuilder.Entity<Tariff>()
                .Property(x => x.CarWashId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    CreateUniqueIndex("IX_CarType_WorkId_CarWashId", 1));

            modelBuilder.Entity<Tariff>()
                .Property(x => x.WorkId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    CreateUniqueIndex("IX_CarType_WorkId_CarWashId", 2));

            modelBuilder.Entity<CarWashPhoto>()
                .Property(x => x.PhotoUrl)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    CreateUniqueIndex("IX_CarWashPhotoUrl", 0));

            #endregion

            base.OnModelCreating(modelBuilder);
        }


        private IndexAnnotation CreateUniqueIndex(string name, int order)
        {
            return new IndexAnnotation(new IndexAttribute(name) {IsUnique = true, Order = order});
        }
    }
}