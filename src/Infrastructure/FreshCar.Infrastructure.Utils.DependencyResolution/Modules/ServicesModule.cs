﻿using System.Data.Entity;
using Autofac;
using FreshCar.Core.Services;
using FreshCar.Infrastructure.Core.Services.EF;
using FreshCar.Infrastructure.Core.Services.EF.Services;
using FreshCar.Infrastructure.Core.Services.EF.UnitOfWork;

namespace FreshCar.Infrastructure.Utils.DependencyResolution.Modules
{
    /// <summary>
    /// Модуль, в котором находятся DI настройки для сервисов, работающих с БД
    /// </summary>
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FreshCarContext>().AsSelf().As<DbContext>().InstancePerRequest();
            builder.RegisterGeneric(typeof (EfRepository<>)).As(typeof (IRepository<>));
            builder.RegisterType<EfUnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            builder.RegisterType<AccountService>().As<IAccountService>();
            builder.RegisterType<CarWashService>().As<ICarWashService>();
            builder.RegisterType<WorkService>().As<IWorkService>();
            builder.RegisterType<CarWashBoxService>().As<ICarWashBoxService>();
            builder.RegisterType<OrderService>().As<IOrderService>();
            builder.RegisterType<CompanyService>().As<ICompanyService>();
            builder.RegisterType<EmployeeService>().As<IEmployeeService>();
        }
    }
}
